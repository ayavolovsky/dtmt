/*
 * Model.cpp
 *
 *  Created on: May 14, 2013
 *      Author: ayavol2
 */

#include "Model.h"
#include <boost/format.hpp>
#include <boost/tokenizer.hpp>
#include <boost/filesystem.hpp>
#include <iostream>
#include <string>
#include <map>
#include <ModelInput.h>

#include "boost/shared_array.hpp"
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/discrete_distribution.hpp>

#include "Common.h"

using namespace std;

State::State()
{
	bImmediateFailureEnabled = false;
	bProbabilisticFailureEnabled  = true;
}

/*virtual*/ State::~State()
{

}

/*virtual*/ void State::EnableImmediateFailure(bool bEnable)
{
	bImmediateFailureEnabled = bEnable;
}

/*virtual*/ void State::EnableProbabilisticFailure(bool bEnable)
{
	bProbabilisticFailureEnabled = bEnable;
}

bool State::isImmediateFailureEnabled()
{
	return bImmediateFailureEnabled;
}

bool State::isProbabilisticFailureEnabled()
{
	return bProbabilisticFailureEnabled;
}

Model::Model() {
	bEmptyFile = false;
}

Model::~Model() {
	// TODO Auto-generated destructor stub
}

/*virtual*/ boost::shared_ptr<State> Model::CreateStartState()
{
	boost::shared_ptr<State> state = CreateNewState();
	state->ResetStart();

	return state;
}

/*virtual*/ double Model::RewardAction(boost::shared_ptr<State> state, MONITOR_ACTION_TYPE action, ModelRewardSystem& rewards)
{
	bool bFailureState = state->IsFailureState();

	switch (action)
	{
	case ACTION_CONTINUE:
		if (bFailureState)
		{
			return rewards.BadContinue;
		}
		else
		{
			return rewards.GoodContinue;
		}

		break;
	case ACTION_ALARM:
		if (bFailureState)
		{
			return rewards.BadAlarm;
		}
		else
		{
			return rewards.GoodAlarm;
		}

		break;
	}
	return 0.0;
}

/*virtual*/ void Model::EvaluateAction(boost::shared_ptr<State> state, MONITOR_ACTION_TYPE action, MONITOR_ACTION_EVALUATION& evaluation)
{
	bool bFailureState = state->IsFailureState();

	switch (action)
	{
	case ACTION_CONTINUE:
		if (bFailureState)
		{
			// state was a failure state but continue action was selected
			evaluation = ACTION_EVAL_MISSED_ALARM;
		}
		else
		{
			// state was not a failure state and continue action was selected
			evaluation = ACTION_EVAL_CORRECT_CONTINUE;
		}
		break;
	case ACTION_ALARM:
		if (bFailureState)
		{
			// state was a failure state and alarm action was selected
			evaluation = ACTION_EVAL_CORRECT_ALARM;
		}
		else
		{
			// state was a consistent state but alarm action was selected
			evaluation = ACTION_EVAL_FALSE_ALARM;
		}
		break;
	}
}

/*virtual*/ bool Model::Step(boost::shared_ptr<State> state, MONITOR_ACTION_TYPE action, boost::shared_ptr<ModelInput> input, boost::shared_ptr<ModelInput>& actualInput)
{
	bool bTerminalState = false;

	double time = state->GetTime();
	bool bEnableFailure = (time<=Options.GetPossibleFailureTimeRange());

	switch (action)
	{
	case ACTION_CONTINUE:
		state->Continue(bEnableFailure, input, actualInput);
		break;
	case ACTION_ALARM:
		bTerminalState = true;
		break;
	}

	return bTerminalState;
}

/*virtual*/ void Model::GetStateObservation(boost::shared_ptr<State> state, boost::shared_ptr<ModelObservation>& observation)
{
	observation = state->GetObservation();
}

void Model::InitializeHistoryFile(const std::string& historyDirPath, unsigned int GroupID, unsigned int ID, bool bNoGroup /*= false*/)
{
	if (HistoryFile.is_open())
	{
		// file already initialized
		return;
	}

	string dirPath = historyDirPath;
	if (historyDirPath.empty())
	{
		dirPath = ".";
	}

	string filePath;
	boost::filesystem::path dir(dirPath);

	if (bNoGroup)
	{
		boost::filesystem::path file(boost::str(boost::format("History%1%.csv") % ID));
		boost::filesystem::path full_path = dir / file;

		int index = 1;
		while (boost::filesystem::exists(full_path))
		{
			file = boost::filesystem::path(boost::str(boost::format("History%1%_%2%.csv") % ID % index));
			full_path = dir / file;
			index++;
		}

		filePath = full_path.string();

		//filePath = boost::str(boost::format("%1%\\History%2%.csv") % dirPath % ID);
	}
	else
	{
		boost::filesystem::path file(boost::str(boost::format("History%1%_%2%.csv") % GroupID % ID));
		boost::filesystem::path full_path = dir / file;
		filePath = full_path.string();

		//filePath = boost::str(boost::format("%1%\\History%2%_%3%.csv") % dirPath % GroupID % ID);
	}

	HistoryFile.open(filePath.c_str(), ios::out);

	bEmptyFile = true;

	if (!HistoryFile.is_open())
	{
		cerr << "Unable to create history file at " << filePath << "\n";
	}
}

void Model::AddHistoryState(boost::shared_ptr<State> state, boost::shared_ptr<ModelObservation> observation, boost::shared_ptr<ModelInput> input, bool bTerminal)
{
	if (!HistoryFile.is_open())
	{
		return;
	}

	if (bEmptyFile)
	{
		if (input)
		{
			HistoryFile << "Terminal," << state->GetCSVHeader() << "," << observation->GetCSVHeader() << "," << input->GetCSVHeader() << std::endl;
		}
		else
		{
			HistoryFile << "Terminal," << state->GetCSVHeader() << "," << observation->GetCSVHeader() << std::endl;
		}

		bEmptyFile = false;
	}

	if (input)
	{
		HistoryFile << bTerminal << "," << state->GetCSV() << "," << observation->GetCSV() << "," << input->GetCSV() << std::endl;
	}
	else
	{
		HistoryFile << bTerminal << "," << state->GetCSV() << "," << observation->GetCSV() << std::endl;
	}
}

void Model::FinalizeHistoryFile()
{
	if (!HistoryFile.is_open())
	{
		return;
	}

	HistoryFile.close();
}

void Model::SetSimulationOptions(const ModelSimulationOptions& o)
{
	Options = o;
}

/*virtual*/ void Model::GetInitialBelief(BeliefState& belief, boost::shared_ptr<State> realStartState, unsigned int particles)
{
	for (unsigned int i = 0; i<particles; i++)
	{
		boost::shared_ptr<State> particle = CreateNewState();
		particle->Copy(realStartState);

		belief.AddParticle(particle);
	}
}

/*virtual*/ void Model::GetObservationDitributionAsParticles(BeliefState& belief, std::vector< boost::shared_ptr<ModelObservation> >& observations)
{
	double totalObservationWeight = 0;

	map< unsigned long, boost::shared_ptr<ModelObservation> > exploredObservations;
	map< unsigned long, double > exploredObservationWeights;

	unsigned int nTotalSampledStates = 200;
	unsigned int nTotalStateRepeats = 10;

	for (unsigned int iSample = 0; iSample < nTotalSampledStates; iSample++)
	{
		unsigned int iState = floor(UniformRand(0, belief.GetSize()-1));

		for (unsigned int iStateRepeat = 0; iStateRepeat < nTotalStateRepeats; iStateRepeat++)
		{
			boost::shared_ptr<ModelObservation> stateObservation = belief.GetParticle(iState)->GetObservation();
			unsigned long observationID = belief.GetParticle(iState)->GetObservationID(stateObservation);
			if (exploredObservations.find(observationID)!=exploredObservations.end())
			{
				continue;
			}

			exploredObservations[observationID] = stateObservation;

			double totalWeight = 0;
			for (unsigned int iState = 0; iState < belief.GetSize(); iState++)
			{
				totalWeight += Weight(belief.GetParticle(iState), stateObservation);
			}
			totalWeight /= belief.GetSize();
			exploredObservationWeights[observationID] = totalWeight;

			totalObservationWeight += totalWeight;
		}
	}

	boost::shared_array<double> observationWeights(new double[exploredObservations.size()]);
	vector< boost::shared_ptr<ModelObservation> > modelObservations;

	// normalize weights and move them to array
	int i = 0;
	for (map< unsigned long, boost::shared_ptr<ModelObservation> >::iterator iObservation = exploredObservations.begin(); iObservation!=exploredObservations.end(); iObservation++, i++)
	{
		observationWeights[i] = exploredObservationWeights[iObservation->first] / totalObservationWeight;
		modelObservations.push_back(iObservation->second);
	}

	// resampling step
	boost::mt19937 gen;
	boost::random::discrete_distribution<> dist(observationWeights.get(), observationWeights.get() + modelObservations.size());
	for (unsigned int iObservation = 0; iObservation<modelObservations.size(); iObservation++)
	{
		int selectedParticle = dist(gen);
		observations.push_back(modelObservations.at(selectedParticle));
	}
}

/*virtual*/ double Model::Weight(boost::shared_ptr<State> state, boost::shared_ptr<ModelObservation> observation)
{
	return state->WeightObservation(observation);
}

bool Model::NextStepFromFile(std::fstream& file, boost::shared_ptr<State>& state, boost::shared_ptr<ModelObservation>& observation, boost::shared_ptr<ModelInput>& input, bool& bTerminal, bool bUpdateProperty, const boost::shared_ptr<State>& currentRealState)
{
	if (!file.is_open())
	{
		return false;
	}

	if (file.eof())
	{
		return false;
	}

	string line;
	getline(file, line);

	typedef boost::tokenizer< boost::escaped_list_separator<char> > Tokenizer;
	vector< string > parts;

	Tokenizer tok(line);
	parts.assign(tok.begin(),tok.end());

	if (parts.size()<1)	// terminal
	{
		return false;
	}

	if (parts.at(0).compare("1")==0) bTerminal = true; else bTerminal = false;
	parts.erase(parts.begin());

	// create empty state
	boost::shared_ptr<State> newState = CreateNewState();
	observation = CreateNewObservation();
	input = CreateNewInput();

	// restore state
	if (!newState->FromCSVTokens(parts, observation, input, bUpdateProperty, currentRealState))
	{
		return false;
	}

	state = newState;

	return true;
}

unsigned long Model::GetObservationID(boost::shared_ptr<ModelObservation> observation)
{
	if (!tmpState)
	{
		tmpState = CreateNewState();
	}

	return tmpState->GetObservationID(observation);
}

void Model::SetInputFunction(boost::shared_ptr<ModelInputFunction> inputFunc)
{
	InputFunction = inputFunc;
}

boost::shared_ptr<ModelInputFunction> Model::GetInputFunction()
{
	return InputFunction;
}

boost::shared_ptr<ModelInput> Model::GetInputAt(boost::shared_ptr<State> state)
{
	if (!InputFunction || !state)
	{
		return boost::shared_ptr<ModelInput>();
	}

	return state->GetInput(InputFunction);
}

std::string State::ObservationToString(boost::shared_ptr<ModelObservation> observation)
{
	string str = "<Observation>";
	return str;
}

/*virtual*/ void State::AddObservationDisturbance(boost::shared_ptr<ModelObservation> observation)
{
	// do nothing by default
}

/*
 * Monitor.cpp
 *
 *  Created on: Apr 16, 2014
 *      Author: andrey
 */

#include "Monitor.h"
#include "DecisionSystem.h"
#include <Input/ModelInputFunction.h>
#include "Problems/SingleCarBreakingSystem.h"
#include "Problems/SingleCarBreakingSystemV2.h"
#include "Problems/RPMSimpleSystem.h"
#include "Problems/MultipleCarBreakingSystem.h"
#include "DecisionMakers/POMCP.h"
#include "DecisionMakers/ThresholdBased.h"
#include "DecisionMakers/MaximumExpectedUtility.h"
#include "MonitoringHandle.h"
#include <iostream>

#include <boost/shared_ptr.hpp>

using namespace std;

Monitor::Monitor() {
	Handle = NULL;
}

Monitor::~Monitor() {
	// TODO Auto-generated destructor stub
}

bool Monitor::Initialize(const SolverOptions& Options)
{
	boost::shared_ptr<Model> SimulationModel;
	boost::shared_ptr<Model> ControlModel;
	boost::shared_ptr<ModelInputFunction> InputFunction;

	MonitoringHandle* pHandle = new MonitoringHandle();

	// allocate the real and simulation models
	switch (Options.GetExperimentModel())
	{
	case SolverOptions::MODEL_SINGLE_CAR_BRAKING_SYSTEM:
		SimulationModel = boost::shared_ptr<Model>(new SingleCarBreakingSystem());
		ControlModel = boost::shared_ptr<Model>(new SingleCarBreakingSystem());
		break;
	case SolverOptions::MODEL_SINGLE_CAR_BRAKING_SYSTEM_V2:
		SimulationModel = boost::shared_ptr<Model>(new SingleCarBreakingSystemV2());
		ControlModel = boost::shared_ptr<Model>(new SingleCarBreakingSystemV2());
		break;
	case SolverOptions::MODEL_MULTILPE_CAR_BRAKING_SYSTEM:
		SimulationModel = boost::shared_ptr<Model>(new MultipleCarBreakingSystem());
		ControlModel = boost::shared_ptr<Model>(new MultipleCarBreakingSystem());
		break;
	case SolverOptions::MODEL_RPM_SIMPLE_SYSTEM:
		SimulationModel = boost::shared_ptr<Model>(new RPMSimpleSystem());
		ControlModel =  boost::shared_ptr<Model>(new RPMSimpleSystem());

		InputFunction	= boost::shared_ptr<RPMSimpleSystemInputFunction>(new RPMSimpleSystemInputFunction(Options.GetInputFunctionDataFile()));

		ControlModel->SetInputFunction(InputFunction);
		SimulationModel->SetInputFunction(InputFunction);
		break;
	default:
		cout << "Unknown experiment model to execute: " << Options.GetExperimentModel() << "\n";
		return false;
		break;
	}

	SimulationModel->SetRewardSystem(Options.GetRewardsSystem());

	ModelSimulationOptions simulationOptions;
	simulationOptions.SetErrorneousTimeRange(Options.GetErrornousTimeRange());

	SimulationModel->SetSimulationOptions(simulationOptions);

	boost::shared_ptr<DecisionSystem> decisionSystemObj = boost::shared_ptr<DecisionSystem>(new DecisionSystem(SimulationModel, cout.rdbuf()));

	// allocate the decision maker
	switch (Options.GetDecisionMaker())
	{
	case SolverOptions::DECISION_POMCP:
		{
			boost::shared_ptr<POMCP> pomcpSolver = boost::shared_ptr<POMCP>(new POMCP());

			pomcpSolver->GetOptions().DiscountFactor = Options.GetDiscountFactor();
			pomcpSolver->GetOptions().ExplorationConstant = Options.GetExplorationConstant();
			pomcpSolver->GetOptions().MaxTreeDepth = Options.GetPOMCPMaxTreeDepth();
			pomcpSolver->GetOptions().Simulations = Options.GetPOMCPSimulations();

			decisionSystemObj->AddDecisionMaker("", Options.GetDecisionMaker(), pomcpSolver, Options.Rewards);
		}
		break;
	case SolverOptions::DECISION_THRESHOLD_BASED:
		{
			boost::shared_ptr<ThresholdBased> thresholdSolver = boost::shared_ptr<ThresholdBased>(new ThresholdBased());
			thresholdSolver->GetOptions().ThresholdValue = Options.GetThresholdValue();
			thresholdSolver->GetOptions().Accuracy = Options.GetThresholdAccuracy();

			decisionSystemObj->AddDecisionMaker("", Options.GetDecisionMaker(), thresholdSolver, Options.Rewards);
		}
		break;
	case SolverOptions::DECISION_MAX_EXPECTED_UTILITY:
		{
			boost::shared_ptr<MaximumExpectedUtility> maxExpSolver = boost::shared_ptr<MaximumExpectedUtility>(new MaximumExpectedUtility());

			decisionSystemObj->AddDecisionMaker("", Options.GetDecisionMaker(), maxExpSolver, Options.Rewards);
		}
		break;
	default:
		cout << "Unknown decision maker type: " << Options.GetDecisionMaker() << "\n";
		return false;
		break;
	}

	cout << "Trials Dir path: " << Options.GetTrialsDirPath() << "\n";

	if (!Options.GetTrialsDirPath().empty())
	{
		ControlModel->InitializeHistoryFile(Options.GetTrialsDirPath(), 0, 0, true);
		pHandle->bHistoryLoggingEnabled = true;
	}
	else
	{
		pHandle->bHistoryLoggingEnabled = false;
	}

	pHandle->SimulationModel = SimulationModel;
	pHandle->DecisionSystemObj = decisionSystemObj;
	pHandle->ControlModel = ControlModel;
	pHandle->ControlState = ControlModel->CreateStartState();

	Handle = (void*) pHandle;

	decisionSystemObj->SetOutput(cout.rdbuf());

	BeliefState initialBelief;
	ControlModel->GetInitialBelief(initialBelief, pHandle->ControlState, Options.GetBeliefStateDimension());
	decisionSystemObj->SetInitialBelief(initialBelief);

	return true;
}

MONITOR_ACTION_TYPE Monitor::monitor_GetAction()
{
	if (Handle==NULL)
	{
		return ACTION_CONTINUE;
	}

	MonitoringHandle* pHandle = (MonitoringHandle*) Handle;

	std::vector<MONITOR_ACTION_TYPE> actions;
	std::vector<double> actionsDuration;
	pHandle->DecisionSystemObj->GetAction(actions, actionsDuration);

	return actions[0];
}

void Monitor::control_Continue(ModelInput* input)
{
	if (Handle==NULL)
	{
		return;
	}

	MonitoringHandle* pHandle = (MonitoringHandle*) Handle;

	boost::shared_ptr<ModelInput> clonedInput = boost::shared_ptr<ModelInput>(input->Clone());

	if (pHandle->bHistoryLoggingEnabled && pHandle->MostRecentObservation)
	{
		pHandle->ControlModel->AddHistoryState(pHandle->ControlState, pHandle->MostRecentObservation, clonedInput, 0);
	}

	boost::shared_ptr<ModelInput> actualInput;
	pHandle->ControlModel->Step(pHandle->ControlState, ACTION_CONTINUE, clonedInput, actualInput);
}

void Monitor::monitor_BeliefUpdate(MONITOR_ACTION_TYPE action, ModelInput* lastInput, ModelObservation* currentObservation)
{
	if (Handle==NULL)
	{
		return;
	}

	MonitoringHandle* pHandle = (MonitoringHandle*) Handle;

	boost::shared_ptr<ModelObservation> clonedCurrentObservation = boost::shared_ptr<ModelObservation>(currentObservation->Clone());
	boost::shared_ptr<ModelInput> clonedLastInput = boost::shared_ptr<ModelInput>(lastInput->Clone());

	pHandle->ControlState->AddObservationDisturbance(clonedCurrentObservation);

	pHandle->MostRecentObservation = clonedCurrentObservation;

	BeliefState tmpBelief;
	pHandle->DecisionSystemObj->ForwardUpdate(action, clonedCurrentObservation, clonedLastInput, tmpBelief);

	cout << "Belief state: " << pHandle->DecisionSystemObj->BeliefToString(pHandle->ControlState) << "\n";
}

void Monitor::control_Observe(ModelObservation* observation)
{
	if (Handle==NULL)
	{
		return;
	}

	MonitoringHandle* pHandle = (MonitoringHandle*) Handle;

	boost::shared_ptr<ModelObservation> modelObs = boost::shared_ptr<ModelObservation>(observation->Clone());
	pHandle->ControlState->FullyObserve(modelObs);
	observation->Copy(modelObs);
}

void Monitor::EnableImmediateFailure(bool bEnable)
{
	if (Handle==NULL)
	{
		return;
	}

	MonitoringHandle* pHandle = (MonitoringHandle*) Handle;

	pHandle->ControlState->EnableImmediateFailure(bEnable);
}

void Monitor::EnableProbabilisticFailure(bool bEnable)
{
	if (Handle==NULL)
	{
		return;
	}

	MonitoringHandle* pHandle = (MonitoringHandle*) Handle;

	pHandle->ControlState->EnableProbabilisticFailure(bEnable);
}

/*
 * ThresholdBased.cpp
 *
 *  Created on: Jun 21, 2013
 *      Author: ayavol2
 */

#include "ThresholdBased.h"
#include "math.h"
#include <DecisionCommon.h>
#include <DecisionSystem.h>
#include <MonitoringDefs.h>
#include <Common.h>
#include "boost/program_options.hpp"
#include <iostream>

namespace po = boost::program_options;

ThresholdBased::ThresholdBased()
{
	// TODO Auto-generated constructor stub

}

ThresholdBased::~ThresholdBased() {
	// TODO Auto-generated destructor stub
}

/*virtual*/ bool ThresholdBased::LoadOptions(const std::string& optionsFilePath)
{
	po::options_description options("Options");
	options.add_options()
			("threshold", po::value<double>(&Options.ThresholdValue)->default_value(0.0), "")
			("accuracy", po::value<double>(&Options.Accuracy)->default_value(1e-3), "")
			("horizon", po::value<unsigned int>(&Options.Horizon)->default_value(1), "")
			("simulations", po::value<unsigned int>(&Options.Simulations)->default_value(1), "")
			;

	po::variables_map vm;

    try
    {
        if (!optionsFilePath.empty())
        {
        	std::ifstream ifs(optionsFilePath.c_str());
            if (!ifs)
            {
                return false;
            }
            else
            {
				po::store(po::parse_config_file(ifs, options), vm);
				po::notify(vm);
            }
        }
    }
    catch (po::error& e)
    {
    	return false;
    }

    return false;
}

/*virtual*/ MONITOR_ACTION_TYPE ThresholdBased::GetAction(BeliefState* currentBelief)
{
	if (Options.Horizon==1)
	{
		double failureChance = 0;
		failureChance = GetDecisionSystem()->FailureProbability();

		// TMP
		GetDecisionSystem()->Output() << "ThresholdBased: RejProb Horizon = " << Options.Horizon << " : " << failureChance << "\n";

		if (failureChance-Options.ThresholdValue > Options.Accuracy ||
				fabs(failureChance-Options.ThresholdValue)<Options.Accuracy)
		{
			return ACTION_ALARM;
		}
	}
	else
	{
		double rejProb = DecisionCommon::GetHorizonRejectionProbability(currentBelief, GetDecisionSystem()->GetSimulationModel(), Options.Horizon, Options.Simulations);
		if (rejProb - Options.ThresholdValue > Options.Accuracy ||
				fabs(rejProb - Options.ThresholdValue)<Options.Accuracy)
		{
			return ACTION_ALARM;
		}
	}

	return ACTION_CONTINUE;
}

ThresholdOptions& ThresholdBased::GetOptions()
{
	return Options;
}

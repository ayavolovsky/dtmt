/*
 * main.cpp
 *
 *  Created on: Feb 12, 2017
 *      Author: ayavol2
 */

#include <boost/shared_ptr.hpp>
#include "ThresholdBased.h"

boost::shared_ptr<DecisionMaker> CreateDecisionMaker()
{
	boost::shared_ptr<ThresholdBased> dm = boost::shared_ptr<ThresholdBased>(new ThresholdBased());
	return dm;
}



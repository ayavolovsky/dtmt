/*
 * ThresholdBased.h
 *
 *  Created on: Jun 21, 2013
 *      Author: ayavol2
 */

#ifndef THRESHOLDBASED_H_
#define THRESHOLDBASED_H_

#include <DecisionMaker.h>
#include <MonitoringDefs.h>
#include <Model.h>

class ThresholdOptions
{
public:
	ThresholdOptions()
	{
		ThresholdValue = 0.9;
		Accuracy = 1e-3;
		Horizon = 0;
		Simulations = 5000;
	}

	virtual ~ThresholdOptions()
	{

	}

	double ThresholdValue;
	double Accuracy;
	unsigned int Horizon;
	unsigned int Simulations;
};

class ThresholdBased : public DecisionMaker {
public:
	ThresholdBased();
	virtual ~ThresholdBased();

	// loads options for the decision maker, i.e. makes it ready
	virtual bool LoadOptions(const std::string& optionsFilePath);

	// find the action to be executed according to the current knowledge
	virtual MONITOR_ACTION_TYPE GetAction(BeliefState* currentBelief);

	ThresholdOptions& GetOptions();
private:
	double CalculateHorizonFailureProbability( BeliefState* currentBelief );
	double CalculateHorizon1PredictedFailureProbability( BeliefState* currentBelief );

private:
	ThresholdOptions Options;

};

#endif /* THRESHOLDBASED_H_ */

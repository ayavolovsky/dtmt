/*
 * POMCP.h
 *
 *  Created on: May 15, 2013
 *      Author: ayavol2
 */

#ifndef POMCP_H_
#define POMCP_H_

#include <DecisionMaker.h>
#include <MonitoringDefs.h>
#include <Model.h>
#include <ClassFactory.h>

class TreeNode;
class MCTSNodeData;

class POMCPOptions
{
public:
	POMCPOptions()
	{
		Simulations = 1;
		DiscountFactor = 0.95;
		ExplorationConstant = 1;
		MaxTreeDepth = 100;
		bContinueOnly = false;
	}

	virtual ~POMCPOptions()
	{

	}

	unsigned int Simulations;
	double DiscountFactor;
	double ExplorationConstant;
	unsigned int MaxTreeDepth;
	bool bContinueOnly;
	ModelRewardSystem Rewards;
};

class POMCP: public DecisionMaker {
private:
	enum UCB_STATE_INFO {UCB_UNDEFINED, UCB_CONSISTENT, UCB_FAILURE};

	class POMCP_ACTION
	{
		MONITOR_ACTION_TYPE monitor_action;
		boost::shared_ptr<ModelInput> input_with_action;
	};
public:
	POMCP();
	virtual ~POMCP();

	// loads options for the decision maker, i.e. makes it ready
	virtual bool LoadOptions(const std::string& optionsFilePath);

	// find the action to be executed according to the current knowledge
	virtual MONITOR_ACTION_TYPE GetAction(BeliefState* currentBelief);

	POMCPOptions& GetOptions();
private:
	boost::shared_ptr<TreeNode> NewNode();

	boost::shared_ptr<MCTSNodeData> NodeData(boost::shared_ptr<TreeNode> node);

	virtual double SimulateV(boost::shared_ptr<State> sample, boost::shared_ptr<TreeNode> node, unsigned int treeDepth, std::vector<boost::shared_ptr<State> >::iterator iAllocatedState, std::vector<boost::shared_ptr<ModelObservation> >::iterator iAllocatedObservation);

	virtual double SimulateQ(boost::shared_ptr<State> sample, boost::shared_ptr<TreeNode> node, MONITOR_ACTION_TYPE action, unsigned int treeDepth, std::vector<boost::shared_ptr<State> >::iterator iAllocatedState, std::vector<boost::shared_ptr<ModelObservation> >::iterator iAllocatedObservation);

	double Rollout(boost::shared_ptr<State> sample, unsigned int treeDepth);

	MONITOR_ACTION_TYPE GreedyUCB(boost::shared_ptr<TreeNode> node, UCB_STATE_INFO stateInfo, bool bIgnoreUnattemptedAction, bool bFastUCB, double& valueContinue, double& valueAlarm);

	double FastUCB(int N, int n, double logN);
protected:
	double GetContinueValue(BeliefState* currentBelief);

	void UCTSearch(boost::shared_ptr<TreeNode> ROOT);

	virtual void EvaluateAction(boost::shared_ptr<State> state, MONITOR_ACTION_TYPE action, unsigned int treeDepth, double& reward, MONITOR_ACTION_EVALUATION& evaluation);
	virtual unsigned long GetStateObservationID(boost::shared_ptr<State> state);

	POMCPOptions Options;

	ClassFactory<TreeNode> TreeNodesFactory;
	std::vector<boost::shared_ptr<TreeNode> > AllocatedTreeNodes;
	std::vector<boost::shared_ptr<TreeNode> >::iterator iAllocatedTreeNode;

	ClassFactory<MCTSNodeData> NodeDataFactory;
};

#endif /* POMCP_H_ */

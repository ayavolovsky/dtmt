/*
 * TreeNode.cpp
 *
 *  Created on: May 17, 2013
 *      Author: ayavol2
 */

#include "TreeNode.h"
#include "TreeData.h"
#include <sstream>

using namespace std;

TreeNode::TreeNode() {
	// TODO Auto-generated constructor stub

}

TreeNode::~TreeNode() {
	// TODO Auto-generated destructor stub
}

void TreeNode::AddChild(unsigned int id1, unsigned int id2, boost::shared_ptr<TreeNode> node)
{
	Children[id1][id2] = node;
}

bool TreeNode::GetChild(unsigned int id1, unsigned int id2, boost::shared_ptr<TreeNode>& child)
{
	map<unsigned int, map<unsigned int, boost::shared_ptr<TreeNode> > >::iterator iChild1 = Children.find(id1);
	if (iChild1==Children.end())
	{
		return false;
	}

	map<unsigned int, boost::shared_ptr<TreeNode> >::iterator iChild2 = iChild1->second.find(id2);
	if (iChild2==iChild1->second.end())
	{
		return false;
	}

	child = iChild2->second;
	return true;
}

void TreeNode::RemoveChild1(unsigned int id1)
{
	Children.erase(id1);
}

void TreeNode::RemoveChild(unsigned int id1, unsigned int id2)
{
	map<unsigned int, map<unsigned int, boost::shared_ptr<TreeNode> > >::iterator iChild1 = Children.find(id1);
	if (iChild1==Children.end())
	{
		return;
	}

	iChild1->second.erase(id2);
}

void TreeNode::RemoveAllChildren()
{
	Children.clear();
}

string TreeNode::ToString(const std::string& indent)
{
	stringstream ss;

	if (Data!=boost::shared_ptr<TreeData>())
	{
		ss << indent << Data->ToString() << "\n";
	}

	ss << indent << "# of children = " << Children.size() << "\n";

	for (map<unsigned int, map<unsigned int, boost::shared_ptr<TreeNode> > >::iterator iChild1 = Children.begin(); iChild1!=Children.end(); iChild1++)
	{
		for (map<unsigned int, boost::shared_ptr<TreeNode> >::iterator iChild2 = iChild1->second.begin(); iChild2!=iChild1->second.begin(); iChild2++)
		{
			ss << indent << "Child = " << iChild1->first << ";" << iChild2->first << "\n";
			ss << iChild2->second->ToString(indent + "\t") << "\n";
		}
	}

	return ss.str();
}

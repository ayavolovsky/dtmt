/*
 * MCTSNodeData.h
 *
 *  Created on: May 17, 2013
 *      Author: ayavol2
 */

#ifndef MCTSNODEDATA_H_
#define MCTSNODEDATA_H_

#include "TreeData.h"
#include <BeliefState.h>

class MCTSNodeData : public TreeData {
public:
	MCTSNodeData();
	virtual ~MCTSNodeData();

	BeliefState& GetBelief();

	virtual std::string ToString();

	void SetBelief(const BeliefState& bel);
	void AddValue(double val);

	double GetValueSum();
	unsigned int GetValueCount();

	double GetValue();
private:
	BeliefState Belief;

	unsigned int ValueCount;
	double ValueSum;
};

#endif /* MCTSNODEDATA_H_ */

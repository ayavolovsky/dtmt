/*
 * TreeData.h
 *
 *  Created on: May 17, 2013
 *      Author: ayavol2
 */

#ifndef TREEDATA_H_
#define TREEDATA_H_

#include <string>

class TreeData {
public:
	TreeData();
	virtual ~TreeData();

	virtual std::string ToString() = 0;
};

#endif /* TREEDATA_H_ */

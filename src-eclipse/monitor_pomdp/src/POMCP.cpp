/*
 * POMCP.cpp
 *
 *  Created on: May 15, 2013
 *      Author: ayavol2
 */

#include "POMCP.h"
#include "MCTSNodeData.h"
#include "TreeNode.h"

#include "boost/shared_array.hpp"
#include <math.h>
#include <iostream>
#include <float.h>
#include "boost/program_options.hpp"
#include <iostream>

#include <DecisionCommon.h>
#include <DecisionSystem.h>
#include <ModelRewardSystem.h>

using namespace std;

#define Infinity 1e+10

namespace po = boost::program_options;

POMCP::POMCP()
{
	// TODO Auto-generated constructor stub

}

POMCP::~POMCP() {
	// TODO Auto-generated destructor stub
}

/*virtual*/ bool POMCP::LoadOptions(const std::string& optionsFilePath)
{
	po::options_description options("Options");
	options.add_options()
			("simulations", po::value<unsigned int>(&Options.Simulations)->default_value(1000), "")
			("discount", po::value<double>(&Options.DiscountFactor)->default_value(1), "")
			("explorationconstant", po::value<double>(&Options.ExplorationConstant)->default_value(1), "")
			("maxtreedepth", po::value<unsigned int>(&Options.MaxTreeDepth)->default_value(10), "")
			("good_alarm", po::value<double>(&Options.Rewards.GoodAlarm)->default_value(0), "")
			("good_continue", po::value<double>(&Options.Rewards.GoodContinue)->default_value(0), "")
			("bad_alarm", po::value<double>(&Options.Rewards.BadAlarm)->default_value(0), "")
			("bad_continue", po::value<double>(&Options.Rewards.BadContinue)->default_value(0), "")
			;

	po::variables_map vm;

    try
    {
        if (!optionsFilePath.empty())
        {
        	std::ifstream ifs(optionsFilePath.c_str());
            if (!ifs)
            {
                return false;
            }
            else
            {
				po::store(po::parse_config_file(ifs, options), vm);
				po::notify(vm);
            }
        }
    }
    catch (po::error& e)
    {
    	return false;
    }

    return false;
}

/*virtual*/ MONITOR_ACTION_TYPE POMCP::GetAction(BeliefState* currentBelief)
{
	boost::shared_ptr<TreeNode> ROOT = NewNode();
	NodeData(ROOT)->SetBelief(*currentBelief);

	UCTSearch(ROOT);

	UCB_STATE_INFO stateInfo = UCB_CONSISTENT;
	bool bIgnoreUnattemptedAction = true;
	bool bFastUCB = false;
	double valueContinue = 0;
	double valueAlarm = 0;

	return GreedyUCB(ROOT, stateInfo, bIgnoreUnattemptedAction, bFastUCB, valueContinue, valueAlarm);
}

double POMCP::GetContinueValue(BeliefState* currentBelief)
{
	boost::shared_ptr<TreeNode> ROOT = NewNode();
	NodeData(ROOT)->SetBelief(*currentBelief);

	UCTSearch(ROOT);

	UCB_STATE_INFO stateInfo = UCB_CONSISTENT;
	bool bIgnoreUnattemptedAction = true;
	bool bFastUCB = false;
	double valueContinue = 0;
	double valueAlarm = 0;

	GreedyUCB(ROOT, stateInfo, bIgnoreUnattemptedAction, bFastUCB, valueContinue, valueAlarm);

	return valueContinue;
}

void POMCP::UCTSearch(boost::shared_ptr<TreeNode> ROOT)
{
	// statistics
	unsigned int nConsistentStates = 0;
	unsigned int nFailureStates = 0;

	// allocate memory for states and observations for every depth of the tree
	// this should save considerable amount of time on memory allocation
	//static bool bBuffersAllocated = false;
	vector<boost::shared_ptr<State> > statesBuffer;
	vector<boost::shared_ptr<ModelObservation> > observationBuffer;

	/*if (!bBuffersAllocated)
	{*/
		for (unsigned int i=0; i<Options.MaxTreeDepth+1; i++)
		{
			statesBuffer.push_back(GetDecisionSystem()->GetSimulationModel()->CreateNewState());
		}

		for (unsigned int i=0; i<Options.MaxTreeDepth+1; i++)
		{
			observationBuffer.push_back(GetDecisionSystem()->GetSimulationModel()->CreateNewObservation());
		}

/*		bBuffersAllocated = true;
	}*/

	vector<boost::shared_ptr<State> >::iterator iState = statesBuffer.begin();
	vector<boost::shared_ptr<State> >::iterator iNextState = iState++;
	vector<boost::shared_ptr<ModelObservation> >::iterator iObservation = observationBuffer.begin();

	//boost::shared_ptr<State> sample = GetDecisionSystem()->GetSimulationModel()->CreateNewState();

	for (unsigned int iSimulation = 0; iSimulation<Options.Simulations; iSimulation++)
	{
		//GetDecisionSystem()->Output() << "Root" << "\n";

		//NodeData(ROOT)->GetBelief().CopyRandomSample(sample);

		boost::shared_ptr<State>& sample = *iState;
		NodeData(ROOT)->GetBelief().CopyRandomSample(sample);

		if (sample->IsFailureState())
		{
			nFailureStates++;
		}
		else
		{
			nConsistentStates++;
		}

		unsigned int treeDepth = 0;
		SimulateV(sample, ROOT, treeDepth, iNextState, iObservation);
	}
}

double POMCP::SimulateV(boost::shared_ptr<State> sample, boost::shared_ptr<TreeNode> vnode, unsigned int treeDepth, vector<boost::shared_ptr<State> >::iterator iAllocatedState, std::vector<boost::shared_ptr<ModelObservation> >::iterator iAllocatedObservation)
{
/*	if (treeDepth>=Options.MaxTreeDepth)
	{
		return 0.0;
	}*/

	UCB_STATE_INFO stateInfo = UCB_UNDEFINED;
	if (sample->IsFailureState())
	{
		stateInfo = UCB_FAILURE;
	}
	else
	{
		stateInfo = UCB_CONSISTENT;
	}

	bool bIgnoreUnattemptedAction = false;
	bool bFastUCB = true;

	double valueContinue = 0;
	double valueAlarm = 0;
	MONITOR_ACTION_TYPE action = GreedyUCB(vnode, stateInfo, bIgnoreUnattemptedAction, bFastUCB, valueContinue, valueAlarm);

	boost::shared_ptr<TreeNode> qnode;
	if (!vnode->GetChild(action, 0, qnode))
	{
		qnode = NewNode();
		vnode->AddChild(action, 0, qnode);
	}

	double totalReward = SimulateQ(sample, qnode, action, treeDepth, iAllocatedState, iAllocatedObservation);
	NodeData(vnode)->AddValue(totalReward);

	return totalReward;
}

double POMCP::SimulateQ(boost::shared_ptr<State> sample, boost::shared_ptr<TreeNode> qnode, MONITOR_ACTION_TYPE action, unsigned int treeDepth , vector<boost::shared_ptr<State> >::iterator iAllocatedState, std::vector<boost::shared_ptr<ModelObservation> >::iterator iAllocatedObservation)
{
	double immediateReward;
	MONITOR_ACTION_EVALUATION evaluation;

	EvaluateAction(sample, action, treeDepth, immediateReward, evaluation);

	if (treeDepth>=Options.MaxTreeDepth)
	{
		return immediateReward;
	}

	std::vector<boost::shared_ptr<ModelInput> > possibleStateInputs;

	if (IsInputFunctionKnown())
	{
		boost::shared_ptr<ModelInput> knownInput = GetDecisionSystem()->GetSimulationModel()->GetInputAt(sample);
		possibleStateInputs.push_back(knownInput);
	}
	else
	{
		if (treeDepth>=1)
		{
			boost::shared_ptr<ModelInput> dummyInput;
			possibleStateInputs.push_back(dummyInput);
		}
		else
		{
			sample->GetPossibleInputs(possibleStateInputs);
		}
	}

	double totalRewardSum = 0.0;

	boost::shared_ptr<State> sampleCopy = GetDecisionSystem()->GetSimulationModel()->CreateNewState();
	//boost::shared_ptr<State>& sampleCopy = *iAllocatedState;

	for (int i = 0; i<possibleStateInputs.size(); i++)
	{
		boost::shared_ptr<ModelInput> testInput = possibleStateInputs.at(i);
		boost::shared_ptr<ModelInput> actuallyUsedInput;

		sampleCopy->Copy(sample);

		bool bTerminal = GetDecisionSystem()->GetSimulationModel()->Step(sampleCopy, action, testInput, actuallyUsedInput);

		unsigned long observationID = GetStateObservationID(sampleCopy);
		/*boost::shared_ptr<ModelObservation> observation = GetDecisionSystem()->GetSimulationModel()->CreateNewObservation();
		//boost::shared_ptr<ModelObservation>& observation = *iAllocatedObservation;
		GetDecisionSystem()->GetSimulationModel()->GetStateObservation(sampleCopy, observation);
		unsigned long observationID = sampleCopy->GetObservationID(observation);*/

		unsigned long inputID = 0;
		if (action==ACTION_CONTINUE)
		{
			inputID = sampleCopy->GetInputID(actuallyUsedInput);
		}

		boost::shared_ptr<TreeNode> vnode;
		if (!qnode->GetChild(observationID, inputID, vnode) && NodeData(qnode)->GetValueCount()>=1)
		{
			if (!bTerminal)
			{
				vnode = NewNode();
				qnode->AddChild(observationID, inputID, vnode);
			}
		}

		double delayedReward = 0.0;

		if (!bTerminal)
		{
			if (vnode)
				delayedReward = SimulateV(sampleCopy, vnode, treeDepth + 1, iAllocatedState++, iAllocatedObservation++);
			else
				delayedReward = Rollout(sampleCopy, treeDepth + 1);
		}

		totalRewardSum += immediateReward + Options.DiscountFactor * delayedReward;
	}

	double averageTotalReward = totalRewardSum/possibleStateInputs.size();
	NodeData(qnode)->AddValue(averageTotalReward);

	return averageTotalReward;
}

/*double POMCP::SimulateQ(boost::shared_ptr<State> sample, boost::shared_ptr<TreeNode> qnode, MONITOR_ACTION_TYPE action, unsigned int treeDepth)
{
	double immediateReward;
	MONITOR_ACTION_EVALUATION evaluation;

	EvaluateAction(sample, action, immediateReward, evaluation);

	boost::shared_ptr<ModelInput> dummyInput;
	if (IsInputFunctionKnown())
	{
		dummyInput = GetDecisionSystem()->GetSimulationModel()->GetInputAt(sample);
	}

	bool bTerminal = GetDecisionSystem()->GetSimulationModel()->Step(sample, action, dummyInput);

	boost::shared_ptr<ModelObservation> observation = GetDecisionSystem()->GetSimulationModel()->CreateNewObservation();
	GetDecisionSystem()->GetSimulationModel()->GetStateObservation(sample, observation);

	unsigned long observationID = sample->GetObservationID(observation);

	boost::shared_ptr<TreeNode> vnode;
	if (!qnode->GetChild(observationID, vnode) && NodeData(qnode)->GetValueCount()>=1)
	{
		if (!bTerminal)
		{
			vnode = NewNode();
			qnode->AddChild(observationID, vnode);
		}
	}

	double delayedReward = 0.0;

	if (!bTerminal)
	{
		if (vnode)
			delayedReward = SimulateV(sample, vnode, treeDepth + 1);
		else
			delayedReward = Rollout(sample, treeDepth + 1);
	}

	double totalReward = immediateReward + Options.DiscountFactor * delayedReward;
	NodeData(qnode)->AddValue(totalReward);

	return totalReward;
}*/

MONITOR_ACTION_TYPE POMCP::GreedyUCB(boost::shared_ptr<TreeNode> valueNode, UCB_STATE_INFO stateInfo, bool bIgnoreUnattemptedAction, bool bFastUCB, double& rValueContinue, double& rValueAlarm)
{
	rValueContinue = 0;
	rValueAlarm = 0;

	unsigned int parentValueCount = NodeData(valueNode)->GetValueCount();
	double logN = log((double) parentValueCount+1);

	double continueValue = 0;
	unsigned int continueValueCount = 0;

	double alarmValue = 0;
	unsigned int alarmValueCount = 0;

	boost::shared_ptr<TreeNode> continueNode;
	if (valueNode->GetChild(ACTION_CONTINUE, 0, continueNode))
	{
		continueValue = NodeData(continueNode)->GetValue();
		continueValueCount = NodeData(continueNode)->GetValueCount();
	}

	if (Options.bContinueOnly)
	{
		rValueContinue = continueValue;
		return ACTION_CONTINUE;
	}

	boost::shared_ptr<TreeNode> alarmNode;
	if (valueNode->GetChild(ACTION_ALARM, 0, alarmNode))
	{
		alarmValue = NodeData(alarmNode)->GetValue();
		alarmValueCount = NodeData(alarmNode)->GetValueCount();
	}

	double continueV = continueValue;
	double alarmV = alarmValue;

	if (bFastUCB)
	{
		continueV += FastUCB(parentValueCount, continueValueCount, logN);
		alarmV += FastUCB(parentValueCount, alarmValueCount, logN);
	}

	if (bIgnoreUnattemptedAction)
	{
		if (continueValueCount==0)
			continueV = -DBL_MAX;

		if (alarmValueCount==0)
			alarmV = -DBL_MAX;
	}

	rValueContinue = continueV;
	rValueAlarm = alarmV;

	if (fabs(continueV-alarmV)<1e-6)
	{
		switch (stateInfo)
		{
		case UCB_CONSISTENT:
			return ACTION_CONTINUE;
			break;
		case UCB_FAILURE:
			return ACTION_ALARM;
			break;
		case UCB_UNDEFINED:
			return GetDecisionSystem()->RandomAction();
			break;
		}
	}
	else
	if (continueV>alarmV)
	{
		return ACTION_CONTINUE;
	}
	else
	if (alarmV>continueV)
	{
		return ACTION_ALARM;
	}

	return ACTION_CONTINUE;
}

double POMCP::FastUCB(int N, int n, double logN)
{
    if (n == 0)
        return Infinity;
    else
        return Options.ExplorationConstant * sqrt(logN / n);
}

/*virtual*/ void POMCP::EvaluateAction(boost::shared_ptr<State> state, MONITOR_ACTION_TYPE action, unsigned int treeDepth, double& reward, MONITOR_ACTION_EVALUATION& evaluation)
{
	GetDecisionSystem()->GetSimulationModel()->EvaluateAction(state, action, evaluation);
	reward = GetDecisionSystem()->GetSimulationModel()->RewardAction(state, action, Options.Rewards);
}

/*virtual*/ unsigned long POMCP::GetStateObservationID(boost::shared_ptr<State> state)
{
	boost::shared_ptr<ModelObservation> observation = GetDecisionSystem()->GetSimulationModel()->CreateNewObservation();

	GetDecisionSystem()->GetSimulationModel()->GetStateObservation(state, observation);
	return state->GetObservationID(observation);
}

double POMCP::Rollout(boost::shared_ptr<State> sample, unsigned int treeDepth)
{
	bool bTerminal = false;
	double immediateReward = 0.0;
	double totalReward = 0.0;
	double discountFactor = 1.0;

	for (unsigned int iSteps = 0; iSteps + treeDepth < Options.MaxTreeDepth && !bTerminal; iSteps++)
	{
		MONITOR_ACTION_TYPE action = GetDecisionSystem()->RandomAction();

		// pretend that system is fully observable as random action does not make sense here
		if (sample->IsFailureState())
		{
			action = ACTION_ALARM;
		}
		else
		{
			action = ACTION_CONTINUE;
		}

		MONITOR_ACTION_EVALUATION evaluation;

		EvaluateAction(sample, action, treeDepth, immediateReward, evaluation);

		boost::shared_ptr<ModelInput> dummyInput;
		boost::shared_ptr<ModelInput> actualInput;

		if (IsInputFunctionKnown())
		{
			dummyInput = GetDecisionSystem()->GetSimulationModel()->GetInputAt(sample);
		}

		bTerminal = GetDecisionSystem()->GetSimulationModel()->Step(sample, action, dummyInput, actualInput);

		totalReward += immediateReward * discountFactor;
		discountFactor *= Options.DiscountFactor;
	}

	return totalReward;
}

boost::shared_ptr<TreeNode> POMCP::NewNode()
{
	/*if (AllocatedTreeNodes.size()!=0)
	{
		iAllocatedTreeNode++;
	}

	unsigned int oldSize = AllocatedTreeNodes.size();
	if (AllocatedTreeNodes.size()==0 || iAllocatedTreeNode==AllocatedTreeNodes.end())
	{
		for (int i=0; i<1000; i++)
		{
			boost::shared_ptr<TreeNode> node = TreeNodesFactory.Create();
			node->Data = NodeDataFactory.Create();

			AllocatedTreeNodes.push_back(node);
		}

		iAllocatedTreeNode = AllocatedTreeNodes.begin() + oldSize;
	}

	return *iAllocatedTreeNode;*/

	boost::shared_ptr<TreeNode> node = TreeNodesFactory.Create();
	node->Data = NodeDataFactory.Create();

	return node;
}

boost::shared_ptr<MCTSNodeData> POMCP::NodeData(boost::shared_ptr<TreeNode> node)
{
	boost::shared_ptr<MCTSNodeData> data = boost::static_pointer_cast<MCTSNodeData>(node->Data);
	return data;
}

POMCPOptions& POMCP::GetOptions()
{
	return Options;
}

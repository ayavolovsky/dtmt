/*
 * main.cpp
 *
 *  Created on: Feb 12, 2017
 *      Author: ayavol2
 */

#include <boost/shared_ptr.hpp>
#include "POMCP.h"

using namespace boost;

// `extern "C"` - specifies C linkage: forces the compiler to export function/variable by a pretty (unmangled) C name.
#define API extern "C" BOOST_SYMBOL_EXPORT

boost::shared_ptr<DecisionMaker> CreateDecisionMaker()
{
	boost::shared_ptr<POMCP> dm = boost::shared_ptr<POMCP>(new POMCP());
	return dm;
}




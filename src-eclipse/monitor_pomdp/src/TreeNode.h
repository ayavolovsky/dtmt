/*
 * TreeNode.h
 *
 *  Created on: May 17, 2013
 *      Author: ayavol2
 */

#ifndef TREENODE_H_
#define TREENODE_H_

#include <map>
#include <string>
#include <boost/shared_ptr.hpp>

class TreeData;

class TreeNode {
public:
	TreeNode();
	virtual ~TreeNode();

	void AddChild(unsigned int id1, unsigned int id2, boost::shared_ptr<TreeNode> node);

	bool GetChild(unsigned int id1, unsigned int id2, boost::shared_ptr<TreeNode>& child);

	void RemoveChild(unsigned int id1, unsigned int id2);
	void RemoveChild1(unsigned int id1);

	void RemoveAllChildren();

	std::string ToString(const std::string& indent);
public:
	boost::shared_ptr<TreeData> Data;
	std::map<unsigned int, std::map<unsigned int, boost::shared_ptr<TreeNode> > > Children;
};

#endif /* TREENODE_H_ */

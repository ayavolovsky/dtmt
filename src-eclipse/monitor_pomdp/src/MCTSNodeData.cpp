/*
 * MCTSNodeData.cpp
 *
 *  Created on: May 17, 2013
 *      Author: ayavol2
 */

#include "MCTSNodeData.h"
#include <boost/format.hpp>

MCTSNodeData::MCTSNodeData() {
	ValueCount = 0;
	ValueSum = 0.0;
}

MCTSNodeData::~MCTSNodeData() {
	// TODO Auto-generated destructor stub
}

BeliefState& MCTSNodeData::GetBelief()
{
	return Belief;
}

void MCTSNodeData::SetBelief(const BeliefState& bel)
{
	Belief = bel;
}

void MCTSNodeData::AddValue(double val)
{
	ValueCount++;
	ValueSum += val;
}

double MCTSNodeData::GetValueSum()
{
	return ValueSum;
}

double MCTSNodeData::GetValue()
{
	return (ValueCount==0) ? 0.0 : ValueSum/ValueCount;
}

unsigned int MCTSNodeData::GetValueCount()
{
	return ValueCount;
}

/*virtual*/ std::string MCTSNodeData::ToString()
{
	std::string str = boost::str (boost::format("counter = (%1%), average value = (%2%)") 	% ValueCount
	                                                                          			% (ValueSum/ValueCount));
	return str;
}

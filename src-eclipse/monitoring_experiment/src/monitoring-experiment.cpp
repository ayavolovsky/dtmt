//============================================================================
// Name        : monitoring-pomcp.cpp
// Author      : Andrey Yavolovsky
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>

#include "CommandLineHandler.h"
#include "SolverOptions.h"
#include "Experiment.h"
#include "ReportGenerator.h"
#include "ExperimentGenerator.h"

using namespace std;

int main( int argc, const char* argv[] ) {
	SolverOptions options;
	if (!CommandLineHandler::Instance()->ParseCommandLine(argc, argv, options))
	{
		cerr << "Now exiting..." << endl;
		return 0;
	}

	switch (options.GetMode())
	{
	case SolverOptions::REPORT:
		{
			ReportGenerator report;
			report.CreateReport(options.GetReportFilePath());
		}
		break;
	case SolverOptions::EXPERIMENT:
		{
			ExperimentGenerator experiment(options);
			experiment.CreateExperimentSet();
		}
		break;
	default:
		{
			Experiment experiment(options, cout.rdbuf());
			experiment.Start();

		}
	}

	return 0;
}

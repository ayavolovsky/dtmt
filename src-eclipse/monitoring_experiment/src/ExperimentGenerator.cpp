/*
 * ExperimentGenerator.cpp
 *
 *  Created on: Jul 1, 2013
 *      Author: ayavol2
 */

#include "ExperimentGenerator.h"
#include <boost/filesystem.hpp>
#include <boost/format.hpp>
#include <iostream>
#include <algorithm>
#include <fstream>

using namespace std;
namespace fs = boost::filesystem;

ExperimentGenerator::ExperimentGenerator(SolverOptions& options) {
	Options = options;
}

ExperimentGenerator::~ExperimentGenerator() {

}

bool ExperimentGenerator::CreateExperimentSet()
{
	if (Options.GetMode()!=SolverOptions::EXPERIMENT)
	{
		return false;
	}

	MakeExperimentSetDir();

	switch (Options.GetExperimentSet())
	{
	case SolverOptions::EXPERIMENT_VARYING_PENALTIES:
		DefineExperiments_VaryingPenalties();
		break;
	case SolverOptions::EXPERIMENT_VARYING_CONSISTEMT_ALARM:
		DefineExperiments_VaryingConsistentFailure();
		break;
	case SolverOptions::EXPERIMENT_VARYING_FAILURE_CONTINUE:
		DefineExperiments_VaryingFailureContinue();
		break;
	case SolverOptions::EXPERIMENT_VARYING_THRESHOLD:
		DefineExperiments_VaryingThreashold();
		break;
	}

	SaveExperimentsSet();

	return true;
}

void ExperimentGenerator::SaveExperimentsSet()
{
	map<string, SolverOptions>::iterator iOptions;
	for (iOptions=experiments.begin(); iOptions!=experiments.end(); iOptions++)
	{
		if (!MakeExperimentDir(iOptions->first))
		{
			cout << "Failed to make directory for experiment " << iOptions->first << endl;
			continue;
		}

		if (!iOptions->second.SaveINI(GetExperimentIniFilePath(iOptions->first)))
		{
			cout << "Failed to save experiment " << iOptions->first << " setting to ini file." << endl;
			continue;
		}
	}

	cout << GetExperimentSetBatchFilePath() << endl;

	fstream file;
	file.open(GetExperimentSetBatchFilePath().c_str(), fstream::out);
	if (!file.is_open())
	{
		return;
	}

	if (Options.GetOperatingSystem()==SolverOptions::OS_LINUX)
	{
		file << "#!/bin/sh" << endl << endl;
	}

	for (std::vector<std::string>::iterator iName  = experimentsOrder.begin(); iName!=experimentsOrder.end(); iName++)
	{
		fs::path iniPath(*iName);
		iniPath /= "config.ini";

		switch (Options.GetOperatingSystem())
		{
		case SolverOptions::OS_WINDOWS:
			file << "monitoring-pomcp.exe ";
			break;
		case SolverOptions::OS_LINUX:
			file << "./monitoring-pomcp ";
			break;
		}

		file << "--config " << iniPath.string() << endl;
	}

	file.close();
}

void ExperimentGenerator::DefineExperiments_VaryingPenalties()
{
	SolverOptions basicSettings = Options;
	basicSettings.SetMode("calculation");

	basicSettings.Rewards.GoodAlarm = 0;
	basicSettings.Rewards.BadContinue = 0;
	basicSettings.ExperimentDirPath = "penalty0";
	basicSettings.AutoSetupPOMCPExploration();
	experiments["penalty0"] = basicSettings;
	experimentsOrder.push_back("penalty0");

	basicSettings.Rewards.GoodAlarm = -1;
	basicSettings.Rewards.BadContinue = -1;
	basicSettings.ExperimentDirPath = "penalty-1";
	basicSettings.AutoSetupPOMCPExploration();
	experiments["penalty-1"] = basicSettings;
	experimentsOrder.push_back("penalty-1");

	basicSettings.Rewards.GoodAlarm = -10;
	basicSettings.Rewards.BadContinue = -10;
	basicSettings.ExperimentDirPath = "penalty-10";
	basicSettings.AutoSetupPOMCPExploration();
	experiments["penalty-10"] = basicSettings;
	experimentsOrder.push_back("penalty-10");

	basicSettings.Rewards.GoodAlarm = -50;
	basicSettings.Rewards.BadContinue = -50;
	basicSettings.ExperimentDirPath = "penalty-50";
	basicSettings.AutoSetupPOMCPExploration();
	experiments["penalty-50"] = basicSettings;
	experimentsOrder.push_back("penalty-50");

	for (int k = -100; k>=-1000; k-=100)
	{
		basicSettings.Rewards.GoodAlarm = k;
		basicSettings.Rewards.BadContinue = k;
		string label = boost::str(boost::format("penalty%1%") % k);
		basicSettings.ExperimentDirPath = label;
		basicSettings.AutoSetupPOMCPExploration();
		experiments[label] = basicSettings;
		experimentsOrder.push_back(label);
	}

	for (int k = -1500; k>=-10000; k-=500)
	{
		basicSettings.Rewards.GoodAlarm = k;
		basicSettings.Rewards.BadContinue = k;
		string label = boost::str(boost::format("penalty%1%") % k);
		basicSettings.ExperimentDirPath = label;
		basicSettings.AutoSetupPOMCPExploration();
		experiments[label] = basicSettings;
		experimentsOrder.push_back(label);
	}

	for (int k = -11000; k>=-20000; k-=1000)
	{
		basicSettings.Rewards.GoodAlarm = k;
		basicSettings.Rewards.BadContinue = k;
		string label = boost::str(boost::format("penalty%1%") % k);
		basicSettings.ExperimentDirPath = label;
		basicSettings.AutoSetupPOMCPExploration();
		experiments[label] = basicSettings;
		experimentsOrder.push_back(label);
	}

	for (int k = -30000; k>=-50000; k-=10000)
	{
		basicSettings.Rewards.GoodAlarm = k;
		basicSettings.Rewards.BadContinue = k;
		string label = boost::str(boost::format("penalty%1%") % k);
		basicSettings.ExperimentDirPath = label;
		basicSettings.AutoSetupPOMCPExploration();
		experiments[label] = basicSettings;
		experimentsOrder.push_back(label);
	}
}

void ExperimentGenerator::DefineExperiments_VaryingConsistentFailure()
{
	SolverOptions basicSettings = Options;
	basicSettings.SetMode("calculation");

	for (int k = 0; k>=-100; k-=10)
	{
		basicSettings.Rewards.GoodAlarm = k;
		string label = boost::str(boost::format("consistentFailure%1%") % k);
		basicSettings.ExperimentDirPath = label;
		basicSettings.AutoSetupPOMCPExploration();
		experiments[label] = basicSettings;
		experimentsOrder.push_back(label);
	}


	/*basicSettings.Rewards.RewardConsistentAlarm = 0;
	basicSettings.ExperimentDirPath ="consistentFailure0";
	basicSettings.AutoSetupPOMCPExploration();
	experiments["consistentFailure0"] = basicSettings;
	experimentsOrder.push_back("consistentFailure0");

	basicSettings.Rewards.RewardConsistentAlarm = -0.01;
	basicSettings.ExperimentDirPath = "consistentFailure-0_01";
	basicSettings.AutoSetupPOMCPExploration();
	experiments["consistentFailure-0_01"] = basicSettings;
	experimentsOrder.push_back("consistentFailure-0_01");

	basicSettings.Rewards.RewardConsistentAlarm = -0.025;
	basicSettings.ExperimentDirPath = "consistentFailure-0_025";
	basicSettings.AutoSetupPOMCPExploration();
	experiments["consistentFailure-0_025"] = basicSettings;
	experimentsOrder.push_back("consistentFailure-0_025");

	basicSettings.Rewards.RewardConsistentAlarm = -0.05;
	basicSettings.ExperimentDirPath = "consistentFailure-0_05";
	basicSettings.AutoSetupPOMCPExploration();
	experiments["consistentFailure-0_05"] = basicSettings;
	experimentsOrder.push_back("consistentFailure-0_05");

	basicSettings.Rewards.RewardConsistentAlarm = -0.1;
	basicSettings.ExperimentDirPath = "consistentFailure-0_1";
	basicSettings.AutoSetupPOMCPExploration();
	experiments["consistentFailure-50"] = basicSettings;
	experimentsOrder.push_back("consistentFailure-50");

	basicSettings.Rewards.RewardConsistentAlarm = -0.2;
	basicSettings.ExperimentDirPath = "consistentFailure-0_2";
	basicSettings.AutoSetupPOMCPExploration();
	experiments["consistentFailure-0_2"] = basicSettings;
	experimentsOrder.push_back("consistentFailure-0_2");

	basicSettings.Rewards.RewardConsistentAlarm = -0.3;
	basicSettings.ExperimentDirPath = "consistentFailure-0_3";
	basicSettings.AutoSetupPOMCPExploration();
	experiments["consistentFailure-0_3"] = basicSettings;
	experimentsOrder.push_back("consistentFailure-0_3");

	basicSettings.Rewards.RewardConsistentAlarm = -0.5;
	basicSettings.ExperimentDirPath = "consistentFailure-0_5";
	basicSettings.AutoSetupPOMCPExploration();
	experiments["consistentFailure-0_5"] = basicSettings;
	experimentsOrder.push_back("consistentFailure-0_5");

	basicSettings.Rewards.RewardConsistentAlarm = -0.8;
	basicSettings.ExperimentDirPath = "consistentFailure-0_8";
	basicSettings.AutoSetupPOMCPExploration();
	experiments["consistentFailure-0_8"] = basicSettings;
	experimentsOrder.push_back("consistentFailure-0_8");

	basicSettings.Rewards.RewardConsistentAlarm = -1;
	basicSettings.ExperimentDirPath = "consistentFailure-1";
	basicSettings.AutoSetupPOMCPExploration();
	experiments["consistentFailure-1"] = basicSettings;
	experimentsOrder.push_back("consistentFailure-1");*/

	/*
	for (int k = -100; k>=-1000; k-=100)
	{
		basicSettings.Rewards.RewardConsistentAlarm = k;
		string label = boost::str(boost::format("consistentFailure%1%") % k);
		basicSettings.ExperimentDirPath = label;
		basicSettings.AutoSetupPOMCPExploration();
		experiments[label] = basicSettings;
		experimentsOrder.push_back(label);
	}

	for (int k = -1500; k>=-2500; k-=500)
	//for (int k = -1500; k>=-10000; k-=500)
	{
		basicSettings.Rewards.RewardConsistentAlarm = k;
		string label = boost::str(boost::format("consistentFailure%1%") % k);
		basicSettings.ExperimentDirPath = label;
		basicSettings.AutoSetupPOMCPExploration();
		experiments[label] = basicSettings;
		experimentsOrder.push_back(label);
	}

	for (int k = -11000; k>=-20000; k-=1000)
	{
		basicSettings.Rewards.RewardConsistentAlarm = k;
		string label = boost::str(boost::format("consistentFailure%1%") % k);
		basicSettings.ExperimentDirPath = label;
		basicSettings.AutoSetupPOMCPExploration();
		experiments[label] = basicSettings;
		experimentsOrder.push_back(label);
	}

	for (int k = -30000; k>=-50000; k-=10000)
	{
		basicSettings.Rewards.RewardConsistentAlarm = k;
		string label = boost::str(boost::format("consistentFailure%1%") % k);
		basicSettings.ExperimentDirPath = label;
		basicSettings.AutoSetupPOMCPExploration();
		experiments[label] = basicSettings;
		experimentsOrder.push_back(label);
	}*/
}

void ExperimentGenerator::DefineExperiments_VaryingFailureContinue()
{
	SolverOptions basicSettings = Options;
	basicSettings.SetMode("calculation");

	/*basicSettings.Rewards.RewardFailureContinue = 0;
	basicSettings.ExperimentDirPath = "failureContinue0";
	basicSettings.AutoSetupPOMCPExploration();
	experiments["failureContinue0"] = basicSettings;
	experimentsOrder.push_back("failureContinue0");

	basicSettings.Rewards.RewardFailureContinue = -1;
	basicSettings.ExperimentDirPath = "failureContinue-1";
	basicSettings.AutoSetupPOMCPExploration();
	experiments["failureContinue-1"] = basicSettings;
	experimentsOrder.push_back("failureContinue-1");

	basicSettings.Rewards.RewardFailureContinue = -10;
	basicSettings.ExperimentDirPath = "failureContinue-10";
	basicSettings.AutoSetupPOMCPExploration();
	experiments["failureContinue-10"] = basicSettings;
	experimentsOrder.push_back("failureContinue-10");

	basicSettings.Rewards.RewardFailureContinue = -50;
	basicSettings.ExperimentDirPath = "failureContinue-50";
	basicSettings.AutoSetupPOMCPExploration();
	experiments["failureContinue-50"] = basicSettings;
	experimentsOrder.push_back("failureContinue-50");*/

	//for (int k = -100; k>=-1000; k-=100)


	/*for (int k = 0; k>=-30; k-=3)
	{
		basicSettings.Rewards.RewardFailureContinue = k;
		string label = boost::str(boost::format("failureContinue%1%") % k);
		basicSettings.ExperimentDirPath = label;
		basicSettings.AutoSetupPOMCPExploration();
		experiments[label] = basicSettings;
		experimentsOrder.push_back(label);
	}*/

	for (int k = 0; k>=-380; k-=30)
	{
		basicSettings.Rewards.BadContinue = k;
		string label = boost::str(boost::format("failureContinue%1%") % k);
		basicSettings.ExperimentDirPath = label;
		basicSettings.AutoSetupPOMCPExploration();
		experiments[label] = basicSettings;
		experimentsOrder.push_back(label);
	}


	/*for (int k = -1500; k>=-2500; k-=500)
	//for (int k = -1500; k>=-10000; k-=500)
	{
		basicSettings.Rewards.RewardFailureContinue = k;
		string label = boost::str(boost::format("failureContinue%1%") % k);
		basicSettings.ExperimentDirPath = label;
		basicSettings.AutoSetupPOMCPExploration();
		experiments[label] = basicSettings;
		experimentsOrder.push_back(label);
	}*/
}

void ExperimentGenerator::DefineExperiments_VaryingThreashold()
{
	/*SolverOptions basicSettings = Options;
	basicSettings.SetMode("calculation");
	basicSettings.SetDecisionMaker("threshold");
	basicSettings.SetCompareDecisionMaker("none");

	for (double k = 1; k>=0.05; k-=0.02)
	{
		basicSettings.ThresholdValue = k;
		string label = boost::str(boost::format("threshold%1%") % k);
		basicSettings.ExperimentDirPath = label;

		experiments[label] = basicSettings;
		experimentsOrder.push_back(label);
	}*/
}

bool ExperimentGenerator::MakeExperimentSetDir()
{
	fs::path DirPath(GetExperimentSetDir());

	try {
		fs::create_directory(DirPath);
	}
	catch (const boost::filesystem::filesystem_error& e)
	{
		return false;
	}

	return true;
}

std::string ExperimentGenerator::GetExperimentSetDir()
{
	return Options.GetExperimentDir();
}

std::string ExperimentGenerator::GetExperimentDir(const std::string& experimentName)
{
	fs::path dirPath(GetExperimentSetDir());
	dirPath /= experimentName;

	return dirPath.string();
}

bool ExperimentGenerator::MakeExperimentDir(const std::string& experimentName)
{
	fs::path DirPath(GetExperimentDir(experimentName));

	try {
		fs::create_directory(DirPath);
	}
	catch (const boost::filesystem::filesystem_error& e)
	{
		return false;
	}

	return true;
}

std::string ExperimentGenerator::GetExperimentSetBatchFilePath()
{
	fs::path dirPath(GetExperimentSetDir());

	switch (Options.GetOperatingSystem())
	{
	case SolverOptions::OS_WINDOWS:
		dirPath /= "run.bat";
		break;
	case SolverOptions::OS_LINUX:
		dirPath /= "run.sh";
		break;
	}

	return dirPath.string();
}

std::string ExperimentGenerator::GetExperimentIniFilePath(const std::string& experimentName)
{
	fs::path iniPath(GetExperimentDir(experimentName));
	iniPath /= "config.ini";

	return iniPath.string();
}

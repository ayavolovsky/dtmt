/*
 * ReportGenerator.cpp
 *
 *  Created on: Jun 14, 2013
 *      Author: ayavol2
 */

#include "ReportGenerator.h"
#include "CommandLineHandler.h"
#include <fstream>
#include <iostream>
#include <boost/filesystem.hpp>
#include <boost/tokenizer.hpp>
#include <boost/format.hpp>

using namespace std;
namespace fs = boost::filesystem;

ReportGenerator::ReportGenerator() {
	// TODO Auto-generated constructor stub

}

ReportGenerator::~ReportGenerator() {
	// TODO Auto-generated destructor stub
}

bool ReportGenerator::CreateReport(const std::string& reportFilePath)
{
	fstream file;
	file.open(reportFilePath.c_str(), ios::out);
	if (!file.is_open())
	{
		return false;
	}

	// looking in the current directory
	fs::path searchDir("./");

	ExperimentResults.clear();
	ExperimentOptions.clear();
	ParametersOrder.clear();
	ExperimentsOrder.clear();

	try
	{
		if (!fs::exists(searchDir) || !fs::is_directory(searchDir))
		{
			return false;
		}

		fs::directory_iterator end_iter;
	    for ( fs::directory_iterator dir_itr( searchDir );
	    		dir_itr != end_iter;
	    		++dir_itr )
	    {
	    	// need to check only sub-directories
	    	if (!fs::is_directory(dir_itr->status()))
	    	{
	    		continue;
	    	}



	    	fs::path optionsFilePath(dir_itr->path());
	    	optionsFilePath /= "options.csv";

	    	int i = 0;
	    	fs::path outputFilePath(dir_itr->path());
	    	outputFilePath /= boost::str(boost::format("output%1%.csv") % i);
	    	bool bFileExists = fs::exists(outputFilePath);

	    	while (bFileExists)
	    	{
	    		string experimentName = boost::str(boost::format("%1%_%2%") % dir_itr->path().filename().string() % i);

		    	if (fs::exists(optionsFilePath) && fs::exists(outputFilePath))
		    	{
		    		ExperimentsOrder.push_back(experimentName);
		    		ReadExperimentResults(experimentName, optionsFilePath.string(), outputFilePath.string());
		    	}

		    	i++;

		    	outputFilePath = fs::path(dir_itr->path());
		    	outputFilePath /= boost::str(boost::format("output%1%.csv") % i);
		    	bFileExists = fs::exists(outputFilePath);


	    	}

	    }
	}
	catch (const fs::filesystem_error& ex)
	{
		cout << ex.what() << "\n";
		return false;
	}

	SaveReport(file);

	file.close();

	return true;
}

bool ReportGenerator::ReadExperimentResults(const string& experimentName, const string& optionsFilePath, const string& outFilePath)
{
	map<string, string> optionsParams;
	list<string> optionsParamsOrder;
	if (!ParseCSVFile(optionsFilePath, optionsParams, optionsParamsOrder))
	{
		cout << "Failed to read options for the experiment " << experimentName << "\n";
		return false;
	}

	ExperimentOptions[experimentName] = optionsParams;

	map<string, string> outputParams;
	list<string> outputParamsOrder;
	if (!ParseCSVFile(outFilePath, outputParams, outputParamsOrder))
	{
		cout << "Failed to read output data for the experiment " << experimentName << "\n";
		return false;
	}

	if (ParametersOrder.empty())
	{
		ParametersOrder = outputParamsOrder;
	}

	ExperimentResults[experimentName] = outputParams;

	return true;
}

bool ReportGenerator::ParseCSVFile(const std::string& filePath, map<string, string>& csvParams, std::list<std::string>& paramsOrder)
{
	fstream file;
	file.open(filePath.c_str(), ios::in);

	if (!file.is_open())
	{
		return false;
	}

	while (!file.eof())
	{
		string line;
		getline(file, line);

		typedef boost::tokenizer< boost::escaped_list_separator<char> > Tokenizer;
		vector< string > parts;

		try
		{
			Tokenizer tok(line);
			parts.assign(tok.begin(),tok.end());
		}
		catch (boost::escaped_list_error& ex)
		{
			continue;
		}

		if (parts.size()!=2)	// parameter + value
		{
			continue;
		}

		csvParams[parts.at(0)] = parts.at(1);

		paramsOrder.push_back(parts.at(0));
	}

	return true;
}

inline std::string trim(std::string& str)
{
str.erase(0, str.find_first_not_of(' '));       //prefixing spaces
str.erase(str.find_last_not_of(' ')+1);         //surfixing spaces
return str;
}

bool ReportGenerator::SaveReport(std::fstream& f)
{
	// header
	f << "Experiment name" << ",";
	f << "Decision maker type" << ",";
	f << "ConsistentContinue" << ",";
	f << "ConsistentAlarm" << ",";
	f << "FailureContinue" << ",";
	f << "FailureAlarm" << ",";
	for (list<std::string>::iterator iParameter = ParametersOrder.begin(); iParameter!=ParametersOrder.end(); iParameter++)
	{
		f << *iParameter << ",";
	}
	f << "ThresholdValue" << ",";
	f << "\n";

	// contents

	for (list<string>::iterator iExperiment = ExperimentsOrder.begin(); iExperiment!=ExperimentsOrder.end(); iExperiment++)
	{
		map<string , map<string, string> >::iterator iOptionsData = ExperimentOptions.find(*iExperiment);
		map<string , map<string, string> >::iterator iResultsData = ExperimentResults.find(*iExperiment);

		if (iOptionsData==ExperimentOptions.end() || iResultsData==ExperimentResults.end())
		{
			continue;
		}

		f << *iExperiment << ",";
		f << trim(iOptionsData->second.at("DecisionMaker")) << ",";
		f << trim(iOptionsData->second.at("Rewards.RewardConsistentContinue")) << ",";
		f << trim(iOptionsData->second.at("Rewards.RewardConsistentAlarm")) << ",";
		f << trim(iOptionsData->second.at("Rewards.RewardFailureContinue")) << ",";
		f << trim(iOptionsData->second.at("Rewards.RewardFailureAlarm")) << ",";

		for (list<std::string>::iterator iParameter = ParametersOrder.begin(); iParameter!=ParametersOrder.end(); iParameter++)
		{
			map<string, string>::iterator paramValue = iResultsData->second.find(*iParameter);
			if (paramValue==iResultsData->second.end())
			{
				f << "" << ",";
			}
			else
			{
				f << paramValue->second << ",";
			}
		}

		f << trim(iOptionsData->second.at("ThresholdValue")) << ",";

		f << "\n";
	}

	return true;
}

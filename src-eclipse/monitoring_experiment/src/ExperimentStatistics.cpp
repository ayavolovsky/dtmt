/*
 * ExperimentStatistics.cpp
 *
 *  Created on: May 21, 2013
 *      Author: ayavol2
 */

#include "ExperimentStatistics.h"
#include <iostream>
#include <boost/format.hpp>
#include <sstream>
#include <fstream>
#include <climits>

using namespace std;

ExperimentStatistics::ExperimentStatistics() {
}

ExperimentStatistics::~ExperimentStatistics() {
	// TODO Auto-generated destructor stub
}

void ExperimentStatistics::Clear()
{
	DecisionMakerEvaluations.clear();
	bEvaluationStatisticaValid.clear();
}

void ExperimentStatistics::AddEvaluation(const std::string& trialID, bool bGoodTrial, /*TMP*/vector<double>& deltaRange, double maxRej2Increment, double maxRej2Prob, int iDecisionMaker, const std::string& decisionMakerName, MONITOR_ACTION_EVALUATION evaluation, int earlyAlarmPredictionTime, double failureBelief, int timeFailureDetection, long totalReward, double totalPenalty, bool bMissedAlarmBeforeTerminal)
{
	TrialStatistics statistics;
	statistics.evaluation = evaluation;
	statistics.goodTrial = bGoodTrial;

	statistics.deltaRange = deltaRange;
	statistics.maxRej2Increment = maxRej2Increment;
	statistics.maxRej2Prob = maxRej2Prob;
	statistics.earlyAlarmPredictionTime = earlyAlarmPredictionTime;
	statistics.failureBelief = failureBelief;
	statistics.failureDetectionTime = timeFailureDetection;
	statistics.totalReward = totalReward;
	statistics.totalPenalty = totalPenalty;
	statistics.missedAlarmOccured = bMissedAlarmBeforeTerminal;

	DecisionMakerEvaluations[iDecisionMaker][trialID].multiTrialData.push_back(statistics);
	bEvaluationStatisticaValid[iDecisionMaker] = false;
	DecisionMakerNamesByID[iDecisionMaker] = decisionMakerName;
}

bool ExperimentStatistics::SaveCSV(const std::string& filePath, int nDecisionMakerIndex, const std::string& decisionMakerName)
{
	ofstream file;
	file.open(filePath.c_str(), ios::out);
	if (!file.is_open())
	{
		return false;
	}

	CalculatedStatistics calcStats;
	map<MONITOR_ACTION_EVALUATION, vector<string> > evalStats;

	GetEvaluationStatistics(nDecisionMakerIndex, evalStats, calcStats);

	double consistent_pct = 0.0;
	double alarm_pct = 0.0;
	double early_alarm_pct = 0.0;
	double false_alarm_pct = 0.0;
	double missed_alarm_pct = 0.0;

	if (calcStats.totalGoodTrials!=0)
	{
		consistent_pct = 1.0 * evalStats[ACTION_EVAL_CORRECT_CONTINUE].size() / calcStats.totalGoodTrials;
	}

	if (calcStats.totalBadTrials!=0)
	{
		alarm_pct = 1.0 * evalStats[ACTION_EVAL_CORRECT_ALARM].size() / calcStats.totalBadTrials;
		early_alarm_pct = 1.0 * evalStats[ACTION_EVAL_EARLY_ALARM].size() / calcStats.totalBadTrials;
		missed_alarm_pct = 1.0 * evalStats[ACTION_EVAL_MISSED_ALARM].size() / calcStats.totalBadTrials;
	}

	if (calcStats.totalBadTrials + calcStats.totalGoodTrials!=0)
	{
		false_alarm_pct = 1.0 * evalStats[ACTION_EVAL_FALSE_ALARM].size() / (calcStats.totalBadTrials + calcStats.totalGoodTrials);
	}

	// the most important
	file << "DecisionMaker," << decisionMakerName << endl;
	file << "AA, " << AA(evalStats) << endl;
	file << "RA, " << RA(evalStats) << endl;
	file << "MTIME, " << calcStats.averageFailureDetectionTime << endl;
	file << "MTIME_mode, " << calcStats.modeFailureDetectionTime << endl;

	file << "FalseAlarms, " << evalStats[ACTION_EVAL_FALSE_ALARM].size() << endl;
	//file << "MissedAlarmTrials, " << 1.0*calcStats.totalTrialsWithMissedAlarm/calcStats.totalBadTrials << endl;

	file << "Good_MaxRej2Prob, " << calcStats.maxRej2ProbGood << endl;
	file << "Bad_MaxDelta, " << calcStats.maxBadDelta << endl;
	file << "MaxRej2Increment, " << calcStats.maxRej2Increment << endl;
	file << "FixedFalseByMaxBadDelta, " << calcStats.rateFixedFalseAlarmsByMaxBadDelta << endl;
	file << "AllGood_MinDelta, " << calcStats.minAllGoodDelta << endl;

	//file << "maxGood_to_bad_lower_bound, " << calcStats.maxGood_to_bad_lower_bound << endl;

	file << "AverageFailureBelief, " << calcStats.averageFailureBelief << endl;

	/*file << "BadRejectedTrialsByTime, " << calcStats.strBadRejectedTrialsByTime << endl;
	file << "RejectedEarly, " << calcStats.rejectedAtTimeCounter[-1] << endl;
	for (int i=0; i<7; i++)
	{
		map<int, unsigned int>::iterator iCounter = calcStats.rejectedAtTimeCounter.find(i);
		unsigned int nCounter = 0;
		if (iCounter!=calcStats.rejectedAtTimeCounter.end())
		{
			nCounter = iCounter->second;
		}
		file << "RejectedAt" << i << ", " << nCounter << endl;
	}

	file << "BadExecutionsWithRejectionOffset, " << calcStats.strBadTrialsWithAlarmOffset << endl;*/

	// other data
	file << "Good, " << calcStats.totalGoodTrials << endl;
	file << "Bad, " << calcStats.totalBadTrials << endl;
	file << "Trials, " << DecisionMakerEvaluations.at(nDecisionMakerIndex).size() << endl;

	file << "Consistent, " << evalStats[ACTION_EVAL_CORRECT_CONTINUE].size() << endl;
	file << "Consistent_pct, " << consistent_pct  << endl;

	file << "Alarms, " << evalStats[ACTION_EVAL_CORRECT_ALARM].size() << endl;
	file << "Alarms_pct, " << alarm_pct << endl;

	file << "EarlyAlarms, " << evalStats[ACTION_EVAL_EARLY_ALARM].size() << endl;
	file << "EarlyAlarms_pct, " << early_alarm_pct << endl;

	file << "FalseAlarms_pct, " << false_alarm_pct << endl;

	file << "MissedAlarms, " << evalStats[ACTION_EVAL_MISSED_ALARM].size() << endl;
	file << "MissedAlarms_pct, " << missed_alarm_pct << endl;

	/*file << "AverageEarlyPredictionTime, " << calcStats.averageEarlyPredictionTime << endl;
	file << "MinEarlyPredictionTime, " << calcStats.minEarlyPredictionTime << endl;
	file << "MaxEarlyPredictionTime, " << calcStats.maxEarlyPredictionTime << endl;*/

	file << "MinFailureBelief, " << calcStats.minFailureBelief << endl;
	file << "MinFailureBeliefTrialID, " << calcStats.minFailureBeliefTrialID << endl;
	file << "MaxFailureBelief, " << calcStats.maxFailureBelief << endl;
	file << "MaxFailureBeliefTrialID, " << calcStats.maxFailureBeliefTrialID << endl;

	file << "MinFailureDetectionTime, " << calcStats.minFailureDetectionTime << endl;
	file << "MinFailureDetectionTimeTrialID, " << calcStats.minFailureDetectionTimeTrialID << endl;
	file << "MaxFailureDetectionTime, " << calcStats.maxFailureDetectionTime << endl;
	file << "MaxFailureDetectionTimeTrialID, " << calcStats.maxFailureDetectionTimeTrialID << endl;

	/*file << "AverageTotalReward, " << calcStats.averageTotalReward << endl;
	file << "MinTotalReward, " << calcStats.minTotalReward << endl;
	file << "MaxTotalReward, " << calcStats.maxTotalReward << endl;

	file << "AverageTotalPenalty, " << calcStats.averageTotalPenalty << endl;
	file << "MinTotalPenalty, " << calcStats.minTotalPenalty << endl;
	file << "MaxTotalPenalty, " << calcStats.maxTotalPenalty << endl;*/

	file.close();

	return true;
}

bool ExperimentStatistics::AppendComparison_CSV(const std::string& filePath, ExperimentStatistics& other,  int nDecisionMakerIndex)
{
	ofstream file;
	file.open(filePath.c_str(), ios::app);
	if (!file.is_open())
	{
		return false;
	}

	CalculatedStatistics calcStats;
	map<MONITOR_ACTION_EVALUATION, vector<string> > evalStats;
	GetEvaluationStatistics(nDecisionMakerIndex, evalStats, calcStats);

	CalculatedStatistics other_calcStats;
	map<MONITOR_ACTION_EVALUATION, vector<string> > other_evalStats;
	other.GetEvaluationStatistics(nDecisionMakerIndex, other_evalStats, other_calcStats);

	file << "AverageRewardCompared, " << calcStats.averageTotalReward - other_calcStats.averageTotalReward << endl;

	file.close();

	return true;
}

bool ExperimentStatistics::AppendComparison_TXT(const std::string& filePath, ExperimentStatistics& other,  int nDecisionMakerIndex)
{
	ofstream file;
	file.open(filePath.c_str(), ios::app);
	if (!file.is_open())
	{
		return false;
	}

	CalculatedStatistics calcStats;
	map<MONITOR_ACTION_EVALUATION, vector<string> > evalStats;
	GetEvaluationStatistics(nDecisionMakerIndex, evalStats, calcStats);

	CalculatedStatistics other_calcStats;
	map<MONITOR_ACTION_EVALUATION, vector<string> > other_evalStats;
	other.GetEvaluationStatistics(nDecisionMakerIndex, other_evalStats, other_calcStats);

	file << "AverageRewardCompared = " << calcStats.averageTotalReward - other_calcStats.averageTotalReward << endl;

	file.close();

	return true;
}

bool ExperimentStatistics::SaveTXT(const std::string& filePath, int nDecisionMakerIndex, const std::string& decisionMakerName)
{
	ofstream file;
	file.open(filePath.c_str(), ios::out);
	if (!file.is_open())
	{
		return false;
	}

	file << ToString(nDecisionMakerIndex, decisionMakerName) << endl;

	file.close();

	return true;
}

std::string ExperimentStatistics::ToString(int nDecisionMakerIndex, const std::string& decisionMakerName)
{
	CalculatedStatistics calcStats;
	map<MONITOR_ACTION_EVALUATION, vector<string> > evalStats;
	GetEvaluationStatistics(nDecisionMakerIndex, evalStats, calcStats);

	double consistent_pct = 0.0;
	double alarm_pct = 0.0;
	double early_alarm_pct = 0.0;
	double false_alarm_pct = 0.0;
	double missed_alarm_pct = 0.0;

	if (calcStats.totalGoodTrials!=0)
	{
		consistent_pct = 1.0 * evalStats[ACTION_EVAL_CORRECT_CONTINUE].size() / calcStats.totalGoodTrials;
	}

	if (calcStats.totalBadTrials!=0)
	{
		alarm_pct = 1.0 * evalStats[ACTION_EVAL_CORRECT_ALARM].size() / calcStats.totalBadTrials;
		early_alarm_pct = 1.0 * evalStats[ACTION_EVAL_EARLY_ALARM].size() / calcStats.totalBadTrials;
		missed_alarm_pct = 1.0 * evalStats[ACTION_EVAL_MISSED_ALARM].size() / calcStats.totalBadTrials;
	}

	if (calcStats.totalBadTrials + calcStats.totalGoodTrials!=0)
	{
		false_alarm_pct = 1.0 * evalStats[ACTION_EVAL_FALSE_ALARM].size() / (calcStats.totalBadTrials + calcStats.totalGoodTrials);
	}

	stringstream ss;
	ss << "Decision maker name: " << decisionMakerName << "\n";
	ss << "Total number of trials: " << DecisionMakerEvaluations.at(nDecisionMakerIndex).size() << "\n";
	ss << "Total number of good trials: " << calcStats.totalGoodTrials << "\n";
	ss << "Total number of bad trials: " << calcStats.totalBadTrials << "\n";

	ss << "Trials by evaluation:" << "\n";
	//ss << "\t" << "CONSISTENT CONTINUE = " 	<< evalStats[ACTION_EVAL_CONSISTENT_CONTINUE].size() << "\n";
	ss << "\t" << "CONSISTENT CONTINUE % = " 	<< consistent_pct << "\n";

	//ss << "\t" << "CORRECT ALARM = " 		<< evalStats[ACTION_EVAL_CORRECT_ALARM].size() << "\n";
	ss << "\t" << "CORRECT ALARM % = " 		<< alarm_pct << "\n";

	//ss << "\t" << "FALSE ALARM = " 			<< evalStats[ACTION_EVAL_FALSE_ALARM].size() << "\n";
	ss << "\t" << "FALSE ALARM % = " 		<< false_alarm_pct << "\n";

	/*if (evalStats[ACTION_EVAL_FALSE_ALARM].size()!=0)
	{
		ss << "\t\t\t";
		for (unsigned int i=0; i<evalStats[ACTION_EVAL_FALSE_ALARM].size(); i++)
			ss << " " << evalStats[ACTION_EVAL_FALSE_ALARM].at(i);
		ss << "\n";
	}*/

	//ss << "\t" << "MISSED ALARM = " 		<< evalStats[ACTION_EVAL_MISSED_ALARM].size() << "\n";
	ss << "\t" << "MISSED ALARM % = " 		<< missed_alarm_pct << "\n";

	if (evalStats[ACTION_EVAL_MISSED_ALARM].size()!=0)
	{
		ss << "\t\t\t";
		for (unsigned int i=0; i<evalStats[ACTION_EVAL_MISSED_ALARM].size(); i++)
			ss << " " << evalStats[ACTION_EVAL_MISSED_ALARM].at(i);
		ss << "\n";
	}

	//ss << "\t" << "EARLY ALARM = " 			<< evalStats[ACTION_EVAL_EARLY_ALARM].size() << "\n";
	ss << "\t" << "EARLY ALARM % = " 		<< early_alarm_pct << "\n";

	/*if (evalStats[ACTION_EVAL_EARLY_ALARM].size()!=0)
	{
		ss << "\t\t\t";
		for (unsigned int i=0; i<evalStats[ACTION_EVAL_EARLY_ALARM].size(); i++)
			ss << " " << evalStats[ACTION_EVAL_EARLY_ALARM].at(i);
		ss << "\n";
	}*/

	ss << "Accuracy: " << "\n";
	ss << "\tAA = " << AA(evalStats) << "\n";
	ss << "\tRA = " << RA(evalStats) << "\n";

	//ss << "AverageEarlyPredictionTime = " << calcStats.averageEarlyPredictionTime << "\n";
	//ss << "MinEarlyPredictionTime = " << calcStats.minEarlyPredictionTime << "\n";
	//ss << "MaxEarlyPredictionTime = " << calcStats.maxEarlyPredictionTime << "\n";
	ss << "AverageFailureBelief = " << calcStats.averageFailureBelief << "\n";
	ss << "MinFailureBelief = " << calcStats.minFailureBelief << "\n";
	//ss << "MinFailureBeliefTrialID = " << calcStats.minFailureBeliefTrialID << "\n";
	ss << "MaxFailureBelief = " << calcStats.maxFailureBelief << "\n";
	//ss << "MaxFailureBeliefTrialID = " << calcStats.maxFailureBeliefTrialID << "\n";
	ss << "AverageFailureDetectionTime = " << calcStats.averageFailureDetectionTime << "\n";
	ss << "MinFailureDetectionTime = " << calcStats.minFailureDetectionTime << "\n";
	//ss << "MinFailureDetectionTimeTrialID = " << calcStats.minFailureDetectionTimeTrialID << "\n";
	ss << "MaxFailureDetectionTime = " << calcStats.maxFailureDetectionTime << "\n";
	//ss << "MaxFailureDetectionTimeTrialID = " << calcStats.maxFailureDetectionTimeTrialID << "\n";
	ss << "AverageTotalReward = " << calcStats.averageTotalReward << "\n";
	ss << "MinTotalReward = " << calcStats.minTotalReward << "\n";
	ss << "MaxTotalReward = " << calcStats.maxTotalReward << "\n";

	ss << "AverageTotalPenalty = " << calcStats.averageTotalPenalty << "\n";
	ss << "MinTotalPenalty = " << calcStats.minTotalPenalty << "\n";
	ss << "MaxTotalPenalty = " << calcStats.maxTotalPenalty /*<< "\n"*/;

	return ss.str();
}

double ExperimentStatistics::GetAverageFailureBelief(int nDecisionMakerIndex)
{
	// TODO: stupid implementation

	CalculatedStatistics calcStats;
	map<MONITOR_ACTION_EVALUATION, vector<string> > evalStats;
	GetEvaluationStatistics(nDecisionMakerIndex, evalStats, calcStats);

	return calcStats.averageFailureBelief;
}

void ExperimentStatistics::GetEvaluationStatistics(int decisionMakerID, std::map<MONITOR_ACTION_EVALUATION, std::vector<std::string> >& evaluationTrials, CalculatedStatistics& calcStats)
{
	if (bEvaluationStatisticaValid[decisionMakerID])
	{
		evaluationTrials = EvaluationTrials;
		calcStats = CalcStats;

		return;
	}

	//cout << "" << DecisionMakerNamesByID[decisionMakerID] << endl;

	calcStats.totalGoodTrials = 0;
	calcStats.totalBadTrials = 0;
	calcStats.totalTrialsWithMissedAlarm = 0;
	calcStats.averageEarlyPredictionTime = 0.0;
	calcStats.minEarlyPredictionTime = UINT_MAX;
	calcStats.maxEarlyPredictionTime = 0;
	calcStats.averageFailureBelief = 0.0;
	calcStats.minFailureBelief = 1.0;
	calcStats.maxFailureBelief = 0.0;
	calcStats.averageFailureDetectionTime = 0.0;
	calcStats.minFailureDetectionTime = INT_MAX;
	calcStats.maxFailureDetectionTime = INT_MIN;
	calcStats.averageTotalReward = 0.0;
	calcStats.minTotalReward = 1e30;
	calcStats.maxTotalReward = -1*1e30;
	calcStats.averageTotalPenalty = 0.0;
	calcStats.minTotalPenalty = 1e30;
	calcStats.maxTotalPenalty = -1*1e30;
	calcStats.maxBadDelta = 2;
	calcStats.minAllGoodDelta = -2;
	calcStats.maxRej2Increment = -1;
	calcStats.maxRej2ProbGood = -1;

	unsigned int totalEarlyPredictions = 0;
	unsigned int totalFailuresDetected = 0;
	unsigned int totalAlarmsAfterFailure = 0;

	double totalRewardsSum = 0;
	double totalPenaltySum = 0;

	map<int, map<string, MultiTrialStatistics > >::iterator iEvaluations = DecisionMakerEvaluations.find(decisionMakerID);
	if (iEvaluations==DecisionMakerEvaluations.end())
	{
		return;
	}

	map<string, MultiTrialStatistics >& Evaluations = iEvaluations->second;

	map<int, int> mtime_count;

	map<string, MultiTrialStatistics>::iterator iEvaluation = Evaluations.begin();
	for (; iEvaluation!=Evaluations.end(); iEvaluation++)
	{
		vector<TrialStatistics>::iterator iTrialStatistics = iEvaluation->second.multiTrialData.begin();
		double trialAverageReward = 0.0;
		double trialAveragePenalty = 0.0;

		for (; iTrialStatistics!=iEvaluation->second.multiTrialData.end(); iTrialStatistics++)
		{
			(iTrialStatistics->goodTrial) ? calcStats.totalGoodTrials++ : calcStats.totalBadTrials++;
			if (iTrialStatistics->missedAlarmOccured)
			{
				calcStats.totalTrialsWithMissedAlarm++;
			}

			if (iTrialStatistics->goodTrial && iTrialStatistics->deltaRange.size()==2 && iTrialStatistics->evaluation==ACTION_EVAL_FALSE_ALARM)
			{
				// minGoodDelta should be max over all iTrialStatistics->deltaRange.at(1)
				if (calcStats.minAllGoodDelta<iTrialStatistics->deltaRange.at(1))
				{
					calcStats.minAllGoodDelta = iTrialStatistics->deltaRange.at(1);
				}
			}

			if (!iTrialStatistics->goodTrial && iTrialStatistics->deltaRange.size()==2)
			{
				// maxBadDelta should be min over all iTrialStatistics->deltaRange.at(1)
				if (calcStats.maxBadDelta>iTrialStatistics->deltaRange.at(1))
				{
					calcStats.maxBadDelta = iTrialStatistics->deltaRange.at(1);
				}
			}

			if (calcStats.maxRej2Increment<iTrialStatistics->maxRej2Increment)
			{
				calcStats.maxRej2Increment = iTrialStatistics->maxRej2Increment;
			}

			if (iTrialStatistics->goodTrial && iTrialStatistics->evaluation==ACTION_EVAL_CORRECT_CONTINUE)
			{
				if (calcStats.maxRej2ProbGood<iTrialStatistics->maxRej2Prob)
				{
					calcStats.maxRej2ProbGood = iTrialStatistics->maxRej2Prob;
				}
			}

			/*if (iTrialStatistics->maxGood_to_bad_lower_bound>calcStats.maxGood_to_bad_lower_bound)
			{
				calcStats.maxGood_to_bad_lower_bound = iTrialStatistics->maxGood_to_bad_lower_bound;
			}*/

			// add evaluation for AA/RA statistics
			evaluationTrials[iTrialStatistics->evaluation].push_back(iEvaluation->first);

			trialAverageReward += iTrialStatistics->totalReward;
			if (iTrialStatistics->totalReward < calcStats.minTotalReward)
			{
				calcStats.minTotalReward = iTrialStatistics->totalReward;
			}

			if (iTrialStatistics->totalReward > calcStats.maxTotalReward)
			{
				calcStats.maxTotalReward = iTrialStatistics->totalReward;
			}

			trialAveragePenalty += iTrialStatistics->totalPenalty;
			if (iTrialStatistics->totalPenalty < calcStats.minTotalPenalty)
			{
				calcStats.minTotalPenalty = iTrialStatistics->totalPenalty;
			}

			if (iTrialStatistics->totalPenalty > calcStats.maxTotalPenalty)
			{
				calcStats.maxTotalPenalty = iTrialStatistics->totalPenalty;
			}

			/*if (iTrialStatistics->evaluation==ACTION_EVAL_EARLY_ALARM)
			{
				totalEarlyPredictions++;
				calcStats.averageEarlyPredictionTime += iTrialStatistics->earlyAlarmPredictionTime;

				if (iTrialStatistics->earlyAlarmPredictionTime<calcStats.minEarlyPredictionTime)
				{
					calcStats.minEarlyPredictionTime = iTrialStatistics->earlyAlarmPredictionTime;
				}

				if (iTrialStatistics->earlyAlarmPredictionTime>calcStats.maxEarlyPredictionTime)
				{
					calcStats.maxEarlyPredictionTime = iTrialStatistics->earlyAlarmPredictionTime;
				}
			}
			else*/
			if (iTrialStatistics->evaluation==ACTION_EVAL_CORRECT_ALARM || iTrialStatistics->evaluation==ACTION_EVAL_EARLY_ALARM)
			{
				totalFailuresDetected++;

				calcStats.averageFailureBelief += iTrialStatistics->failureBelief;


				if (calcStats.rejectedAtTimeCounter.find(iTrialStatistics->failureDetectionTime)==calcStats.rejectedAtTimeCounter.end())
				{
					calcStats.rejectedAtTimeCounter[iTrialStatistics->failureDetectionTime] = 0;
				}
				calcStats.rejectedAtTimeCounter[iTrialStatistics->failureDetectionTime]++;

				/*if (iTrialStatistics->failureDetectionTime!=-1)
				{*/
					calcStats.averageFailureDetectionTime += iTrialStatistics->failureDetectionTime;
					totalAlarmsAfterFailure++;
				//}

				if (iTrialStatistics->failureBelief<calcStats.minFailureBelief)
				{
					calcStats.minFailureBelief = iTrialStatistics->failureBelief;
					calcStats.minFailureBeliefTrialID = iEvaluation->first;
				}

				if (iTrialStatistics->failureBelief>calcStats.maxFailureBelief)
				{
					calcStats.maxFailureBelief = iTrialStatistics->failureBelief;
					calcStats.maxFailureBeliefTrialID = iEvaluation->first;
				}

				if (/*iTrialStatistics->failureDetectionTime!=-1 && */iTrialStatistics->failureDetectionTime<calcStats.minFailureDetectionTime)
				{
					calcStats.minFailureDetectionTime = iTrialStatistics->failureDetectionTime;
					calcStats.minFailureDetectionTimeTrialID = iEvaluation->first;
				}

				if (/*iTrialStatistics->failureDetectionTime!=-1 && */iTrialStatistics->failureDetectionTime>calcStats.maxFailureDetectionTime)
				{
					calcStats.maxFailureDetectionTime = iTrialStatistics->failureDetectionTime;
					calcStats.maxFailureDetectionTimeTrialID = iEvaluation->first;
				}

				if (mtime_count.find(iTrialStatistics->failureDetectionTime)==mtime_count.end())
				{
					mtime_count[iTrialStatistics->failureDetectionTime] = 0;
				}
				mtime_count[iTrialStatistics->failureDetectionTime]++;
			}
		}

		if (iEvaluation->second.multiTrialData.size()!=0)
		{
			trialAverageReward /= iEvaluation->second.multiTrialData.size();
			trialAveragePenalty /= iEvaluation->second.multiTrialData.size();
		}

		calcStats.averageTrialReward[iEvaluation->first] = trialAverageReward;

		totalRewardsSum += trialAverageReward;
		totalPenaltySum += trialAveragePenalty;
	}

	int maxCounter = -1;
	for (map<int,int>::iterator iMTIME_count = mtime_count.begin(); iMTIME_count!=mtime_count.end(); iMTIME_count++)
	{
		if (iMTIME_count->second>maxCounter)
		{
			maxCounter = iMTIME_count->second;
			calcStats.modeFailureDetectionTime = iMTIME_count->first;
		}
	}

	iEvaluation = Evaluations.begin();
	unsigned int goodFalseAlarm = 0;
	unsigned int goodFalseAlarmFixed = 0;
	for (; iEvaluation!=Evaluations.end(); iEvaluation++)
	{
		vector<TrialStatistics>::iterator iTrialStatistics = iEvaluation->second.multiTrialData.begin();
		double trialAverageReward = 0.0;
		double trialAveragePenalty = 0.0;

		for (; iTrialStatistics!=iEvaluation->second.multiTrialData.end(); iTrialStatistics++)
		{
			if (iTrialStatistics->goodTrial && iTrialStatistics->deltaRange.size()==2 && iTrialStatistics->evaluation==ACTION_EVAL_FALSE_ALARM)
			{
				goodFalseAlarm++;
				if (iTrialStatistics->deltaRange.at(1)<calcStats.maxBadDelta)
				{
					goodFalseAlarmFixed++;
				}
			}
		}
	}
	if (goodFalseAlarm>0)
	{
		calcStats.rateFixedFalseAlarmsByMaxBadDelta = 1.0*goodFalseAlarmFixed/goodFalseAlarm;
	}
	else
	{
		calcStats.rateFixedFalseAlarmsByMaxBadDelta = 0;
	}

	if (Evaluations.size()!=0)
	{
		calcStats.averageTotalReward = totalRewardsSum / Evaluations.size();
		calcStats.averageTotalPenalty = totalPenaltySum / Evaluations.size();
	}
	else
	{
		calcStats.averageTotalReward = 0.0;
		calcStats.averageTotalPenalty = 0.0;
	}

	if (totalEarlyPredictions!=0)
	{
		calcStats.averageEarlyPredictionTime/=totalEarlyPredictions;
	}
	else
	{
		calcStats.averageEarlyPredictionTime = 0.0;
		calcStats.minEarlyPredictionTime = 0;
		calcStats.maxEarlyPredictionTime = 0;
	}

	if (totalFailuresDetected+totalEarlyPredictions!=0)
	{
		calcStats.averageFailureBelief /= (totalFailuresDetected + totalEarlyPredictions);
	}
	else
	{
		calcStats.averageFailureBelief = 0.0;
		calcStats.minFailureBelief = 0;
		calcStats.maxFailureBelief = 0;
	}

	//if (totalFailuresDetected+totalEarlyPredictions!=0)
	if (totalAlarmsAfterFailure!=0)
	{
		//cout << "totalAlarmsAfterFailure = " << totalAlarmsAfterFailure << endl;

		calcStats.averageFailureDetectionTime /= totalAlarmsAfterFailure; //(totalFailuresDetected + totalEarlyPredictions);
	}
	else
	{
		calcStats.averageFailureDetectionTime = 0.0;
		calcStats.minFailureDetectionTime = 0;
		calcStats.maxFailureDetectionTime = 0;
	}

	EvaluationTrials = evaluationTrials;
	CalcStats = calcStats;
	bEvaluationStatisticaValid[decisionMakerID] = true;

	// find out trial statistics that compares time that it took for requested decision maker with others

	/*decision maker ID*/  /*time difference*/  /*number of occurrences*/
	map<int, map<int, unsigned int> > mapTrialsWithAlarmOffset;

	map<int, map<string, MultiTrialStatistics > >::iterator iReqDecisionMakerEvaluations = DecisionMakerEvaluations.find(decisionMakerID);
	//cout << DecisionMakerNamesByID[decisionMakerID] << "\n";

	for (iEvaluations = DecisionMakerEvaluations.begin(); iEvaluations != DecisionMakerEvaluations.end(); iEvaluations++)
	{
		map<string, MultiTrialStatistics >& reqDecisionMakerEvaluations = iReqDecisionMakerEvaluations->second;

		if (iEvaluations->first==decisionMakerID)
		{
			continue;
		}

		//cout << "\t" << DecisionMakerNamesByID[iEvaluations->first] << "\n";

		map<string, MultiTrialStatistics >& otherDecisionMakerEvaluations = iEvaluations->second;

		map<string, MultiTrialStatistics>::iterator iReqDecisionMakerEvaluation = reqDecisionMakerEvaluations.begin();

		//cout << "\t" << reqDecisionMakerEvaluations.size() << "\n";

		for (; iReqDecisionMakerEvaluation!=reqDecisionMakerEvaluations.end(); iReqDecisionMakerEvaluation++)
		{
			MultiTrialStatistics& otherDecisionMakerEvaluation = otherDecisionMakerEvaluations[iReqDecisionMakerEvaluation->first];

			vector<TrialStatistics>::iterator iReqTrialStatistics = iReqDecisionMakerEvaluation->second.multiTrialData.begin();
			if (iReqTrialStatistics->evaluation!=ACTION_EVAL_CORRECT_ALARM &&
					iReqTrialStatistics->evaluation!=ACTION_EVAL_EARLY_ALARM)
			{
				continue;
			}

			vector<TrialStatistics>::iterator iOtherTrialStatistics = otherDecisionMakerEvaluation.multiTrialData.begin();

			//cout << "\t\t" << iReqDecisionMakerEvaluation->second.multiTrialData.size() << "\n";

			for (; iReqTrialStatistics!=iReqDecisionMakerEvaluation->second.multiTrialData.end(); iReqTrialStatistics++, iOtherTrialStatistics++)
			{
				if (iOtherTrialStatistics->evaluation!=ACTION_EVAL_CORRECT_ALARM &&
						iReqTrialStatistics->evaluation!=ACTION_EVAL_EARLY_ALARM)
				{
					continue;
				}

				int reqMTIME = iReqTrialStatistics->failureDetectionTime;
				int otherMTIME = iOtherTrialStatistics->failureDetectionTime;

				/*cout << "reqMTIME = " << reqMTIME << "\n";
				cout << "otherMTIME = " << otherMTIME << "\n";*/

				if (reqMTIME-otherMTIME<0)
				{
					continue;
				}

				//cout << "\t\t" << "Counted " << DecisionMakerNamesByID[iEvaluations->first] << " " << reqMTIME-otherMTIME << "\n";

				if (mapTrialsWithAlarmOffset[iEvaluations->first].find(reqMTIME-otherMTIME)==mapTrialsWithAlarmOffset[iEvaluations->first].end())
				{
					mapTrialsWithAlarmOffset[iEvaluations->first][reqMTIME-otherMTIME] = 0;
				}

				mapTrialsWithAlarmOffset[iEvaluations->first][reqMTIME-otherMTIME]++;
			}
		}
	}

	stringstream ss;
	map<int, map<int, unsigned int> >::iterator iOffsetInfoByDecisionMaker;
	for (iOffsetInfoByDecisionMaker=mapTrialsWithAlarmOffset.begin(); iOffsetInfoByDecisionMaker!=mapTrialsWithAlarmOffset.end(); iOffsetInfoByDecisionMaker++)
	{
		if (iOffsetInfoByDecisionMaker->second.empty())
		{
			continue;
		}

		int max = 0;
		for (map<int, unsigned int>::iterator iOffsetInfoByOffsetLength = iOffsetInfoByDecisionMaker->second.begin();
				iOffsetInfoByOffsetLength != iOffsetInfoByDecisionMaker->second.end(); iOffsetInfoByOffsetLength++)
		{
			if (iOffsetInfoByOffsetLength->first>max)
			{
				max = iOffsetInfoByOffsetLength->first;
			}
		}

		bool bFirst = true;
		for (int i=0; i<=max; i++)
		{
			map<int, unsigned int>::iterator iOffsetInfoByOffsetLength = iOffsetInfoByDecisionMaker->second.find(i);
			if (iOffsetInfoByOffsetLength==iOffsetInfoByDecisionMaker->second.end())
			{
				continue;
			}

			if (bFirst)
			{
				ss << DecisionMakerNamesByID[iOffsetInfoByDecisionMaker->first] << " # ";
				bFirst = false;
			}

			ss << iOffsetInfoByOffsetLength->first << "=" << iOffsetInfoByOffsetLength->second << " and ";
		}
	}
	calcStats.strBadTrialsWithAlarmOffset = ss.str();

	int max = -1;
	int min = 0;
	for (map<int, unsigned int>::iterator iCounter=calcStats.rejectedAtTimeCounter.begin(); iCounter!=calcStats.rejectedAtTimeCounter.end(); iCounter++)
	{
		if (iCounter->first>max)
		{
			max = iCounter->first;
		}
		if (iCounter->first<min)
		{
			min = iCounter->first;
		}
	}

	stringstream ss1;
	for (int i=min; i<=max; i++)
	{
		map<int, unsigned int>::iterator iCounter=calcStats.rejectedAtTimeCounter.find(i);
		if (iCounter==calcStats.rejectedAtTimeCounter.end())
		{
			continue;
		}

		ss1 << i << "=" << iCounter->second << "; ";
	}

	calcStats.strBadRejectedTrialsByTime = ss1.str();
}

double ExperimentStatistics::AA(std::map<MONITOR_ACTION_EVALUATION, std::vector<string> >& evaluationTrials)
{
	unsigned int g_a = 0;	// good accepted
	unsigned int g_r = 0;	// good rejected

	g_a = evaluationTrials[ACTION_EVAL_CORRECT_CONTINUE].size();
	g_r = evaluationTrials[ACTION_EVAL_FALSE_ALARM].size();

	double aa = 1.0;
	if (g_r + g_a!=0)
	{
		aa = 1.0 * g_a / (g_a + g_r);
	}

	return aa;
}

double ExperimentStatistics::RA(std::map<MONITOR_ACTION_EVALUATION, std::vector<string> >& evaluationTrials)
{
	unsigned int b_a = 0;	// bad accepted
	unsigned int b_r = 0;	// bad rejected

	b_a = evaluationTrials[ACTION_EVAL_MISSED_ALARM].size();
	b_r = evaluationTrials[ACTION_EVAL_CORRECT_ALARM].size() + evaluationTrials[ACTION_EVAL_EARLY_ALARM].size();

	double ra = 1.0;
	if (b_r + b_a!=0)
	{
		ra = 1.0 * b_r / (b_a + b_r);
	}

	return ra;
}

/*
 * CommandLineHandler.cpp
 *
 *  Created on: May 14, 2013
 *      Author: ayavol2
 */

#include "CommandLineHandler.h"
#include "SolverOptions.h"
#include "stddef.h"
#include "boost/program_options.hpp"
#include <fstream>
#include <iostream>

using namespace std;
namespace po = boost::program_options;

/*static*/ CommandLineHandler* CommandLineHandler::pInstance = NULL;

CommandLineHandler::CommandLineHandler() {

}

CommandLineHandler::~CommandLineHandler() {

}

/*static*/ CommandLineHandler* CommandLineHandler::Instance()
{
	if (!pInstance)
	{
		pInstance = new CommandLineHandler();
	}

	return pInstance;
}

bool CommandLineHandler::ParseCommandLine(int argc, const char* argv[], SolverOptions& options, const std::string& iniFilePath /*= ""*/)
{
	string config_file;

	po::options_description desc("Generic options");
	desc.add_options()
			("help", "produce help message")
			("config", po::value<string>(&config_file)->default_value(""), "set name of configuration file")
			("report", po::value<string>(&options.ReportFilePath)->default_value(""), "path to report file that will be used for the final report. If specified the current directory will be scanned to folders with config.ini files and experiment results will be extracted.")
			;

	string experimentModel;
	string decisionMaker;
	string compareDecisionMaker;
	string mode;
	string experimentSet;
	string operatingSystem;

	po::options_description config("Configuration options");
	config.add_options()
		("mode", po::value<string>(&mode)->default_value("calculation"), "Type of operation to be performed by the solver")
		("trialsDir", po::value<string>(&options.TrialsDirPath)->default_value(""), "Directory with files containing trial files to use for the experiment")
		("threads", po::value<unsigned long>(&options.ExperimentThreads)->default_value(1), "Number of threads to use to run the experiment")
		("trials", po::value<unsigned long>(&options.Trials)->default_value(1), "Number of trials to run in experiment")
		("trialRepeats", po::value<unsigned long>(&options.TrialRepeats)->default_value(1), "Number of times to repeat each trial during experiment")
		("trialOutputVariants", po::value<unsigned long>(&options.OutputVariantsPerTrial)->default_value(1), "Number of output sequences to consider for each trial during experiment")
		("earlyAlarmDelay", po::value<unsigned long>(&options.MaxDelayAfterFalseAlarm)->default_value(5), "Number of time steps to wait after false alarm to confirm early alarm")
		("stepsPerTrial", po::value<unsigned long>(&options.MaxTrialSteps)->default_value(1000), "Maximum number of steps to perform in each experiment")
		("errorneousRange", po::value<unsigned long>(&options.ErrorneousTimeRange)->default_value(700), "Maximum time slice where the failure might occur during the trial")
		("beliefDimension", po::value<unsigned long>(&options.BeliefStateDimension)->default_value(100), "Dimension of the belief state used for experiment")
		("experimentModel", po::value<string>(&options.ExperimentModel)->default_value("SingleCarBreakingSystem"), "Model that will be used for the experiment")
		("decisionMaker", po::value<string>(&options.DecisionMaker)->default_value("pomcp"), "Decision maker algorithm that will be used during the experiment")
		("experimentDirPath", po::value<string>(&options.ExperimentDirPath)->default_value("Experiment"), "Directory path to store the history and result files for the experiment")
		("saveTrialBeliefs", po::value<bool>(&options.SaveTrialBeliefs)->default_value(false), "Save beliefs for each step of each trial (DEBUG purposes)")
		("consistentContinue",	po::value<double>(&options.Rewards.GoodContinue)->default_value(1.0), "Reward for being in consistent state and executing Continue action")
		("consistentAlarm", 	po::value<double>(&options.Rewards.GoodAlarm)->default_value(-1.0), "Reward for being in consistent state and executing Alarm action")
		("failureContinue",		po::value<double>(&options.Rewards.BadContinue)->default_value(-1.0), "Reward for being in failure state and executing Continue action")
		("failureAlarm",		po::value<double>(&options.Rewards.BadAlarm)->default_value(1.0), "Reward for being in failure state and executing Alarm action")
		("experimentSet",		po::value<string>(&experimentSet)->default_value("varying_penalties"), "Type of experiment set")
		("compare", 			po::value<string>(&options.CompareDecisionMaker)->default_value("none"), "Decision maker to use for comparison")
		("calculationBelief", 	po::value<string>(&options.CalculationBeliefFilePath)->default_value(""), "File path to the belief state to calculation")
		("inputFunctionDataFile", po::value<string>(&options.InputFunctionDataFilePath)->default_value(""), "File path to the data file for the model input function")
		("operatingSystem", po::value<string>(&operatingSystem)->default_value("Windows"), "Operating system selection")
		("missedAlarmDelay", po::value<unsigned long>(&options.MaxDelayBeforeMissedAlarm)->default_value(10), "Number of time steps to wait after false alarm to confirm early alarm")
		;

	po::options_description pomcp_config("POMDP options");
	config.add_options()
		("discount", po::value<double>(&options.POMDPDiscountFactor)->default_value(0.95), "POMDP discount factor")
		("exploration", po::value<double>(&options.POMCPExplorationConstant)->default_value(1.0), "POMCP exploration constant")
		("maxTreeDepth", po::value<unsigned long>(&options.POMCPMaxTreeDepth)->default_value(100), "POMCP maximum depth of the search tree")
		("pomcpSimulations", po::value<unsigned long>(&options.POMCPSimulations)->default_value(1000), "Number of Monte-Carlo simulations")
		("thresholdValue", po::value<double>(&options.ThresholdValue)->default_value(0.95), "Level of the threshold for the threshold based decision maker")
		("thresholdAccuracy", po::value<double>(&options.ThresholdAccuracy)->default_value(1e-6), "Accuracy of the threshold")
		;

	po::options_description cmdline_options;
	cmdline_options.add(desc).add(config).add(pomcp_config);

    po::options_description config_file_options;
    config_file_options.add(config).add(pomcp_config);


    po::variables_map vm;

    try
    {
    	if (iniFilePath.empty())
        {
    		po::store(po::parse_command_line(argc, argv, cmdline_options), vm);
    		po::notify(vm);

            if (vm.count("help")) {
                cout << desc << config << "\n";
                return false;
            }

        }
    	else
    	{
    		config_file = iniFilePath;
    	}

        if (!config_file.empty())
        {
        	ifstream ifs(config_file.c_str());
            if (!ifs)
            {
                    cout << "Failed to open configuration file: " << config_file << "\n";
                    return false;
            }
            else
            {
                    po::store(po::parse_config_file(ifs, config_file_options), vm);
                    po::notify(vm);
            }
        }
    }
    catch (po::error& e)
    {
    	cout << "Failed to parse command line. Error: " << e.what() << endl;
    	return false;
    }

    bool bRequiredDefined = true;

    if (!options.SetMode(mode))
    {
    	cerr << "Unrecognized mode " << mode << "\n";
    	bRequiredDefined = false;
    }

    if (!options.SetExperimentSet(experimentSet))
    {
    	cerr << "Unrecognized experiment set" << experimentSet << "\n";
    	bRequiredDefined = false;
    }

    if (!options.SetOperatingSystem(operatingSystem))
    {
    	cerr << "Unrecognized operating system " << operatingSystem << "\n";
    	bRequiredDefined = false;
    }

    if (!bRequiredDefined)
    {
    	return false;
    }

	return true;
}

bool CommandLineHandler::ParseConfigFile(const std::string& iniFilePath, SolverOptions& options)
{
	return ParseCommandLine(0, NULL, options, iniFilePath);
}

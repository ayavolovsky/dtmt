/*
 * POMCPExperiment.cpp
 *
 *  Created on: May 14, 2013
 *      Author: ayavol2
 */

#include "Experiment.h"
#include "DecisionMaker.h"
#include "DecisionSystem.h"
#include <iostream>
#include <MonitoringDefs.h>
#include <ModelSimulationOptions.h>
#include <ModelRewardSystem.h>
#include <ModelInputFunction.h>
#include <SolverOptions.h>
#include <boost/filesystem.hpp>
#include <boost/format.hpp>
#include <boost/chrono.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/dll.hpp>

using namespace std;
namespace fs = boost::filesystem;

/*static*/ unsigned int Experiment::TrialsToExecute = 0 ;
/*static*/ boost::recursive_mutex Experiment::TrialsCounterMutex;
/*static*/ boost::recursive_mutex Experiment::TrialStatisticsMutex;
/*static*/ boost::recursive_mutex Experiment::ExperimentsCounterMutex;
/*static*/ ExperimentStatistics Experiment::Statistics;
/*static*/ std::map<std::string, unsigned long> Experiment::TrialFilesMap;
/*static*/ int Experiment::ExperimentCounter = 0;
/*static*/ int Experiment::TotalExperiments = 0;
/*static*/ Experiment::ThreadMode Experiment::threadingMode = THM_CALCULATION;

Experiment::Experiment(const SolverOptions& options, std::streambuf* outBuf, unsigned int groupID /*= 0*/):
		Options(options),
		GroupID(groupID),
		Out(outBuf)
{
}

Experiment::~Experiment() {

}

bool Experiment::Start()
{
	if (!Initialize())
	{
		Out << "Failed to initialize experiment." << "\n";
		return false;
	}

	switch (Options.GetMode())
	{
	case SolverOptions::CALCULATION_BELIEF:
		// create experiment directory
		if (!MakeExperimentDir())
		{
			Out << "Failed to create experiment directory." << "\n";
			return false;
		}

		if (!ExecuteExperimentBeliefState())
		{
			Out << "There was an error executing trial from the belief point." << "\n";
			return false;
		}

		break;
	case SolverOptions::CALCULATION:
		// create experiment directory
		if (!MakeExperimentDir())
		{
			Out << "Failed to create experiment directory." << "\n";
			return false;
		}

		if (!ExecuteExperimentsTrials())
		{
			Out << "There was an error executing one of the experiment trials." << "\n";
			return false;
		}

		/*if (!Options.CompareDecisionMaker.IsEmpty())
		{
			for (int iDecision=0; iDecision<DecisionSystemObj->GetDecisionMakerCount(); iDecision++)
			{
				ExperimentStatistics originalStatistics = Statistics;
				string csvFilePath = GetOutputCSVFilePath(iDecision);
				string txtFilePath = GetOutputTXTFilePath(iDecision);

				if (Options.GetCompareDecisionMaker()==SolverOptions::DECISION_THRESHOLD_BASED)
				{
					Options.ConvertToThresholdMode(Statistics.GetAverageFailureBelief(0));
				}
				else
				{
					Options.ConvertToCompareDecision();
				}

				Statistics.Clear();

				Experiment thresholdExperiment(Options, Out.rdbuf());
				thresholdExperiment.Start();

				// TODO:
				// calculate statistical comparison value between initial method and threshold method
				// since the same set of trials is being used, the same is done:
				// for each trial average reward is taken from both methods
				// take difference between average rewards for trial
				// sum up the differences and divide by the number of trials
				// that will represent how much one method is on average better than the second one

				originalStatistics.AppendComparison_CSV(csvFilePath, Statistics, iDecision);
				originalStatistics.AppendComparison_TXT(txtFilePath, Statistics, iDecision);
			}
		}*/

		break;
	case SolverOptions::GENERATE_BELIEF_TRIALS:
		if (!GenerateBeliefTrials())
		{
			Out << "There was an error generating belief trials." << "\n";
			return false;
		}
		break;
	case SolverOptions::TRIALS:
		if (!GenerateExperimentTrials())
		{
			Out << "There was an error generating experiment trials." << "\n";
			return false;
		}
		break;
	default:
		{
			Out << "Mode is not supported" << "\n";
			return false;
		}
	}

	return true;
}

/*void Experiment::AddPOMCPExperiment(double p, double cc, double fc, double fa)
{
	boost::shared_ptr<POMCP> pomcpSolver = boost::shared_ptr<POMCP>(new POMCP());
	pomcpSolver->GetOptions().DiscountFactor = Options.GetDiscountFactor();
	pomcpSolver->GetOptions().ExplorationConstant = Options.GetExplorationConstant();
	pomcpSolver->GetOptions().MaxTreeDepth = Options.GetPOMCPMaxTreeDepth();
	pomcpSolver->GetOptions().Simulations = Options.GetPOMCPSimulations();

	double ca = (p*(fa-fc+cc)-cc)/(p-1);

	ModelRewardSystem rewards;
	rewards.RewardConsistentContinue = cc;
	rewards.RewardConsistentAlarm = ca;
	rewards.RewardFailureContinue = fc;
	rewards.RewardFailureAlarm = fa;

	// C = R_hi - R_lo
	// R_hi - highest return achieved during sample runs with c = 0
	// R_lo - lowest return achieved during sample rollouts

	double maxReward = std::max(rewards.RewardConsistentAlarm,
			std::max(rewards.RewardConsistentContinue,
					std::max(rewards.RewardFailureAlarm, rewards.RewardFailureContinue)));

	double minReward = std::min(rewards.RewardConsistentAlarm,
			std::min(rewards.RewardConsistentContinue,
					std::min(rewards.RewardFailureAlarm, rewards.RewardFailureContinue)));

	pomcpSolver->GetOptions().ExplorationConstant = maxReward - minReward;
	pomcpSolver->SetInputFunctionKnown(true);

	string name = boost::str(boost::format("POMCP_%1%_%2%_%3%_%4%_%5%") % p % cc % ca % fc % fa);
	boost::replace_all(name, ".", "-");

	DecisionSystemObj->AddDecisionMaker(name, SolverOptions::DECISION_POMCP, pomcpSolver, rewards);
}*/

// TMP
/*void Experiment::AddExperiments_CASE2016(std::vector<double>& correspondingThresholds)
{
	double Ga = 1;
	double Lc = 0.5;
	double La = 0;
	double Gc = 0;

	boost::shared_ptr<POMCP> pomcpSolver = boost::shared_ptr<POMCP>(new POMCP());
	pomcpSolver->GetOptions().DiscountFactor = Options.GetDiscountFactor();
	pomcpSolver->GetOptions().ExplorationConstant = Options.GetExplorationConstant();
	pomcpSolver->GetOptions().MaxTreeDepth = Options.GetPOMCPMaxTreeDepth();
	pomcpSolver->GetOptions().Simulations = Options.GetPOMCPSimulations();

	ModelRewardSystem rewards;
	rewards.RewardConsistentContinue = Gc;
	rewards.RewardConsistentAlarm = -1*La;
	rewards.RewardFailureContinue = -1*Lc;
	rewards.RewardFailureAlarm = Ga;

	// C = R_hi - R_lo
	// R_hi - highest return achieved during sample runs with c = 0
	// R_lo - lowest return achieved during sample rollouts

	double maxReward = std::max(rewards.RewardConsistentAlarm,
			std::max(rewards.RewardConsistentContinue,
					std::max(rewards.RewardFailureAlarm, rewards.RewardFailureContinue)));

	double minReward = std::min(rewards.RewardConsistentAlarm,
			std::min(rewards.RewardConsistentContinue,
					std::min(rewards.RewardFailureAlarm, rewards.RewardFailureContinue)));

	pomcpSolver->GetOptions().ExplorationConstant = maxReward - minReward;
	pomcpSolver->SetInputFunctionKnown(true);

	string name = boost::str(boost::format("POMCP:%1%:{%2%;%3%;%4%;%5%}") 	% rewards.RewardFailureContinue
																			% rewards.RewardConsistentContinue
																			% rewards.RewardConsistentAlarm
																			% rewards.RewardFailureContinue
																			% rewards.RewardFailureAlarm);
	boost::replace_all(name, ".", "-");

	DecisionSystemObj->AddDecisionMaker(name, SolverOptions::DECISION_POMCP, pomcpSolver, rewards);
}*/

/*void Experiment::AddExperiments_CASE2016_parametrized(std::vector<double>& correspondingThresholds, double La, double Gc)
{
	double Ga = 1;
	double Lc = 0;

	std::vector<double> Lc_values;
	//Lc_values.push_back(0.000005);

	//Lc_values.push_back(0.00005);
	//Lc_values.push_back(0.0005);
	Lc_values.push_back(0.005);
	//Lc_values.push_back(0.015);
	Lc_values.push_back(0.025);
	Lc_values.push_back(0.05);
	Lc_values.push_back(0.1);
	Lc_values.push_back(0.2);
	Lc_values.push_back(0.3);
	Lc_values.push_back(0.5);

	//Lc_values.push_back(0.75);

	for (vector<double>::iterator iLc = Lc_values.begin(); iLc!=Lc_values.end(); iLc++)
	{
		Lc = *iLc;

		boost::shared_ptr<POMCP> pomcpSolver = boost::shared_ptr<POMCP>(new POMCP());
		pomcpSolver->GetOptions().DiscountFactor = Options.GetDiscountFactor();
		pomcpSolver->GetOptions().ExplorationConstant = Options.GetExplorationConstant();
		pomcpSolver->GetOptions().MaxTreeDepth = Options.GetPOMCPMaxTreeDepth();
		pomcpSolver->GetOptions().Simulations = Options.GetPOMCPSimulations();

		ModelRewardSystem rewards;
		rewards.RewardConsistentContinue = Gc;
		rewards.RewardConsistentAlarm = -1*La;
		rewards.RewardFailureContinue = -1*Lc;
		rewards.RewardFailureAlarm = Ga;

		// C = R_hi - R_lo
		// R_hi - highest return achieved during sample runs with c = 0
		// R_lo - lowest return achieved during sample rollouts

		double maxReward = std::max(rewards.RewardConsistentAlarm,
				std::max(rewards.RewardConsistentContinue,
						std::max(rewards.RewardFailureAlarm, rewards.RewardFailureContinue)));

		double minReward = std::min(rewards.RewardConsistentAlarm,
				std::min(rewards.RewardConsistentContinue,
						std::min(rewards.RewardFailureAlarm, rewards.RewardFailureContinue)));

		pomcpSolver->GetOptions().ExplorationConstant = maxReward - minReward;
		pomcpSolver->SetInputFunctionKnown(true);

		string name = boost::str(boost::format("POMCP:%1%:{%2%;%3%;%4%;%5%}") 	% rewards.RewardFailureContinue
																				% rewards.RewardConsistentContinue
																				% rewards.RewardConsistentAlarm
																				% rewards.RewardFailureContinue
																				% rewards.RewardFailureAlarm);
		boost::replace_all(name, ".", "-");

		DecisionSystemObj->AddDecisionMaker(name, SolverOptions::DECISION_POMCP, pomcpSolver, rewards);
	}
}

void Experiment::AddExperiments_RV2016(std::vector<double>& correspondingThresholds)
{
	double Ga = 1;

	double Gc = 0;
	double Lc = 0;
	double La = 0;

	std::vector<double> Gc_values;
	Gc_values.push_back(0);
	Gc_values.push_back(0.1);
	Gc_values.push_back(0.5);
	Gc_values.push_back(1);
	Gc_values.push_back(1.5);

	std::vector<double> La_values;
	La_values.push_back(0);
	La_values.push_back(0.1);
	La_values.push_back(0.5);
	La_values.push_back(1);
	La_values.push_back(1.5);
	La_values.push_back(2);

	for (vector<double>::iterator iTh = correspondingThresholds.begin(); iTh!=correspondingThresholds.end(); iTh++)
	{
		for (vector<double>::iterator iGc = Gc_values.begin(); iGc!=Gc_values.end(); iGc++)
		{
			for (vector<double>::iterator iLa = La_values.begin(); iLa!=La_values.end(); iLa++)
			{
				double th = *iTh;
				Gc = *iGc;
				La = *iLa;
				Lc = (Gc+La-th*(Ga+La+Gc))/th;

				if (Lc<0)
				{
					continue;
				}

				boost::shared_ptr<POMCP> pomcpSolver = boost::shared_ptr<POMCP>(new POMCP());
				pomcpSolver->GetOptions().DiscountFactor = Options.GetDiscountFactor();
				pomcpSolver->GetOptions().ExplorationConstant = Options.GetExplorationConstant();
				pomcpSolver->GetOptions().MaxTreeDepth = Options.GetPOMCPMaxTreeDepth();
				pomcpSolver->GetOptions().Simulations = Options.GetPOMCPSimulations();

				ModelRewardSystem rewards;
				rewards.RewardConsistentContinue = Gc;
				rewards.RewardConsistentAlarm = -1*La;
				rewards.RewardFailureContinue = -1*Lc;
				rewards.RewardFailureAlarm = Ga;

				// C = R_hi - R_lo
				// R_hi - highest return achieved during sample runs with c = 0
				// R_lo - lowest return achieved during sample rollouts

				double maxReward = std::max(rewards.RewardConsistentAlarm,
						std::max(rewards.RewardConsistentContinue,
								std::max(rewards.RewardFailureAlarm, rewards.RewardFailureContinue)));

				double minReward = std::min(rewards.RewardConsistentAlarm,
						std::min(rewards.RewardConsistentContinue,
								std::min(rewards.RewardFailureAlarm, rewards.RewardFailureContinue)));

				pomcpSolver->GetOptions().ExplorationConstant = maxReward - minReward;
				pomcpSolver->SetInputFunctionKnown(true);

				string name = boost::str(boost::format("POMCP:%1%:{%2%;%3%;%4%;%5%}") 	% rewards.RewardFailureContinue
																						% rewards.RewardConsistentContinue
																						% rewards.RewardConsistentAlarm
																						% rewards.RewardFailureContinue
																						% rewards.RewardFailureAlarm);
				boost::replace_all(name, ".", "-");

				DecisionSystemObj->AddDecisionMaker(name, SolverOptions::DECISION_POMCP, pomcpSolver, rewards);
			}
		}
	}

}*/

/*void Experiment::AddExperiments_beliefBased(std::vector<double>& thresholds)
{
	ModelRewardSystem rewards;

	vector<double> all_p;
	vector<double> all_delta1;
	vector<double> all_delta2;

	const double F = 0.8;

	all_p.push_back(0.5);
	all_delta1.push_back(0.2092);
	all_delta2.push_back(1);

	all_p.push_back(0.6);
	all_delta1.push_back(0.1818);
	all_delta2.push_back(1);

	all_p.push_back(0.7);
	all_delta1.push_back(0.1146);
	all_delta2.push_back(1);

	all_p.push_back(0.8);
	all_delta1.push_back(0.0146);
	all_delta2.push_back(1);

	string name;

	vector<double>::iterator iDelta1 = all_delta1.begin();
	vector<double>::iterator iDelta2 = all_delta2.begin();

	int nMaxHorizon = 2;
	for (vector<double>::iterator iP=all_p.begin(); iP!=all_p.end(); iP++)
	{
		for (int iH=nMaxHorizon; iH>=2; iH--)
		{
			boost::shared_ptr<BeliefBased> beliefBasedSolver = boost::shared_ptr<BeliefBased>(new BeliefBased());
			beliefBasedSolver->SetInputFunctionKnown(true);
			beliefBasedSolver->GetOptions().p = *iP;
			beliefBasedSolver->GetOptions().delta_horizon = iH;
			beliefBasedSolver->GetOptions().delta1 = *iDelta1;
			beliefBasedSolver->GetOptions().delta2 = *iDelta2;
			name = boost::str(boost::format("ThresholdCombined:%1%:%2%:%3%:%4%") % *iP % iH % *iDelta1 % *iDelta2);
			boost::replace_all(name, ".", "-");
			DecisionSystemObj->AddDecisionMaker(name, SolverOptions::DECISION_BELIEF_BASED, beliefBasedSolver, rewards);
		}

		iDelta1++;
		iDelta2++;
	}
}*/

/*void Experiment::AddExperiments_threshold(std::vector<double>& correspondingThresholds)
{
	std::vector<unsigned int> horizons;
	//horizons.push_back(5);
	//horizons.push_back(4);
	//horizons.push_back(3);
	horizons.push_back(2);
	horizons.push_back(1);

	//horizons.push_back(6);
	//horizons.push_back(7);
	//horizons.push_back(8);
	//horizons.push_back(9);
	//horizons.push_back(10);
	//horizons.push_back(15);
	//horizons.push_back(20);
	//horizons.push_back(40);

	for (vector<double>::iterator iThreshold = correspondingThresholds.begin(); iThreshold!=correspondingThresholds.end(); iThreshold++)
	{
		double currentThreshold = *iThreshold;

		for (vector<unsigned int>::iterator iHorizon = horizons.begin(); iHorizon!=horizons.end(); iHorizon++)
		{
			unsigned int h = *iHorizon;
			double p = *iThreshold;

			boost::shared_ptr<ThresholdBased> thresholdSolver = boost::shared_ptr<ThresholdBased>(new ThresholdBased());
			thresholdSolver->GetOptions().ThresholdValue = p;
			thresholdSolver->GetOptions().Accuracy = Options.GetThresholdAccuracy();
			thresholdSolver->GetOptions().Horizon = h;
			thresholdSolver->SetInputFunctionKnown(true);
			string name_th = boost::str(boost::format("Threshold_horizon_%1%_%2%") % h % p);
			boost::replace_all(name_th, ".", "-");
			DecisionSystemObj->AddDecisionMaker(name_th, SolverOptions::DECISION_THRESHOLD_BASED, thresholdSolver, Options.Rewards);
		}
	}
}*/

// TMP
/*void Experiment::AddExperiments(std::vector<double>& correspondingThresholds)
{
	std::vector<double> R_values;
	R_values.push_back(0.25);
	R_values.push_back(0.5);
	R_values.push_back(1);
	R_values.push_back(1.5);
	R_values.push_back(2);
	R_values.push_back(5);

	std::vector<double> fc_values;
	//fa_values.push_back(0);
	fc_values.push_back(-0.2);
	fc_values.push_back(-0.5);
	fc_values.push_back(-1);
	fc_values.push_back(-5);
	fc_values.push_back(-10);

	for (vector<double>::iterator iFC = fc_values.begin(); iFC!=fc_values.end(); iFC++) {
		for (vector<double>::iterator iR = R_values.begin(); iR!=R_values.end(); iR++) {
			for (vector<double>::iterator iThreshold = correspondingThresholds.begin(); iThreshold!=correspondingThresholds.end(); iThreshold++)
			{
				double currentThreshold = *iThreshold;

				boost::shared_ptr<POMCP> pomcpSolver = boost::shared_ptr<POMCP>(new POMCP());
				pomcpSolver->GetOptions().DiscountFactor = Options.GetDiscountFactor();
				pomcpSolver->GetOptions().ExplorationConstant = Options.GetExplorationConstant();
				pomcpSolver->GetOptions().MaxTreeDepth = Options.GetPOMCPMaxTreeDepth();
				pomcpSolver->GetOptions().Simulations = Options.GetPOMCPSimulations();

				ModelRewardSystem rewards;

				double p = *iThreshold;
				double ratio = *iR;


				double ca = 0;
				double fa = 0;
				double cc = 0;
				double fc = 0;

				cc = 1;
				fc = *iFC;
				fa = ratio*cc;
				ca = (p * (cc+fa-fc)-cc) / (p-1);

				if (ca>0 || fa<0 || cc<0 || fc>0)
					continue;

				rewards.RewardConsistentContinue = cc;
				rewards.RewardConsistentAlarm = ca;
				rewards.RewardFailureContinue = fc;
				rewards.RewardFailureAlarm = fa;

				// C = R_hi - R_lo
				// R_hi - highest return achieved during sample runs with c = 0
				// R_lo - lowest return achieved during sample rollouts

				double maxReward = std::max(rewards.RewardConsistentAlarm,
						std::max(rewards.RewardConsistentContinue,
								std::max(rewards.RewardFailureAlarm, rewards.RewardFailureContinue)));

				double minReward = std::min(rewards.RewardConsistentAlarm,
						std::min(rewards.RewardConsistentContinue,
								std::min(rewards.RewardFailureAlarm, rewards.RewardFailureContinue)));

				pomcpSolver->GetOptions().ExplorationConstant = maxReward - minReward;
				pomcpSolver->SetInputFunctionKnown(true);


				string name = boost::str(boost::format("POMCP:%1%:%2%:%3%:{%4%;%5%;%6%;%7%}") % fc % *iR % currentThreshold % cc % ca % fc % fa);
				boost::replace_all(name, ".", "-");

				DecisionSystemObj->AddDecisionMaker(name, SolverOptions::DECISION_POMCP, pomcpSolver, rewards);
			}
		}
	}
}*/

// TMP
/*void Experiment::AddExperiments_old(std::vector<double>& correspondingThresholds)
{
	std::vector<double> R_values;
	//R_values.push_back(0.1);
	R_values.push_back(0.5);
	R_values.push_back(1);
	//R_values.push_back(5);
	R_values.push_back(10);
	R_values.push_back(100);

	std::vector<double> fa_values;
	fa_values.push_back(0);
	fa_values.push_back(0.1);
	fa_values.push_back(0.5);
	fa_values.push_back(1);
	//fa_values.push_back(5);
	fa_values.push_back(10);

	for (vector<double>::iterator iFA = fa_values.begin(); iFA!=fa_values.end(); iFA++) {
		for (vector<double>::iterator iR = R_values.begin(); iR!=R_values.end(); iR++) {
			for (vector<double>::iterator iThreshold = correspondingThresholds.begin(); iThreshold!=correspondingThresholds.end(); iThreshold++)
			{
				double currentThreshold = *iThreshold;

				boost::shared_ptr<POMCP> pomcpSolver = boost::shared_ptr<POMCP>(new POMCP());
				pomcpSolver->GetOptions().DiscountFactor = Options.GetDiscountFactor();
				pomcpSolver->GetOptions().ExplorationConstant = Options.GetExplorationConstant();
				pomcpSolver->GetOptions().MaxTreeDepth = Options.GetPOMCPMaxTreeDepth();
				pomcpSolver->GetOptions().Simulations = Options.GetPOMCPSimulations();

				ModelRewardSystem rewards;

				double p = *iThreshold;
				double ratio = *iR;

				double ca = -1;
				double fc = ca*ratio;
				double fa = *iFA;

				// double cc = (fa + ca - p * (ca + fc)) / (1 - p);		// incorrect
				double cc = (ca + p * (fa-fc-ca)) / (1 - p);

				rewards.RewardConsistentContinue = cc;
				rewards.RewardConsistentAlarm = ca;
				rewards.RewardFailureContinue = fc;
				rewards.RewardFailureAlarm = fa;

				// C = R_hi - R_lo
				// R_hi - highest return achieved during sample runs with c = 0
				// R_lo - lowest return achieved during sample rollouts

				double maxReward = std::max(rewards.RewardConsistentAlarm,
						std::max(rewards.RewardConsistentContinue,
								std::max(rewards.RewardFailureAlarm, rewards.RewardFailureContinue)));

				double minReward = std::min(rewards.RewardConsistentAlarm,
						std::min(rewards.RewardConsistentContinue,
								std::min(rewards.RewardFailureAlarm, rewards.RewardFailureContinue)));

				pomcpSolver->GetOptions().ExplorationConstant = maxReward - minReward;
				pomcpSolver->SetInputFunctionKnown(true);

				string name = boost::str(boost::format("POMCP:%1%:%2%:%3%") % fa % ratio % currentThreshold);
				boost::replace_all(name, ".", "-");

				DecisionSystemObj->AddDecisionMaker(name, SolverOptions::DECISION_POMCP, pomcpSolver, rewards);
			}
		}
	}
}*/

bool Experiment::Initialize()
{
	// default: no input function for the model
	InputFunction = boost::shared_ptr<ModelInputFunction>();

	// allocate the real and simulation models
	std::string experimentModelLibrary = Options.ExperimentModel;
	auto createStateModel_func = boost::dll::import<boost::shared_ptr<Model>()>(experimentModelLibrary, "CreateStateModel");
	auto createStateInputFunction_func = boost::dll::import<boost::shared_ptr<ModelInputFunction>(const std::string&)>(experimentModelLibrary, "CreateStateInputFunction");
	RealModel 		= createStateModel_func();
	SimulationModel = createStateModel_func();
	InputFunction	= createStateInputFunction_func(Options.GetInputFunctionDataFile());

//	else
	//{
//		Out << "Unknown experiment model to execute: " << Options.GetExperimentModel() << "\n";
//		return false;
//	}

	RealModel->SetRewardSystem(Options.GetRewardsSystem());
	SimulationModel->SetRewardSystem(Options.GetRewardsSystem());

	ModelSimulationOptions simulationOptions;
	simulationOptions.SetErrorneousTimeRange(Options.GetErrornousTimeRange());

	RealModel->SetSimulationOptions(simulationOptions);
	SimulationModel->SetSimulationOptions(simulationOptions);

	DecisionSystemObj = boost::shared_ptr<DecisionSystem>(new DecisionSystem(SimulationModel, Out.rdbuf()));

	std::string decisionMakerLibrary = Options.DecisionMaker;
	auto createDecisionMaker_func = boost::dll::import<boost::shared_ptr<DecisionMaker>()>(experimentModelLibrary, "CreateDecisionMaker");
	auto configureDecisionMaker_func = boost::dll::import<void(boost::shared_ptr<DecisionMaker>&, const SolverOptions&)>(experimentModelLibrary, "ConfigureDecisionMaker");

	boost::shared_ptr<DecisionMaker> pDecisionMaker = createDecisionMaker_func();
	configureDecisionMaker_func(pDecisionMaker, Options);
	DecisionSystemObj->AddDecisionMaker("", Options.DecisionMaker, pDecisionMaker, Options.Rewards);

	// allocate the decision maker
	/*switch (Options.GetDecisionMaker())
	{
	case SolverOptions::DECISION_POMCP:
		{
			vector<double> thresholds;
			thresholds.push_back(0.1);
			thresholds.push_back(0.15);
			thresholds.push_back(0.2);
			thresholds.push_back(0.25);
			thresholds.push_back(0.3);
			thresholds.push_back(0.35);
			thresholds.push_back(0.4);
			thresholds.push_back(0.45);
			thresholds.push_back(0.5);
			thresholds.push_back(0.55);
			thresholds.push_back(0.6);
			thresholds.push_back(0.65);
			thresholds.push_back(0.7);
			thresholds.push_back(0.75);
			thresholds.push_back(0.8);
			thresholds.push_back(0.85);
			thresholds.push_back(0.9);
			thresholds.push_back(0.95);
			thresholds.push_back(0.99);
			thresholds.push_back(0.995);
			AddExperiments_threshold(thresholds);

			thresholds.clear();
		}
		break;
	case SolverOptions::DECISION_THRESHOLD_BASED:
		{
			boost::shared_ptr<ThresholdBased> thresholdSolver = boost::shared_ptr<ThresholdBased>(new ThresholdBased());
			thresholdSolver->GetOptions().ThresholdValue = Options.GetThresholdValue();
			thresholdSolver->GetOptions().Accuracy = Options.GetThresholdAccuracy();

			DecisionSystemObj->AddDecisionMaker("", Options.GetDecisionMaker(), thresholdSolver, Options.Rewards);
		}
		break;
	case SolverOptions::DECISION_MAX_EXPECTED_UTILITY:
		{
			boost::shared_ptr<MaximumExpectedUtility> maxExpSolver = boost::shared_ptr<MaximumExpectedUtility>(new MaximumExpectedUtility());

			DecisionSystemObj->AddDecisionMaker("", Options.GetDecisionMaker(), maxExpSolver, Options.Rewards);
		}
		break;
	default:
		Out << "Unknown decision maker type: " << Options.DecisionMaker() << "\n";
		return false;
		break;
	}*/

	DecisionSystemObj->SetOutput(Out.rdbuf());

	return true;
}

bool Experiment::RestoreTrialsList()
{
	fs::path searchDir(Options.GetTrialsDirPath());

	TrialFilesMap.clear();

	try
	{
		if (!fs::exists(searchDir) || !fs::is_directory(searchDir))
		{
			return false;
		}

		fs::directory_iterator end_iter;
	    for ( fs::directory_iterator dir_itr( searchDir );
	    		dir_itr != end_iter;
	    		++dir_itr )
	    {
	    	if (fs::is_directory(dir_itr->status()))
	    	{
	    		continue;
	    	}

	    	if (!fs::is_regular_file(dir_itr->path()))
	    	{
	    		continue;
	    	}

	    	fs::path filePath(dir_itr->path());

	    	if (fs::extension(filePath).compare(".csv")!=0)
	    	{
	    		continue;
	    	}

	    	TrialFilesMap[filePath.string()] = Options.GetTrialsRepeats();
	    }
	}
	catch (const fs::filesystem_error& ex)
	{
		Out << ex.what() << "\n";
		return false;
	}

	Out << "Successfully located " << TrialFilesMap.size() << " trials. Experiment will be executed using all of them." << endl;

	return true;
}

bool Experiment::ExecuteExperimentBeliefState()
{
	Out << "Restoring belief state from file " << Options.GetCalculationBeliefFilePath() << "\n";
	if (!DecisionSystemObj->RestoreBeliefFromFile(Options.GetCalculationBeliefFilePath()))
	{
		return false;
	}

	boost::shared_ptr<State> realState;
	realState = RealModel->CreateStartState();

	Out << "Belief state: " << DecisionSystemObj->BeliefToString(realState) << "\n";

	std::map<MONITOR_ACTION_TYPE, unsigned int> actionCounter;
	actionCounter[ACTION_CONTINUE] = 0;
	actionCounter[ACTION_ALARM] = 0;

	for (unsigned int iRepeat = 0; iRepeat<Options.GetTrialsRepeats(); iRepeat++)
	{
		Out << "---------------------------------------------------------------------\n";
		Out << "Repeat #" << iRepeat << "\n";

		boost::chrono::system_clock::time_point startGetAction = boost::chrono::system_clock::now();

		// find the action to be executed according to the current knowledge
		/*unsigned long observation = 0;
		MONITOR_ACTION_TYPE stepAction = DecisionSystemObj->GetAction(observation);*/
		MONITOR_ACTION_TYPE stepAction = ACTION_CONTINUE;

			boost::chrono::system_clock::time_point stopGetAction = boost::chrono::system_clock::now();
			boost::chrono::duration<double> stepActionDuration = stopGetAction - startGetAction;

		Out << "Selected action ( " << stepActionDuration.count() << " sec) = " << ActionToString(stepAction) << "\n";

		actionCounter[stepAction]++;
	}

	Out << "---------------------------------------------------------------------\n";
	Out << "---------------------------------------------------------------------\n";
	Out << "Action \'" << ActionToString(ACTION_CONTINUE) << "\' selected " << actionCounter[ACTION_CONTINUE] << " times\n";
	Out << "Action \'" << ActionToString(ACTION_ALARM) << "\' selected " << actionCounter[ACTION_ALARM] << " times\n";

	return true;
}

/*static*/ void Experiment::ResetExperimentCounter(int nTotalExperiments)
{
	ExperimentCounter = 0;
	TotalExperiments = nTotalExperiments;
}

/*static*/ double Experiment::IncrementExperimentCounter()
{
	boost::lock_guard<boost::recursive_mutex> _(ExperimentsCounterMutex);

	ExperimentCounter++;
	return (100.0*ExperimentCounter/TotalExperiments);
}

bool Experiment::ExecuteExperimentsTrials()
{
	boost::chrono::system_clock::time_point experimentStart = boost::chrono::system_clock::now();

	TrialsToExecute = Options.GetTrials();

	if (!RestoreTrialsList())
	{
		Out << "Failed to restore list of files containing trials data." << "\n";
		return false;
	}

	vector<boost::shared_ptr<Experiment> > instances;
	vector<boost::shared_ptr<boost::thread> > experimentThreads;
	vector<boost::shared_ptr<ofstream> > experimentFiles;

	int nTotalExperiments = TrialFilesMap.size()* Options.GetTrialsRepeats();
	ResetExperimentCounter(nTotalExperiments);

	threadingMode = THM_CALCULATION;

	for (unsigned long iThread = 0; iThread<Options.GetThreads(); iThread++)
	{
		Out << "Starting thread #" << iThread << "\n";

		// create file for the thread output
		boost::shared_ptr<ofstream> file = boost::shared_ptr<ofstream>(new ofstream());
		file->open(GetGroupLogFilePath(iThread).c_str(), ios::out);

		// create instance to run the thread
		boost::shared_ptr<Experiment> instance = boost::shared_ptr<Experiment>(new Experiment(Options, file->rdbuf(), iThread));

		// start the thread
		boost::shared_ptr<boost::thread> newThread =
				boost::shared_ptr<boost::thread>(new boost::thread(boost::ref(*instance)));

		instances.push_back(instance);
		experimentThreads.push_back(newThread);
		experimentFiles.push_back(file);
	}

	Out << "Waiting for all threads to finish." << "\n";

	// wait for all threads to finish
	for (unsigned long iThread = 0; iThread<Options.GetThreads(); iThread++)
	{
		experimentThreads.at(iThread)->join();
		experimentFiles.at(iThread)->close();
	}

	boost::chrono::system_clock::time_point experimentStop = boost::chrono::system_clock::now();

	Out << "=================" << "\n";
	Out << "Experiment statistics:" << "\n";
	for (int i=0; i<DecisionSystemObj->GetDecisionMakerCount(); i++)
	{
		/*Out << "-----------------------------------------" << "\n";
		Out << "Decision maker #" << i << "\n";
		Out << Statistics.ToString(i, DecisionSystemObj->GetDecisionMakerName(i)) << "\n";*/

		// save statistics to the output file
		Statistics.SaveCSV(GetOutputCSVFilePath(i), i, DecisionSystemObj->GetDecisionMakerName(i));
		Statistics.SaveTXT(GetOutputTXTFilePath(i), i, DecisionSystemObj->GetDecisionMakerName(i));
	}

	boost::chrono::duration<double> experimentDuration = experimentStop - experimentStart;

	Out << "=================" << "\n";
	Out << "Total time: " << experimentDuration.count() << " seconds." << "\n";

	Options.SaveCSV(GetOptionsCSVFilePath());

	return true;
}

void Experiment::operator()()
{
	Out << "Starting thread for group #" << GroupID << "\n";

	if (!Initialize())
	{
		Out << "Failed to initialize experiment." << "\n";
	}

	std::string trialFilePath;
	unsigned long trialRepeat;

	while (OnStartNextTrial(trialFilePath, trialRepeat))
	{
		ActiveTrialFilePath = trialFilePath;

		if (!InitializeReadFromActiveTrialFile())
		{
			Out << "Failed to initialize reading from " << ActiveTrialFilePath << "\n";
			break;
		}

		cout << "Starting repeat #" << trialRepeat+1 << " of trial from " << ActiveTrialFilePath << " (" << IncrementExperimentCounter() << "%)" << "\n";

		boost::chrono::system_clock::time_point startTrial = boost::chrono::system_clock::now();

		switch (Experiment::threadingMode)
		{
		case THM_CALCULATION:
			if (!ExecuteExperimentTrial(GroupID, trialRepeat, ActiveTrialFilePath))
			{
				Out << "There was an error executing trial " << ActiveTrialFilePath << "\n";
				continue;
			}
			break;
		case THM_BELIEF_GENERATION:
			if (!RecordExperimentBeliefTrial(trialRepeat, ActiveTrialFilePath))
			{
				Out << "There was an error executing trial " << ActiveTrialFilePath << "\n";
				continue;
			}
			break;
		}

		boost::chrono::system_clock::time_point stopTrial = boost::chrono::system_clock::now();
		boost::chrono::duration<double> trialDuration = stopTrial - startTrial;
		cout << "Trial repeat completed in " << trialDuration << " sec \n";

		if (ActiveTrialFile.is_open())
		{
			ActiveTrialFile.close();
		}
	}
}

bool Experiment::GetTrialStartState(boost::shared_ptr<State>& realState, boost::shared_ptr<ModelObservation>& observation, boost::shared_ptr<ModelInput>& input)
{
	bool bTerminal;
	bool bResult = RealModel->NextStepFromFile(ActiveTrialFile, realState, observation, input, bTerminal, false, realState);

	return bResult;
}

bool Experiment::GetTrialStateEvaluation(boost::shared_ptr<State>& realState, MONITOR_ACTION_TYPE stepAction, MONITOR_ACTION_EVALUATION& evaluation)
{
	// evaluate action
	RealModel->EvaluateAction(realState, stepAction, evaluation);

	return true;
}

bool Experiment::GetTrialNextState(boost::shared_ptr<State>& realState, MONITOR_ACTION_TYPE stepAction, boost::shared_ptr<ModelObservation>& observation, boost::shared_ptr<ModelInput>& input, bool& bTerminal, bool bUpdateProperty, const boost::shared_ptr<State>& currentRealState)
{
	bool bResult = true;

	if (stepAction==ACTION_CONTINUE)
	{
		bResult = RealModel->NextStepFromFile(ActiveTrialFile, realState, observation, input, bTerminal, bUpdateProperty, currentRealState);
	}
	else
	{
		bTerminal = true;
	}

	return bResult;
}

bool Experiment::EvaluateStep(ExperimentCollector& data, int iStep, MONITOR_ACTION_TYPE decisionMakerAction, int maxFalseAlarmWait, boost::shared_ptr<State> realState, unsigned int realFailureTimeStep, bool bEarlyAlarmSupport, vector<double>& deltaRange)
{
	double immediateReward = 0;
	double immediatePenalty = 0;

	if (!GetTrialStateEvaluation(realState, decisionMakerAction, data.evaluation))
	{
		Out << "Unable to get state evaluation." << "\n";
		return false;
	}

	switch (data.evaluation)
	{
	case ACTION_EVAL_CORRECT_CONTINUE:
		/*if (data.bPossibleEarlyAlarm)
		{
			// since it was marked previously that there might have occurred an early alarm and currently system says that Continue is a consistent action
			// the previously raised early alarm was indeed a false alarm

			// NOT CLEAR: sometimes it happens that continue is selected after the first early alarm
			if (iStep-data.falseAlarmTimeStamp>=maxFalseAlarmWait)
			{
				data.evaluation = ACTION_EVAL_FALSE_ALARM;
				data.bTerminated = true;
			}
		}*/
		break;
	case ACTION_EVAL_CORRECT_ALARM:
		/*if (data.bPossibleEarlyAlarm)
		{
			data.evaluation = ACTION_EVAL_EARLY_ALARM;
			data.earlyAlarmPredictionTime = iStep-data.falseAlarmTimeStamp;

			// failure belief would be saved at time when alarm was considered false, but here it confirmed to be early
		}
		else
		{*/
			data.failureBelief = DecisionSystemObj->FailureProbability();
			data.deltaRange = deltaRange;

			if (realState->IsFailureState())
			{
				data.numStepsForFailureDetection = iStep - realFailureTimeStep;
			}
		//}

		break;
	case ACTION_EVAL_EARLY_ALARM:
		immediatePenalty = immediateReward;

		break;
	case ACTION_EVAL_MISSED_ALARM:
		immediatePenalty = immediateReward;

		data.bMissedAlarmBeforeTerminal = true;

		break;
	case ACTION_EVAL_FALSE_ALARM:
		{
			immediatePenalty = immediateReward;
			data.deltaRange = deltaRange;

			// In order to support cases when we don't know yet whether the trajectory is going to be good or bad in the end
			// Keep the belief and decide in the end if we will consider it in calculations
			data.failureBelief = DecisionSystemObj->FailureProbability();
			data.falseAlarmTimeStamp = iStep;

			// if this is the first false alarm
			/*if (!data.bPossibleEarlyAlarm && DecisionSystemObj->EarlyAlarmSupport(0))
			{
				data.falseAlarmTimeStamp = iStep;
				Out << "Potential early alarm detected. Letting system to run " << maxFalseAlarmWait << " more steps to confirm." << "\n";

				data.failureBelief = DecisionSystemObj->FailureProbability();
			}

			// verify that first false alarm was no more than maxFalseAlarmWait steps away
			if (iStep-data.falseAlarmTimeStamp<maxFalseAlarmWait && bEarlyAlarmSupport)
			{
				// if there was a false alarm we give it another chance
				// there might be the case of the early alarm
				// in order to check that we make the system to continue
				// if the next decision action will be ACTION_CONTINUE then this one is indeed a False Alarm
				// if the next one will be ACTION_RISE_ALARM again then we wait again till system confirm that it is indeed a Correct Alarm
				decisionMakerAction = ACTION_CONTINUE;
				data.bPossibleEarlyAlarm = true;

				// get updated reward value
				if (!GetTrialStateEvaluation(realState, decisionMakerAction, immediateReward, data.evaluation))
				{
					Out << "Unable to get state evaluation." << "\n";
					return false;
				}
			}
			*/
		}
		break;
	}

	if (decisionMakerAction == ACTION_ALARM)
	{
		data.bTerminated = true;
	}

	// accumulate reward
	data.totalReward += immediateReward;
	data.totalPenalty += immediatePenalty;

	return true;
}

bool Experiment::RecordExperimentBeliefTrial(unsigned int repeat, const std::string& trialFilePath)
{
	// set to configure whether recorded traces of property in each state should be ignored
	// if ignored then it will be regenerated according to currently configured property state evolution
	bool bIgnoreRecordedProperty = true;

	Out << "Starting trial from file: " << trialFilePath << ". Repeat #" << repeat <<".\n";

	boost::shared_ptr<ModelObservation> observation;
	boost::shared_ptr<State> realState;
	boost::shared_ptr<ModelInput> inputAfterRealState;
	BeliefState initialBelief;
	BeliefState tmpBelief;
	bool bTrialTerminal = false;

	GetTrialStartState(realState, observation, inputAfterRealState);

	RealModel->GetInitialBelief(initialBelief, realState, Options.GetBeliefStateDimension());
	DecisionSystemObj->SetInitialBelief(initialBelief);

	for (unsigned int iStep = 0; iStep<Options.GetMaxTrialSteps(); iStep++)
	{
		std::string beliefFile;
		if (GetBeliefStateFilePath(0, repeat, trialFilePath, iStep, beliefFile))
		{
			DecisionSystemObj->BeliefToFile(beliefFile);
		}

		// perform the step from the current real state
		boost::shared_ptr<ModelInput> prevInput = inputAfterRealState;

		if (!GetTrialNextState(realState, ACTION_CONTINUE, observation, inputAfterRealState, bTrialTerminal, bIgnoreRecordedProperty, realState))
		{
			Out << "Unable to get next state from the model." << "\n";
			break;
		}

		if (bTrialTerminal)
		{
			// terminal state reached, stopping the trial
			break;
		}

		// perform forward update for the decision system
		DecisionSystemObj->ForwardUpdate(ACTION_CONTINUE, observation, prevInput, tmpBelief);
	}

	RealModel->FinalizeHistoryFile();

	return true;
}


bool Experiment::ExecuteExperimentTrial(unsigned int groupID, unsigned int repeat, const std::string& trialFilePath)
{
	// check if recorded beliefs for specified repeat and trialFilePath exists
	bool bTrialBeliefDataExists = false;
	string recordedTrialBeliefsDir;
	bTrialBeliefDataExists = GetRecordedBeliefDirPath(repeat, trialFilePath, recordedTrialBeliefsDir);

	// set to configure whether recorded traces of property in each state should be ignored
	// if ignored then it will be regenerated according to currently configured property state evolution
	bool bIgnoreRecordedProperty = true;

	Out << "Starting trial from file: " << trialFilePath << ". Repeat #" << repeat <<".\n";

	boost::shared_ptr<ModelObservation> observation;
	boost::shared_ptr<State> realState;
	boost::shared_ptr<ModelInput> inputAfterRealState;
	BeliefState initialBelief;
	BeliefState tmpBeliefState;

	double maxRej2Increment = 0;
	double maxRejProb2 = 0;

	unsigned int maxFalseAlarmWait = Options.GetMaxDelayAfterFalseAlarm();
	int realFailureTimeStep = -1;			// indicates that failure did not occur previously
	bool bTrialTerminal = false;

	vector<ExperimentCollector> experimentData;
	for (int i=0; i<DecisionSystemObj->GetDecisionMakerCount(); i++) experimentData.push_back(ExperimentCollector());

	if (!GetTrialStartState(realState, observation, inputAfterRealState))
	{
		return false;
	}

	if (!bTrialBeliefDataExists)
	{
		RealModel->GetInitialBelief(initialBelief, realState, Options.GetBeliefStateDimension());
		DecisionSystemObj->SetInitialBelief(initialBelief);
	}
	else
	{
		fs::path beliefFilePath(recordedTrialBeliefsDir);
		beliefFilePath /=  boost::str(boost::format("%1%.csv") % 0);
		DecisionSystemObj->RestoreBeliefFromFile(beliefFilePath.string());
	}

	for (unsigned int iStep = 0; iStep<Options.GetMaxTrialSteps(); iStep++)
	{
		boost::chrono::system_clock::time_point startStep = boost::chrono::system_clock::now();

		Out << "*****************" << "\n";
		Out << "Starting step #" << iStep << "\n";
		Out << "Real state: " << realState->ToString() << "\n";
		Out << realState->ObservationToString(observation) << "\n";
		Out << "Belief state: " << DecisionSystemObj->BeliefToString(realState) << "\n";

		if (Options.GetSaveTrialBeliefs())
		{
			std::string beliefFile;
			if (GetBeliefStateFilePath(groupID, repeat, trialFilePath, iStep, beliefFile))
			{
				DecisionSystemObj->BeliefToFile(beliefFile);
			}
		}

		// check if the real state is failure state
		if (realState->IsFailureState() && realFailureTimeStep==-1)
		{
			// from this time we need to count how many more steps are needed for the decision maker to raise the alarm
			realFailureTimeStep = iStep;
		}

		// find the action to be executed according to the current knowledge
		vector<MONITOR_ACTION_TYPE> actions;
		vector<double> actionsDuration;
		vector<vector<double> > deltaRange;
		//double rej2Increment;
		//double rejProb1;
		//double rejProb2;

		DecisionSystemObj->GetAction(actions, actionsDuration);

		//DecisionSystemObj->GetDeltaRange(deltaRange);
		//DecisionSystemObj->GetRejectionProbabilityDifference(rej2Increment, rejProb1, rejProb2);
		/*if (rej2Increment>maxRej2Increment)
		{
			maxRej2Increment = rej2Increment;
		}

		if (rejProb2>maxRejProb2)
		{
			maxRejProb2 = rejProb2;
		}*/

		bool bAllTerminated = true;
		for (size_t iActionIndex = 0; iActionIndex<actions.size(); iActionIndex++)
		{
			MONITOR_ACTION_TYPE decisionMakerAction = actions.at(iActionIndex);

			if (experimentData[iActionIndex].bTerminated)
			{
				continue;
			}

			bool bEarlyAlarmSupport = false; //DecisionSystemObj->EarlyAlarmSupport(iActionIndex);

			/*if (!EvaluateStep(experimentData[iActionIndex], iStep, decisionMakerAction, maxFalseAlarmWait, realState, realFailureTimeStep, bEarlyAlarmSupport, deltaRange.at(iActionIndex)))
			{
				break;
			}*/

			vector<double> deltaRange;
			if (!EvaluateStep(experimentData[iActionIndex], iStep, decisionMakerAction, maxFalseAlarmWait, realState, realFailureTimeStep, bEarlyAlarmSupport, deltaRange))
			{
				break;
			}

			Out 	<< DecisionSystemObj->GetDecisionMakerName(iActionIndex)
					<< " - Selected action ( "
					<< actionsDuration.at(iActionIndex)
					<< " sec) = "
					<< ActionToString(decisionMakerAction)
					<< " ("
					<< EvaluationToString(experimentData[iActionIndex].evaluation)
					<< ")"
					<< "\n";

			if (!experimentData[iActionIndex].bTerminated) bAllTerminated = false;
		}

		if (bAllTerminated)
		{
			// terminal state reached, stopping the trial
			break;
		}

		// perform the step from the current real state
		boost::shared_ptr<ModelInput> prevInput = inputAfterRealState;

		if (!GetTrialNextState(realState, ACTION_CONTINUE, observation, inputAfterRealState, bTrialTerminal, bIgnoreRecordedProperty, realState))
		{
			Out << "Unable to get next state from the model." << "\n";
			break;
		}

		if (bTrialTerminal)
		{
			// terminal state reached, stopping the trial
			break;
		}

			boost::chrono::system_clock::time_point startForwardUpdate = boost::chrono::system_clock::now();

		// perform forward update for the decision system
		if (!bTrialBeliefDataExists)
		{
			DecisionSystemObj->ForwardUpdate(ACTION_CONTINUE, observation, prevInput, tmpBeliefState);
		}
		else
		{
			fs::path beliefFilePath(recordedTrialBeliefsDir);
			beliefFilePath /=  boost::str(boost::format("%1%.csv") % (iStep+1));
			DecisionSystemObj->RestoreBeliefFromFile(beliefFilePath.string());
		}

			boost::chrono::system_clock::time_point stopForwardUpdate = boost::chrono::system_clock::now();
			boost::chrono::duration<double> forwardUpdateDuration = stopForwardUpdate - startForwardUpdate;

		Out << "Forward update complete in " << forwardUpdateDuration.count() << " sec" << "\n";

		boost::chrono::system_clock::time_point stopStep = boost::chrono::system_clock::now();
		boost::chrono::duration<double> durationStep = stopStep - startStep;
		Out << "Step #" << iStep << " completed in " << durationStep << " seconds\n";
	}

	bool bGoodTrial = true;
	if (bTrialTerminal)
	{
		bGoodTrial = !realState->IsFailureState();
	}
	else
	{
		for (unsigned int iStep = 0; iStep<Options.GetMaxTrialSteps(); iStep++)
		{
			// check if the real state is failure state
			if (realState->IsFailureState())
			{
				bGoodTrial = false;
				break;
			}

			if (!GetTrialNextState(realState, ACTION_CONTINUE, observation, inputAfterRealState, bTrialTerminal, bIgnoreRecordedProperty, realState))
			{
				Out << "Unable to get next state from the model." << "\n";
				break;
			}

			if (bTrialTerminal)
			{
				// terminal state reached, stopping the trial
				break;
			}
		}
	}

	if (!bGoodTrial)
	{
		// change those evaluated as false alarm into correct alarm, and assume that prediction was correct
		// for now keep the belief the same and assume that time to raise an alarm was 0
		for (int iDecisionMaker=0; iDecisionMaker<DecisionSystemObj->GetDecisionMakerCount(); iDecisionMaker++)
		{
			if (experimentData[iDecisionMaker].evaluation == ACTION_EVAL_FALSE_ALARM)
			{
				//experimentData[iDecisionMaker].evaluation = ACTION_EVAL_CORRECT_ALARM;
				experimentData[iDecisionMaker].evaluation = ACTION_EVAL_EARLY_ALARM;

				// Old implementation assumed that alarms raised early don't contribute to the value of the MTIME
				//experimentData[iDecisionMaker].numStepsForFailureDetection = -1;

				// Negative time to detect the failure - early failure detection
				experimentData[iDecisionMaker].numStepsForFailureDetection = experimentData[iDecisionMaker].falseAlarmTimeStamp - realFailureTimeStep;
			}
		}
	}
	else
	{
		for (int iDecisionMaker=0; iDecisionMaker<DecisionSystemObj->GetDecisionMakerCount(); iDecisionMaker++)
		{
			if (experimentData[iDecisionMaker].evaluation == ACTION_EVAL_FALSE_ALARM)
			{
				experimentData[iDecisionMaker].failureBelief = 0;
			}
		}
	}

	// save the statics info on whether monitor operation was correct or faulty
	{
		boost::lock_guard<boost::recursive_mutex> _(TrialStatisticsMutex);
		string trialID;
		trialID = boost::str(boost::format("%1%\\%2%\\%3%") % groupID % repeat % trialFilePath);

		for (int iDecisionMaker=0; iDecisionMaker<DecisionSystemObj->GetDecisionMakerCount(); iDecisionMaker++)
		{
			/*cout << experimentData[iDecisionMaker].evaluation << "\n";
			cout << experimentData[iDecisionMaker].numStepsForFailureDetection << "\n";
			cout << experimentData[iDecisionMaker].failureBelief << "\n";*/

			/*if (experimentData[iDecisionMaker].evaluation==ACTION_EVAL_CORRECT_ALARM)
			{
				cout << experimentData[iDecisionMaker].evaluation << "\n";
				cout << experimentData[iDecisionMaker].numStepsForFailureDetection << "\n";
			}*/

			/*if (experimentData[iDecisionMaker].alarmRejectionProbDiff_2_1<0.01 && bGoodTrial && experimentData[iDecisionMaker].evaluation==ACTION_EVAL_FALSE_ALARM)
			{
				cout << "Good zero" << "\n";
			}*/

			Statistics.AddEvaluation(trialID, bGoodTrial, experimentData[iDecisionMaker].deltaRange, maxRej2Increment, maxRejProb2,
					iDecisionMaker,
					DecisionSystemObj->GetDecisionMakerName(iDecisionMaker),
					experimentData[iDecisionMaker].evaluation,
					experimentData[iDecisionMaker].earlyAlarmPredictionTime,
					experimentData[iDecisionMaker].failureBelief,
					experimentData[iDecisionMaker].numStepsForFailureDetection,
					experimentData[iDecisionMaker].totalReward,
					experimentData[iDecisionMaker].totalPenalty,
					experimentData[iDecisionMaker].bMissedAlarmBeforeTerminal);
		}
	}

	RealModel->FinalizeHistoryFile();

	Out << "=================" << "\n";
	Out << "=================" << "\n";

	return true;
}

bool Experiment::GenerateBeliefTrials()
{
	if (!RestoreTrialsList())
	{
		Out << "Failed to restore list of files containing trials data." << "\n";
		return false;
	}

	int nTotalExperiments = TrialFilesMap.size()* Options.GetTrialsRepeats();
	ResetExperimentCounter(nTotalExperiments);

/*	std::string trialFilePath;
	unsigned long trialRepeat;

	while (OnStartNextTrial(trialFilePath, trialRepeat))
	{
		ActiveTrialFilePath = trialFilePath;

		if (!InitializeReadFromActiveTrialFile())
		{
			Out << "Failed to initialize reading from " << ActiveTrialFilePath << "\n";
			break;
		}

		cout << "Starting repeat #" << trialRepeat+1 << " of trial from " << ActiveTrialFilePath << " (" << IncrementExperimentCounter() << "%)" << "\n";
		boost::chrono::system_clock::time_point startTrial = boost::chrono::system_clock::now();

		if (!RecordExperimentBeliefTrial(trialRepeat, ActiveTrialFilePath))
		{
			Out << "There was an error executing trial " << ActiveTrialFilePath << "\n";
			continue;
		}

		boost::chrono::system_clock::time_point stopTrial = boost::chrono::system_clock::now();
		boost::chrono::duration<double> trialDuration = stopTrial - startTrial;
		cout << "Trial repeat completed in " << trialDuration << " sec \n";

		if (ActiveTrialFile.is_open())
		{
			ActiveTrialFile.close();
		}
	}*/

	vector<boost::shared_ptr<Experiment> > instances;
	vector<boost::shared_ptr<boost::thread> > experimentThreads;
	vector<boost::shared_ptr<ofstream> > experimentFiles;

	threadingMode = THM_BELIEF_GENERATION;

	for (unsigned long iThread = 0; iThread<Options.GetThreads(); iThread++)
	{
		Out << "Starting thread #" << iThread << "\n";

		// create file for the thread output
		boost::shared_ptr<ofstream> file = boost::shared_ptr<ofstream>(new ofstream());
		file->open(GetGroupLogFilePath(iThread).c_str(), ios::out);

		// create instance to run the thread
		boost::shared_ptr<Experiment> instance = boost::shared_ptr<Experiment>(new Experiment(Options, file->rdbuf(), iThread));

		// start the thread
		boost::shared_ptr<boost::thread> newThread =
				boost::shared_ptr<boost::thread>(new boost::thread(boost::ref(*instance)));

		instances.push_back(instance);
		experimentThreads.push_back(newThread);
		experimentFiles.push_back(file);
	}

	Out << "Waiting for all threads to finish." << "\n";

	// wait for all threads to finish
	for (unsigned long iThread = 0; iThread<Options.GetThreads(); iThread++)
	{
		experimentThreads.at(iThread)->join();
		experimentFiles.at(iThread)->close();
	}

	return true;
}

bool Experiment::GenerateExperimentTrials()
{
	if (!MakeTrialsDir())
	{
		Out << "Unable to create trials directory" << "\n";
		return false;
	}

	vector<unsigned int> badRuns;

	vector< boost::shared_ptr<State> > trialStateSequence;
	vector< boost::shared_ptr<ModelInput> > trialStepInputs;

	for (unsigned int iTrial = 0; iTrial<Options.GetTrials(); iTrial++)
	{
		trialStateSequence.clear();
		trialStepInputs.clear();

		boost::shared_ptr<ModelObservation> observation;

		//RealModel->InitializeHistoryFile(Options.GetTrialsDirPath(), 0, iTrial, true);

		boost::shared_ptr<State> realState = RealModel->CreateStartState();

		RealModel->GetStateObservation(realState, observation);

		boost::shared_ptr<ModelInput> stepInput = RealModel->GetInputAt(realState);
		//RealModel->AddHistoryState(realState, observation, stepInput, false);

		trialStateSequence.push_back(realState);
		trialStepInputs.push_back(stepInput);

		//BeliefState initialBelief;
		//RealModel->GetInitialBelief(initialBelief, realState, Options.GetBeliefStateDimension());

		//double reward;
		MONITOR_ACTION_EVALUATION evaluation;

		bool badRunDetected = false;

		boost::shared_ptr<State> prevState = realState;

		for (unsigned int iStep = 0; iStep<Options.GetMaxTrialSteps(); iStep++)
		{
			boost::shared_ptr<ModelInput> actualInput;

			boost::shared_ptr<State> nextState = RealModel->CreateNewState();
			nextState->Copy(prevState);

			bool bTerminal = RealModel->Step(nextState, ACTION_CONTINUE, stepInput, actualInput);

			if (bTerminal)
			{
				// terminal state reached, stopping the trial
				break;
			}

			// get evaluation of the state
			RealModel->EvaluateAction(nextState, ACTION_CONTINUE, evaluation);

			if (evaluation==ACTION_EVAL_MISSED_ALARM && !badRunDetected)
			{
				badRunDetected = true;
				badRuns.push_back(iTrial);
			}

			// get observation from the real model
			RealModel->GetStateObservation(nextState, observation);

			stepInput = RealModel->GetInputAt(nextState);
			//RealModel->AddHistoryState(realState, observation, stepInput, bTerminal);

			trialStateSequence.push_back(nextState);
			trialStepInputs.push_back(stepInput);

			prevState = nextState;
		}

		//RealModel->FinalizeHistoryFile();

		for (unsigned int iOutputVariant = 0; iOutputVariant<Options.GetOutputVariantsPerTrial(); iOutputVariant++)
		{
			RealModel->InitializeHistoryFile(Options.GetTrialsDirPath(), 0, iTrial, true);

			vector< boost::shared_ptr<ModelInput> >::iterator iRealStateInput = trialStepInputs.begin();
			for (vector< boost::shared_ptr<State> >::iterator iRealState = trialStateSequence.begin();
					iRealState!=trialStateSequence.end(); iRealState++, iRealStateInput++)
			{
				RealModel->GetStateObservation(*iRealState, observation);
				RealModel->AddHistoryState(*iRealState, observation, *iRealStateInput, false);
			}

			RealModel->FinalizeHistoryFile();
		}
	}

	fstream statisticsFile;
	statisticsFile.open(GetGeneratedTrialsStatisticsFile().c_str(), fstream::out);
	if (!statisticsFile.is_open())
	{
		return false;
	}

	statisticsFile << "Total trials = " << Options.GetTrials() << "\n";
	statisticsFile << "Good trials = " << Options.GetTrials() - badRuns.size() << "\n";
	statisticsFile << "Bad trials = " << badRuns.size() << "\n";
	for (unsigned int i = 0; i<badRuns.size(); i++)
	{
		statisticsFile << "\t" << badRuns.at(i) << "\n";
	}
	statisticsFile.close();

	return true;
}

std::string Experiment::ActionToString(MONITOR_ACTION_TYPE action)
{
	switch (action)
	{
	case ACTION_CONTINUE:		return "Continue";
	case ACTION_ALARM:		return "Alarm";
	}
	return "Undefined";
}

std::string Experiment::EvaluationToString(MONITOR_ACTION_EVALUATION evaluation)
{
	switch (evaluation)
	{
	case ACTION_EVAL_CORRECT_CONTINUE:			return "Correct Continue";
	case ACTION_EVAL_CORRECT_ALARM:				return "Correct Alarm";
	case ACTION_EVAL_EARLY_ALARM:				return "Early Alarm";
	case ACTION_EVAL_MISSED_ALARM:				return "Missed Alarm";
	case ACTION_EVAL_FALSE_ALARM:				return "False Alarm";
	}

	return "Undefined";
}

bool Experiment::MakeExperimentDir()
{
	fs::path experimentDirPath(Options.GetExperimentDir());

	try {
		fs::create_directory(experimentDirPath);
	}
	catch (const boost::filesystem::filesystem_error& e)
	{
		return false;
	}

	return true;
}

bool Experiment::MakeTrialsDir()
{
	fs::path trialsDirPath(Options.GetTrialsDirPath());

	try {
		fs::create_directory(trialsDirPath);
	}
	catch (const boost::filesystem::filesystem_error& e)
	{
		return false;
	}

	return true;
}

std::string Experiment::GetGeneratedTrialsStatisticsFile()
{
	fs::path historyDirPath(Options.GetTrialsDirPath());
	historyDirPath /= "statistics.txt";

	return historyDirPath.string();
}

std::string Experiment::GetOptionsCSVFilePath()
{
	fs::path historyDirPath(Options.GetExperimentDir());
	historyDirPath /= "options.csv";

	return historyDirPath.string();
}

std::string Experiment::GetOutputCSVFilePath(int iDecisionMaker)
{
	fs::path historyDirPath(Options.GetExperimentDir());

	historyDirPath /= boost::str(boost::format("output%1%.csv") % iDecisionMaker);

	return historyDirPath.string();
}

std::string Experiment::GetOutputTXTFilePath(int iDecisionMaker)
{
	fs::path historyDirPath(Options.GetExperimentDir());

	historyDirPath /= boost::str(boost::format("output%1%.txt") % iDecisionMaker);

	return historyDirPath.string();
}

bool Experiment::OnStartNextTrial(std::string& trialFilePath, unsigned long& trialRepeat)
{
	boost::lock_guard<boost::recursive_mutex> _(TrialsCounterMutex);

	map<string, unsigned long>::iterator iTrial;
	for (iTrial=TrialFilesMap.begin(); iTrial!=TrialFilesMap.end(); iTrial++)
	{
		if (iTrial->second==0)
		{
			continue;
		}

		trialFilePath = iTrial->first;
		trialRepeat = Options.GetTrialsRepeats() - iTrial->second;

		iTrial->second--;

		cout << "Starting new trial for " << trialFilePath << ". " << iTrial->second << " more are left." << endl;

		return true;
	}

	/*if (!TrialFilesQueue.empty())
	{
		std::string filePath = TrialFilesQueue.front();
		TrialFilesQueue.pop_front();

		ActiveTrialFilePath = filePath;

		cout << "Starting new trial. " << TrialFilesQueue.size() << " more are left." << endl;
		return true;
	}*/

	return false;
}

bool Experiment::InitializeReadFromActiveTrialFile()
{
	if (ActiveTrialFile.is_open())
	{
		ActiveTrialFile.close();
	}

	ActiveTrialFile.open(ActiveTrialFilePath.c_str(), fstream::in);

	if (!ActiveTrialFile.is_open())
	{
		Out << "Unable to open trial history file: " << ActiveTrialFilePath << "\n";
		return false;
	}

	string headerLine;
	getline(ActiveTrialFile, headerLine);

	return true;
}

std::string Experiment::GetGroupLogFilePath(unsigned int groupID)
{
	fs::path logPath(Options.GetExperimentDir());
	logPath /= boost::str(boost::format("Log_%1%.txt") % groupID);
	return logPath.string();
}

std::string Experiment::GetExperimentBeliefsDir()
{
	fs::path beliefsDir(Options.GetExperimentDir());
	beliefsDir /= "Beliefs";
	return beliefsDir.string();
}

bool Experiment::GetRecordedBeliefDirPath(unsigned int repeat, const std::string& trialFilePath, std::string& recordedTrialBeliefsDir)
{
	fs::path trialPath(trialFilePath);
	std::string trialName = trialPath.filename().string();

	fs::path recordedTrailBeliefDirPath;
	recordedTrailBeliefDirPath = trialPath.parent_path();
	recordedTrailBeliefDirPath /= "Beliefs";
	recordedTrailBeliefDirPath /= trialName;
	recordedTrailBeliefDirPath /= boost::str(boost::format("%1%") % repeat);

	if (fs::exists(recordedTrailBeliefDirPath))
	{
		recordedTrialBeliefsDir = recordedTrailBeliefDirPath.string();
		return true;
	}

	return false;
}

bool Experiment::GetBeliefStateFilePath(unsigned int groupID, unsigned int repeat, const std::string& trialFilePath, unsigned int iStep, std::string& beliefFilePath)
{
	fs::path trialPath(trialFilePath);
	std::string trialName = trialPath.filename().string();

	if (!fs::exists(fs::path(GetExperimentBeliefsDir())))
	{
		try {
			fs::create_directory(GetExperimentBeliefsDir());
		}
		catch (const boost::filesystem::filesystem_error& e)
		{
			return false;
		}
	}

	fs::path beliefsDirPath(GetExperimentBeliefsDir());
	beliefsDirPath /= boost::str(boost::format("%1%") % trialName);

	if (!fs::exists(fs::path(beliefsDirPath.string())))
	{
		try {
			fs::create_directory(beliefsDirPath.string());
		}
		catch (const boost::filesystem::filesystem_error& e)
		{
			return false;
		}
	}

	beliefsDirPath /= boost::str(boost::format("%1%") % repeat);
	if (!fs::exists(fs::path(beliefsDirPath.string())))
	{
		try {
			fs::create_directory(beliefsDirPath.string());
		}
		catch (const boost::filesystem::filesystem_error& e)
		{
			return false;
		}
	}

	beliefsDirPath /= boost::str(boost::format("%1%.csv") % iStep);

	beliefFilePath = beliefsDirPath.string();

	return true;
}

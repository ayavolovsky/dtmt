/*
 * ExperimentGenerator.h
 *
 *  Created on: Jul 1, 2013
 *      Author: ayavol2
 */

#ifndef EXPERIMENTGENERATOR_H_
#define EXPERIMENTGENERATOR_H_

#include <map>
#include <vector>
#include <string>
#include "SolverOptions.h"

class ExperimentGenerator {
public:
	ExperimentGenerator(SolverOptions& options);
	virtual ~ExperimentGenerator();

	bool CreateExperimentSet();
private:
	void DefineExperiments_VaryingPenalties();
	void DefineExperiments_VaryingConsistentFailure();
	void DefineExperiments_VaryingFailureContinue();
	void DefineExperiments_VaryingThreashold();

	void SaveExperimentsSet();

	bool MakeExperimentSetDir();
	std::string GetExperimentSetDir();

	std::string GetExperimentDir(const std::string& experimentName);
	bool MakeExperimentDir(const std::string& experimentName);

	std::string GetExperimentSetBatchFilePath();

	std::string GetExperimentIniFilePath(const std::string& experimentName);

	std::map<std::string, SolverOptions> experiments;
	std::vector<std::string> experimentsOrder;
	SolverOptions Options;
};

#endif /* EXPERIMENTGENERATOR_H_ */

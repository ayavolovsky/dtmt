/*
 * CommandLineHandler.h
 *
 *  Created on: May 14, 2013
 *      Author: ayavol2
 */

#ifndef COMMANDLINEHANDLER_H_
#define COMMANDLINEHANDLER_H_

#include <string>

class SolverOptions;

class CommandLineHandler {
	CommandLineHandler();
	static CommandLineHandler* pInstance;
public:
	static CommandLineHandler* Instance();
	virtual ~CommandLineHandler();

	bool ParseCommandLine(int argc, const char* argv[], SolverOptions& options, const std::string& iniFilePath = std::string(""));

	bool ParseConfigFile(const std::string& iniFilePath, SolverOptions& options);
};

#endif /* COMMANDLINEHANDLER_H_ */

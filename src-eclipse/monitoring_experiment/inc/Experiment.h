/*
 * POMCPExperiment.h
 *
 *  Created on: May 14, 2013
 *      Author: ayavol2
 */

#ifndef POMCPEXPERIMENT_H_
#define POMCPEXPERIMENT_H_

#include "SolverOptions.h"
#include <boost/shared_ptr.hpp>
#include "MonitoringDefs.h"
#include "ExperimentStatistics.h"
#include <boost/thread.hpp>
#include "Model.h"

class Model;
class DecisionSystem;
class ModelInputFunction;

class Experiment {
public:
	struct ExperimentCollector
	{
		ExperimentCollector()
		{
			evaluation = ACTION_EVAL_CORRECT_CONTINUE;
			bTerminated = false;
			bPossibleEarlyAlarm = false;
			falseAlarmTimeStamp = -1;
			earlyAlarmPredictionTime = 0;
			failureBelief = 0.0;
			numStepsForFailureDetection = -1;
			totalReward = 0;
			totalPenalty = 0;
			bMissedAlarmBeforeTerminal = false;
		}

		MONITOR_ACTION_EVALUATION evaluation;
		bool bPossibleEarlyAlarm;
		bool bTerminated;
		unsigned int falseAlarmTimeStamp;
		unsigned int earlyAlarmPredictionTime;
		double failureBelief;
		int numStepsForFailureDetection;
		long totalReward;
		long totalPenalty;
		bool bMissedAlarmBeforeTerminal;
		std::vector<double> deltaRange;
	};

	enum ThreadMode
	{
		THM_CALCULATION,
		THM_BELIEF_GENERATION
	};
public:
	Experiment(const SolverOptions& options, std::streambuf* outBuf, unsigned int groupID = 0);
	virtual ~Experiment();

	// starts the experiment with given options
	bool Start();

	// Threading support
	void operator()();
	static ThreadMode threadingMode;
private:
	// initialize all the data structures needed for the experiment
	bool Initialize();

	// executes experiment trials according to the options
	bool ExecuteExperimentsTrials();

	//
	bool ExecuteExperimentBeliefState();

	// executes single trial
	bool ExecuteExperimentTrial(unsigned int groupID, unsigned int repeat, const std::string& trialFilePath);
	bool RecordExperimentBeliefTrial(unsigned int repeat, const std::string& trialFilePath);

	bool EvaluateStep(ExperimentCollector& data, int iStep, MONITOR_ACTION_TYPE decisionMakerAction, int maxFalseAlarmWait, boost::shared_ptr<State> realState, unsigned int realFailureTimeStep, bool bEarlyAlarmSupport, std::vector<double>& deltaRange);

	bool GenerateExperimentTrials();
	bool GenerateBeliefTrials();

	std::string ActionToString(MONITOR_ACTION_TYPE action);

	std::string EvaluationToString(MONITOR_ACTION_EVALUATION evaluation);

	bool MakeExperimentDir();
	bool MakeTrialsDir();

	std::string GetGeneratedTrialsStatisticsFile();

	std::string GetOptionsCSVFilePath();
	std::string GetOutputCSVFilePath(int iDecisionMaker);
	std::string GetOutputTXTFilePath(int iDecisionMaker);
	std::string GetGroupLogFilePath(unsigned int groupID);
	bool GetBeliefStateFilePath(unsigned int groupID, unsigned int repeat, const std::string& trialFilePath, unsigned int iStep, std::string& beliefFilePath);
	bool GetRecordedBeliefDirPath(unsigned int repeat, const std::string& trialFilePath, std::string& recordedTrialBeliefsDir);
	std::string GetExperimentBeliefsDir();

	bool OnStartNextTrial(std::string& trialFilePath, unsigned long& trialRepeat);
	bool InitializeReadFromActiveTrialFile();

	bool RestoreTrialsList();

	void AddExperiments(std::vector<double>& correspondingThresholds);
	void AddExperiments_old(std::vector<double>& correspondingThresholds);
	void AddExperiments_CASE2016(std::vector<double>& correspondingThresholds);
	void AddExperiments_CASE2016_parametrized(std::vector<double>& correspondingThresholds, double La, double Gc);
	void AddExperiments_RV2016(std::vector<double>& correspondingThresholds);
	void AddExperiments_threshold(std::vector<double>& correspondingThresholds);
	void AddExperiments_beliefBased(std::vector<double>& correspondingThresholds);

	void AddPOMCPExperiment(double p, double cc, double fc, double fa);

	static void ResetExperimentCounter(int nTotalExperiments);
	static double IncrementExperimentCounter();
private:
	SolverOptions Options;
	unsigned int GroupID;
	std::fstream ActiveTrialFile;
	std::string ActiveTrialFilePath;

	static unsigned int TrialsToExecute;
	static boost::recursive_mutex TrialsCounterMutex;
	static boost::recursive_mutex TrialStatisticsMutex;
	static boost::recursive_mutex ExperimentsCounterMutex;
	static std::map<std::string, unsigned long> TrialFilesMap;

	static ExperimentStatistics Statistics;

	static int ExperimentCounter;
	static int TotalExperiments;

	boost::shared_ptr<Model> RealModel;
	boost::shared_ptr<Model> SimulationModel;
	boost::shared_ptr<ModelInputFunction> InputFunction;
	boost::shared_ptr<DecisionSystem> DecisionSystemObj;

	std::ostream Out;
private:
	bool GetTrialStartState(boost::shared_ptr<State>& realState, boost::shared_ptr<ModelObservation>& observation, boost::shared_ptr<ModelInput>& input);

	bool GetTrialNextState(boost::shared_ptr<State>& realState, MONITOR_ACTION_TYPE stepAction, boost::shared_ptr<ModelObservation>& observation, boost::shared_ptr<ModelInput>& input, bool& bTerminal, bool bUpdateProperty, const boost::shared_ptr<State>& currentRealState);

	bool GetTrialStateEvaluation(boost::shared_ptr<State>& realState, MONITOR_ACTION_TYPE stepAction, MONITOR_ACTION_EVALUATION& evaluation);
};

#endif /* POMCPEXPERIMENT_H_ */

/*
 * ReportGenerator.h
 *
 *  Created on: Jun 14, 2013
 *      Author: ayavol2
 */

#ifndef REPORTGENERATOR_H_
#define REPORTGENERATOR_H_

#include <string>
#include <map>
#include <list>
#include "SolverOptions.h"

class ReportGenerator {
public:
	ReportGenerator();
	virtual ~ReportGenerator();

	bool CreateReport(const std::string& reportFilePath);
private:
	bool ReadExperimentResults(const std::string& experimentName, const std::string& optionsFilePath, const std::string& outFilePath);

	bool ParseCSVFile(const std::string& filePath, std::map<std::string, std::string>& csvParams, std::list<std::string>& paramsOrder);

	bool SaveReport(std::fstream& f);

	std::map<std::string /*experiment*/, std::map<std::string /*parameter*/, std::string /*value*/> > ExperimentResults;
	std::map<std::string /*experiment*/, std::map<std::string /*parameter*/, std::string /*value*/> > ExperimentOptions;

	std::list<std::string> ParametersOrder;
	std::list<std::string> ExperimentsOrder;
};

#endif /* REPORTGENERATOR_H_ */

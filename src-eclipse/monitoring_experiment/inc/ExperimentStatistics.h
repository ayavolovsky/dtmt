/*
 * ExperimentStatistics.h
 *
 *  Created on: May 21, 2013
 *      Author: ayavol2
 */

#ifndef EXPERIMENTSTATISTICS_H_
#define EXPERIMENTSTATISTICS_H_

#include "MonitoringDefs.h"
#include <string>
#include <map>
#include <vector>

class ExperimentStatistics {
public:
	struct TrialStatistics
	{
		MONITOR_ACTION_EVALUATION evaluation;
		bool goodTrial;

		std::vector<double> deltaRange;
		double maxRej2Increment;
		double maxRej2Prob;

		int earlyAlarmPredictionTime;
		double failureBelief;
		int failureDetectionTime;
		long totalReward;
		long totalPenalty;
		bool missedAlarmOccured;
	};

	struct MultiTrialStatistics
	{
		std::vector<TrialStatistics> multiTrialData;
	};
private:
	struct CalculatedStatistics
	{
		unsigned int totalGoodTrials;
		unsigned int totalBadTrials;
		unsigned int totalTrialsWithMissedAlarm;

		double averageEarlyPredictionTime;
		int minEarlyPredictionTime;
		int maxEarlyPredictionTime;

		double averageFailureBelief;
		double minFailureBelief;
		std::string minFailureBeliefTrialID;
		double maxFailureBelief;
		std::string maxFailureBeliefTrialID;

		double averageFailureDetectionTime;
		int minFailureDetectionTime;
		std::string minFailureDetectionTimeTrialID;
		int maxFailureDetectionTime;
		std::string maxFailureDetectionTimeTrialID;
		int modeFailureDetectionTime;

		double averageTotalReward;
		double minTotalReward;
		double maxTotalReward;

		double averageTotalPenalty;
		double minTotalPenalty;
		double maxTotalPenalty;

		std::map<std::string, double> averageTrialReward;
		std::string strBadTrialsWithAlarmOffset;

		std::string strBadRejectedTrialsByTime;
		std::map<int, unsigned int> rejectedAtTimeCounter;

		/*TMP*/
		double maxBadDelta;
		double minAllGoodDelta;
		double rateFixedFalseAlarmsByMaxBadDelta;
		double maxRej2Increment;
		double maxRej2ProbGood;
	};
public:
	ExperimentStatistics();
	virtual ~ExperimentStatistics();

	void Clear();

	void AddEvaluation(const std::string& trialID, bool bGoodTrial, std::vector<double>& deltaRange, double maxRej2Increment, double maxRej2Prob, int iDecisionMaker, const std::string& decisionMakerName, MONITOR_ACTION_EVALUATION evaluation, int earlyAlarmPredictionTime, double failureBelief, int timeFailureDetection, long totalReward, double totalPenalty, bool bMissedAlarmBeforeTerminal);

	// calculates the Acceptance Accuracy
	double AA(std::map<MONITOR_ACTION_EVALUATION, std::vector<std::string> >& evaluationTrials);

	// calculates the Rejection Accuracy
	double RA(std::map<MONITOR_ACTION_EVALUATION, std::vector<std::string> >& evaluationTrials);

	// wraps up the evaluations
	void GetEvaluationStatistics(int decisionMakerIndex, std::map<MONITOR_ACTION_EVALUATION, std::vector<std::string> >& evaluationTrials, CalculatedStatistics& calcStats);

	double GetAverageFailureBelief(int nDecisionMakerIndex);

	std::string ToString(int nDecisionMakerIndex, const std::string& decisionMakerName);

	bool SaveCSV(const std::string& filePath, int nDecisionMakerIndex, const std::string& decisionMakerName);

	bool SaveTXT(const std::string& filePath, int nDecisionMakerIndex, const std::string& decisionMakerName);

	bool AppendComparison_CSV(const std::string& filePath, ExperimentStatistics& other, int nDecisionMakerIndex);

	bool AppendComparison_TXT(const std::string& filePath, ExperimentStatistics& other, int nDecisionMakerIndex);
private:
	std::map<int/*decision maker index*/, std::map<std::string, MultiTrialStatistics > > DecisionMakerEvaluations;

	std::map<int/*decision maker index*/, bool> bEvaluationStatisticaValid;
	std::map<MONITOR_ACTION_EVALUATION, std::vector<std::string> > EvaluationTrials;
	std::map<int, std::string> DecisionMakerNamesByID;
	CalculatedStatistics CalcStats;
};

#endif /* EXPERIMENTSTATISTICS_H_ */

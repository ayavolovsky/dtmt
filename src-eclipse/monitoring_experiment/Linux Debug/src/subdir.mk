################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/CommandLineHandler.cpp \
../src/Experiment.cpp \
../src/ExperimentGenerator.cpp \
../src/ExperimentStatistics.cpp \
../src/ReportGenerator.cpp \
../src/monitoring-experiment.cpp 

OBJS += \
./src/CommandLineHandler.o \
./src/Experiment.o \
./src/ExperimentGenerator.o \
./src/ExperimentStatistics.o \
./src/ReportGenerator.o \
./src/monitoring-experiment.o 

CPP_DEPS += \
./src/CommandLineHandler.d \
./src/Experiment.d \
./src/ExperimentGenerator.d \
./src/ExperimentStatistics.d \
./src/ReportGenerator.d \
./src/monitoring-experiment.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I"/home/andrey/Documents/src-eclipse/monitoring_experiment/inc" -I"/home/andrey/Documents/src-eclipse/monitoring_common/inc" -O0 -g3 -Wall -c -fmessage-length=0 -pthread -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '



/*
 * BeliefState.cpp
 *
 *  Created on: May 16, 2013
 *      Author: ayavol2
 */

#include "BeliefState.h"
#include "Model.h"

BeliefState::BeliefState() {
	// TODO Auto-generated constructor stub

}

BeliefState::~BeliefState() {
	// TODO Auto-generated destructor stub
}

void BeliefState::AddParticle(boost::shared_ptr<State> particle)
{
	Particles.push_back(particle);
}

void BeliefState::Clear()
{
	Particles.clear();
}

bool BeliefState::CopyRandomSample(boost::shared_ptr<State> copy)
{
	if (0==Particles.size())
	{
		return false;
	}

	int index = rand() % Particles.size();
	boost::shared_ptr<State> part = Particles.at(index);
	copy->Copy(part);

	return true;
}

boost::shared_ptr<State> BeliefState::GetParticle(unsigned int nParticle) const
{
	if (nParticle<0 || nParticle>=Particles.size())
	{
		return boost::shared_ptr<State>();
	}

	return Particles.at(nParticle);
}

bool BeliefState::CopyParticle(unsigned int nParticle, boost::shared_ptr<State> copy) const
{
	if (nParticle<0 || nParticle>=Particles.size())
	{
		return false;
	}

	boost::shared_ptr<State> part = Particles.at(nParticle);
	copy->Copy(part);

	return true;
}

unsigned int BeliefState::GetSize() const
{
	return Particles.size();
}

bool BeliefState::GetHorizonRejectionProbability(unsigned int horizon, double& rejectionProbability)
{
	std::map< unsigned int, double >::iterator iProb = HorizonRejectionProbability.find(horizon);
	if (iProb==HorizonRejectionProbability.end())
	{
		return false;
	}

	rejectionProbability = iProb->second;
	return true;
}

void BeliefState::SetHorizonRejectionProbability(unsigned int horizon, double rejectionProbability)
{
	std::map< unsigned int, double >::iterator iProb = HorizonRejectionProbability.find(horizon);
	if (iProb==HorizonRejectionProbability.end())
	{
		HorizonRejectionProbability[horizon] = rejectionProbability;
	}
}

void BeliefState::ResetHorizonRejectionProbability()
{
	HorizonRejectionProbability.clear();
}

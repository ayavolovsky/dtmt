/*
 * DecisionMaker.cpp
 *
 *  Created on: May 14, 2013
 *      Author: ayavol2
 */

#include "DecisionMaker.h"

using namespace std;

DecisionMaker::DecisionMaker()
{
}

DecisionMaker::~DecisionMaker() {
	// TODO Auto-generated destructor stub
}

DecisionSystem* DecisionMaker::GetDecisionSystem()
{
	return parentDecisionSystem;
}

void DecisionMaker::SetDecisionSystem(DecisionSystem* system)
{
	parentDecisionSystem = system;
}

void DecisionMaker::SetInputFunctionKnown(bool bknown)
{
	bKnownInputFunction = bknown;
}

bool DecisionMaker::IsInputFunctionKnown()
{
	return bKnownInputFunction;
}

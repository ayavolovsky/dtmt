/*
 * DecisionSystem.cpp
 *
 *  Created on: Feb 23, 2015
 *      Author: ayavol2
 */

#include "DecisionSystem.h"
#include <fstream>
#include <map>
#include <boost/tokenizer.hpp>
#include <boost/chrono.hpp>
#include "DecisionCommon.h"
#include "DecisionMaker.h"

using namespace std;

DecisionSystem::DecisionSystem(boost::shared_ptr<Model> simModel, std::streambuf* outBuf) {
	SetOutput(outBuf);
	SimulationModel = simModel;
}

DecisionSystem::~DecisionSystem() {
	// TODO Auto-generated destructor stub
}

/*virtual*/ void DecisionSystem::SetInitialBelief(BeliefState& belief)
{
	CurrentState = belief;
}

boost::shared_ptr<Model> DecisionSystem::GetSimulationModel()
{
	return SimulationModel;
}

std::string DecisionSystem::BeliefToString(boost::shared_ptr<State> generatorState)
{
	return generatorState->BeliefToString(CurrentState);
}

bool DecisionSystem::RestoreBeliefFromFile(const std::string& filePath)
{
	std::fstream beliefFile;
	beliefFile.open(filePath.c_str(), fstream::in);

	if (!beliefFile.is_open())
	{
		Output() << "Unable to open belief file: " << filePath << "\n";
		return false;
	}

	CurrentState.Clear();

	string headerLine;
	getline(beliefFile, headerLine);

	string line;
	while (!beliefFile.eof())
	{
		string line;
		getline(beliefFile, line);

		if (line.empty())
		{
			continue;
		}

		typedef boost::tokenizer< boost::escaped_list_separator<char> > Tokenizer;
		vector< string > parts;

		Tokenizer tok(line);
		parts.assign(tok.begin(),tok.end());

		if (parts.size()==0)	// should not be empty
		{
			Output() << "Invalid state description line: " << line << "\n";
			return false;
		}

		// create empty state
		boost::shared_ptr<State> state = SimulationModel->CreateNewState();
		/*boost::shared_ptr<ModelObservation> observation = SimulationModel->CreateNewObservation();
		boost::shared_ptr<ModelInput> input = SimulationModel->CreateNewInput();*/

		boost::shared_ptr<ModelObservation> observation = boost::shared_ptr<ModelObservation>();
		boost::shared_ptr<ModelInput> input = boost::shared_ptr<ModelInput>();

		// restore state
		if (!state->FromCSVTokens(parts, observation, input, false, state))
		{
			Output() << "Unable to parse state description line: " << line << "\n";
			return false;
		}

		CurrentState.AddParticle(state);
	}

	return true;
}

bool DecisionSystem::BeliefToFile(const std::string& filePath)
{
	ofstream beliefFile;
	beliefFile.open(filePath.c_str(), ios::out);
	if (!beliefFile.is_open())
	{
		return false;
	}

	for (unsigned int i=0; i<CurrentState.GetSize(); i++)
	{
		boost::shared_ptr<State> particle = CurrentState.GetParticle(i);

		// TMP: ignore header
		/*if (i==0)
		{
			beliefFile << particle->GetCSVHeader() << "\n";
		}*/

		beliefFile << particle->GetCSV() << "\n";
	}

	beliefFile.close();

	return true;
}

double DecisionSystem::FailureProbability()
{
	if (CurrentState.GetSize()==0)
	{
		// if there are no particles in the belief claim there is no failure
		return 0.0;
	}

	// use the first particle as generator state
	boost::shared_ptr<State> generatorState = CurrentState.GetParticle(0);

	return generatorState->FailureProbability(CurrentState);
}

void DecisionSystem::SetOutput(std::streambuf* outputBuf)
{
	Out = boost::shared_ptr<std::ostream>(new std::ostream(outputBuf));
}

std::ostream& DecisionSystem::Output()
{
	return *Out;
}

MONITOR_ACTION_TYPE DecisionSystem::RandomAction()
{
	static MONITOR_ACTION_TYPE action;

	int r = rand() % 2;
	switch (r)
	{
	case 0:	action = ACTION_CONTINUE;	break;
	case 1:	action = ACTION_ALARM;	break;
	}
	return action;
}

void DecisionSystem::ForwardUpdate(MONITOR_ACTION_TYPE action, boost::shared_ptr<ModelObservation> observation, boost::shared_ptr<ModelInput> input, BeliefState& tmpState)
{
	DecisionCommon::ParticleFilter(action, observation, CurrentState, GetSimulationModel(), input, tmpState);
}

void DecisionSystem::GetAction(vector<MONITOR_ACTION_TYPE>& actions, vector<double>& actionsDuration)
{
	for (vector<DecisionMakerPair>::iterator iDecisionMaker = DecisionMakers.begin(); iDecisionMaker!=DecisionMakers.end(); iDecisionMaker++)
	{
		boost::chrono::system_clock::time_point startGetAction = boost::chrono::system_clock::now();

		SimulationModel->SetRewardSystem(iDecisionMaker->rewards);

		MONITOR_ACTION_TYPE action = iDecisionMaker->decisionMaker->GetAction(&CurrentState);

		boost::chrono::system_clock::time_point stopGetAction = boost::chrono::system_clock::now();
		boost::chrono::duration<double> actionDuration = stopGetAction - startGetAction;

		actions.push_back(action);
		actionsDuration.push_back(actionDuration.count());
	}
}

void DecisionSystem::AddDecisionMaker(const std::string& name, const std::string& type, boost::shared_ptr<DecisionMaker> decisionMaker, const ModelRewardSystem& rewards)
{
	DecisionMakerPair pair;
	pair.decisitionMakerType = type;
	pair.decisionMaker = decisionMaker;
	pair.name = name;
	pair.rewards = rewards;

	pair.decisionMaker->SetDecisionSystem(this);

	DecisionMakers.push_back(pair);
}

int DecisionSystem::GetDecisionMakerCount()
{
	return DecisionMakers.size();
}

std::string DecisionSystem::GetDecisionMakerName(int iDecisionMaker)
{
	return DecisionMakers[iDecisionMaker].name;
}

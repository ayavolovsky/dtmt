/*
 * SolverOptions.cpp
 *
 *  Created on: May 14, 2013
 *      Author: ayavol2
 */

#include "SolverOptions.h"
#include <fstream>

using namespace std;

SolverOptions::SolverOptions() {
	Mode = CALCULATION;
	ReportFilePath = "";

	Trials = 1;
	TrialRepeats = 10;
	OutputVariantsPerTrial = 1;
	MaxTrialSteps = 100;
	MaxDelayAfterFalseAlarm = 10;
	MaxDelayBeforeMissedAlarm = 10;
	ErrorneousTimeRange = 10;
	ExperimentDirPath = "";
	BeliefStateDimension = 1;
	ExperimentThreads = 1;
	POMDPDiscountFactor = 0.95;
	POMCPExplorationConstant = 1.0;
	POMCPMaxTreeDepth = 100;
	POMCPSimulations = 1000;
	SaveTrialBeliefs = false;
	ThresholdValue = 0.9;
	ThresholdAccuracy = 1e-6;
	OperatingSystem = OS_WINDOWS;

	CalculationBeliefFilePath = "";

	ExperimentSet = EXPERIMENT_VARYING_PENALTIES;
}

SolverOptions::~SolverOptions() {
	// TODO Auto-generated destructor stub
}

unsigned long SolverOptions::GetTrials() const
{
	return Trials;
}

unsigned long SolverOptions::GetMaxTrialSteps() const
{
	return MaxTrialSteps;
}

unsigned long SolverOptions::GetTrialsRepeats() const
{
	return TrialRepeats;
}

unsigned long SolverOptions::GetOutputVariantsPerTrial() const
{
	return OutputVariantsPerTrial;
}

unsigned long SolverOptions::GetMaxDelayAfterFalseAlarm() const
{
	return MaxDelayAfterFalseAlarm;
}

unsigned long SolverOptions::GetMaxDelayBeforeMissedAlarm() const
{
	return MaxDelayBeforeMissedAlarm;
}

unsigned long SolverOptions::GetErrornousTimeRange() const
{
	return ErrorneousTimeRange;
}

string SolverOptions::GetExperimentDir() const
{
	return ExperimentDirPath;
}

bool SolverOptions::SetMode(const std::string& mode)
{
	if (mode.compare("calculation")==0)
	{
		Mode = CALCULATION;
	}
	else
	if (mode.compare("calculation_belief")==0)
	{
		Mode = CALCULATION_BELIEF;
	}
	else
	if (mode.compare("trials")==0)
	{
		Mode = TRIALS;
	}
	else
	if (mode.compare("experiment")==0)
	{
		Mode = EXPERIMENT;
	}
	else
	if (mode.compare("generate_belief_trials")==0)
	{
		Mode = GENERATE_BELIEF_TRIALS;
	}
	else
	if (mode.compare("report")==0)
	{
		Mode = REPORT;
	}
	else
	{
		return false;
	}

	return true;
}

bool SolverOptions::SetExperimentSet(const std::string& set)
{
	if (set.compare("varying_penalties")==0)
	{
		ExperimentSet = EXPERIMENT_VARYING_PENALTIES;
	}
	else
	if (set.compare("varying_consistent_alarm")==0)
	{
		ExperimentSet = EXPERIMENT_VARYING_CONSISTEMT_ALARM;
	}
	else
	if (set.compare("varying_failure_continue")==0)
	{
		ExperimentSet = EXPERIMENT_VARYING_FAILURE_CONTINUE;
	}
	else
	if (set.compare("varying_threshold")==0)
	{
		ExperimentSet = EXPERIMENT_VARYING_THRESHOLD;
	}
	else
	{
		return false;
	}

	return true;
}

bool SolverOptions::SetOperatingSystem(const std::string& os)
{
	if (os.compare("Windows")==0)
	{
		OperatingSystem = OS_WINDOWS;
	}
	else
	if (os.compare("Linux")==0)
	{
		OperatingSystem = OS_LINUX;
	}
	else
	{
		return false;
	}

	return true;
}

SolverOptions::OS SolverOptions::GetOperatingSystem() const
{
	return OperatingSystem;
}

ModelRewardSystem SolverOptions::GetRewardsSystem() const
{
	return Rewards;
}

unsigned long SolverOptions::GetBeliefStateDimension() const
{
	return BeliefStateDimension;
}

unsigned long SolverOptions::GetThreads() const
{
	return ExperimentThreads;
}

double SolverOptions::GetDiscountFactor() const
{
	return POMDPDiscountFactor;
}

double SolverOptions::GetExplorationConstant() const
{
	return POMCPExplorationConstant;
}

unsigned long SolverOptions::GetPOMCPMaxTreeDepth() const
{
	return POMCPMaxTreeDepth;
}

unsigned long SolverOptions::GetPOMCPSimulations() const
{
	return POMCPSimulations;
}

bool SolverOptions::GetSaveTrialBeliefs() const
{
	return SaveTrialBeliefs;
}

std::string SolverOptions::GetTrialsDirPath() const
{
	return TrialsDirPath;
}

std::string SolverOptions::GetCalculationBeliefFilePath() const
{
	return CalculationBeliefFilePath;
}

bool SolverOptions::SaveCSV(const std::string& filePath)
{
	fstream file;
	file.open(filePath.c_str(), fstream::out);
	if (!file.is_open())
	{
		return false;
	}

	file << "Mode, " << Mode2String() << "\n";
	file << "TrialsDirPath, " << TrialsDirPath << "\n";
	file << "Trials, " << Trials << "\n";
	file << "MaxTrialSteps, " << MaxTrialSteps << "\n";
	file << "MaxDelayAfterFalseAlarm, " << MaxDelayAfterFalseAlarm << "\n";
	file << "MaxDelayBeforeMissedAlarm, " << MaxDelayBeforeMissedAlarm << "\n";
	file << "ErrorneousTimeRange, " << ErrorneousTimeRange << "\n";
	file << "BeliefStateDimension, " << BeliefStateDimension << "\n";
	file << "ExperimentThreads, " << ExperimentThreads << "\n";
	file << "ExperimentModel, " << ExperimentModel << "\n";
	file << "DecisionMaker, " << DecisionMaker << "\n";
	file << "ExperimentDirPath, " << ExperimentDirPath << "\n";
	file << "SaveTrialBeliefs, " << SaveTrialBeliefs << "\n";
	file << "POMDPDiscountFactor, " << POMDPDiscountFactor << "\n";
	file << "POMCPExplorationConstant, " << POMCPExplorationConstant << "\n";
	file << "POMCPMaxTreeDepth, " << POMCPMaxTreeDepth << "\n";
	file << "POMCPSimulations, " << POMCPSimulations << "\n";
	file << "ThresholdValue, " << ThresholdValue << "\n";
	file << "ThresholdAccuracy, " << ThresholdAccuracy << "\n";
	file << "Rewards.RewardConsistentContinue, " << Rewards.GoodContinue << "\n";
	file << "Rewards.RewardConsistentAlarm, " << Rewards.GoodAlarm << "\n";
	file << "Rewards.RewardFailureContinue, " << Rewards.BadContinue << "\n";
	file << "Rewards.RewardFailureAlarm, " << Rewards.BadAlarm << "\n";

	file.close();
	return true;
}

bool SolverOptions::SaveINI(const std::string& filePath)
{
	fstream file;
	file.open(filePath.c_str(), fstream::out);
	if (!file.is_open())
	{
		return false;
	}

	file << "mode = " << Mode2String() << "\n";
	file << "trialsDir = " << TrialsDirPath << "\n";
	file << "threads = " << ExperimentThreads << "\n";
	file << "trials = " << Trials << "\n";
	file << "trialRepeats = " << TrialRepeats << "\n";
	file << "earlyAlarmDelay = " << MaxDelayAfterFalseAlarm << "\n";
	file << "missedAlarmDelay = " << MaxDelayBeforeMissedAlarm << "\n";
	file << "stepsPerTrial = " << MaxTrialSteps << "\n";
	file << "errorneousRange = " << ErrorneousTimeRange << "\n";
	file << "beliefDimension = " << BeliefStateDimension << "\n";

	file << "experimentModel = " << ExperimentModel << "\n";
	file << "decisionMaker = " << DecisionMaker << "\n";
	file << "experimentDirPath = " << ExperimentDirPath << "\n";
	file << "saveTrialBeliefs = " << SaveTrialBeliefs << "\n";
	file << "discount = " << POMDPDiscountFactor << "\n";
	file << "exploration = " << POMCPExplorationConstant << "\n";
	file << "maxTreeDepth = " << POMCPMaxTreeDepth << "\n";
	file << "pomcpSimulations = " << POMCPSimulations << "\n";
	file << "thresholdValue = " << ThresholdValue << "\n";
	file << "thresholdAccuracy = " << ThresholdAccuracy << "\n";
	file << "compare = " << DecisionMaker << "\n";

	file << "consistentContinue = " << Rewards.GoodContinue << "\n";
	file << "consistentAlarm = " << Rewards.GoodAlarm << "\n";
	file << "failureContinue = " << Rewards.BadContinue << "\n";
	file << "failureAlarm = " << Rewards.BadAlarm << "\n";

	file << "inputFunctionDataFile = " << InputFunctionDataFilePath << "\n";
	file << "operatingSystem = " << OperatingSystem2String(OperatingSystem) << "\n";

	file.close();
	return true;
}

std::string SolverOptions::GetReportFilePath() const
{
	return ReportFilePath;
}

double SolverOptions::GetThresholdValue() const
{
	return ThresholdValue;
}

double SolverOptions::GetThresholdAccuracy() const
{
	return ThresholdAccuracy;
}

std::string SolverOptions::OperatingSystem2String(SolverOptions::OS op_sys)
{
	switch (op_sys)
	{
	case OS_WINDOWS:
		return "Windows";
		break;
	case OS_LINUX:
		return "Linux";
		break;
	}

	return "undefined";
}

void SolverOptions::ConvertToThresholdMode(double thresholdValue)
{
	//DecisionMaker = DECISION_THRESHOLD_BASED;
	//ThresholdValue = thresholdValue;
	//ExperimentDirPath += "_threshold";
	//CompareDecisionMaker = DECISION_NONE;
}

void SolverOptions::ConvertToCompareDecision()
{
	//DecisionMaker = CompareDecisionMaker;
	//ExperimentDirPath += "_" + DecisionMaker2String(CompareDecisionMaker);
	//CompareDecisionMaker = DECISION_NONE;

	//AutoSetupPOMCPExploration();
}

void SolverOptions::AutoSetupPOMCPExploration()
{
	// C = R_hi - R_lo
	// R_hi - highest return achieved during sample runs with c = 0
	// R_lo - lowest return achieved during sample rollouts

	double maxReward = std::max(Rewards.GoodAlarm,
			std::max(Rewards.GoodContinue,
					std::max(Rewards.BadAlarm, Rewards.BadContinue)));

	double minReward = std::min(Rewards.GoodAlarm,
			std::min(Rewards.GoodContinue,
					std::min(Rewards.BadAlarm, Rewards.BadContinue)));

	POMCPExplorationConstant = maxReward - minReward;
}

SolverOptions::MODE SolverOptions::GetMode() const
{
	return Mode;
}

SolverOptions::EXPERIMENT_SET SolverOptions::GetExperimentSet() const
{
	return ExperimentSet;
}

std::string SolverOptions::GetInputFunctionDataFile() const
{
	return InputFunctionDataFilePath;
}

std::string SolverOptions::Mode2String()
{
	switch (Mode)
	{
	case CALCULATION:
		return "calculation";
		break;
	case CALCULATION_BELIEF:
		return "calculation_belief";
		break;
	case TRIALS:
		return "trials";
		break;
	case EXPERIMENT:
		return "experiment";
		break;
	case GENERATE_BELIEF_TRIALS:
		return "generate_belief_trials";
	case REPORT:
		return "report";
		break;
	default:
		return "undefined";
	}

	return "undefined";
}

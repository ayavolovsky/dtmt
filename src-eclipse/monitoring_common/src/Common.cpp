/*
 * Common.cpp
 *
 *  Created on: May 14, 2013
 *      Author: ayavol2
 */

#include "Common.h"

#include <boost/random.hpp>
#include <boost/random/linear_congruential.hpp>
#include <boost/random/uniform_int.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/random/variate_generator.hpp>
#include <boost/generator_iterator.hpp>
#include <boost/math/distributions/normal.hpp> // for normal_distribution
#include <boost/math/constants/constants.hpp>

using namespace std;
//using namespace UTILS;
using boost::math::normal; // typedef provides default type is double.

typedef boost::minstd_rand base_generator_type;

//base_generator_type generator(static_cast<unsigned int>(std::time(0)));
base_generator_type generator(static_cast<unsigned int>(50 /*std::time(0)*/));
double sqrt_2_pi = sqrt(2* boost::math::constants::pi<double>());

double NormalRand(double mu/*mean*/, double delta/*standard deviation*/)
{
	//static base_generator_type generator(static_cast<unsigned int>(std::time(0)));
	boost::normal_distribution<> nd(mu, delta);

	boost::variate_generator<base_generator_type&, boost::normal_distribution<> > var_nor(generator, nd);

	double res = (double) var_nor();

	return res;
}

double NormalProbability(double mu/*mean*/, double delta/*standard deviation*/, double x)
{
	double f = (1/(delta*sqrt_2_pi)) * (exp(-1 * pow(x - mu, 2) / (2*pow(delta,2))));
	return f;
}

double NormalProbability(double mu/*mean*/, double delta/*standard deviation*/, double variance, double x)
{
	double f = (1/(delta*sqrt_2_pi)) * (exp(-1 * pow(x - mu, 2) / (2*variance)));
	return f;
}

double UniformRand(double min, double max)
{
	//static base_generator_type generator(static_cast<unsigned int>(std::time(0)));

	boost::uniform_real<> uni_dist(min, max);
	boost::variate_generator<base_generator_type&, boost::uniform_real<> > uni(generator, uni_dist);

	double res = uni();

	return res;
}

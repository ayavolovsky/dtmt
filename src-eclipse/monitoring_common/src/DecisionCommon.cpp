/*
 * DecisionCommon.cpp
 *
 *  Created on: Jun 21, 2013
 *      Author: ayavol2
 */

#include "DecisionCommon.h"
#include "BeliefState.h"
#include "Model.h"
#include "Common.h"
#include <iostream>

#include "boost/shared_array.hpp"
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/discrete_distribution.hpp>

using namespace std;

DecisionCommon::DecisionCommon() {
	// TODO Auto-generated constructor stub

}

DecisionCommon::~DecisionCommon() {
	// TODO Auto-generated destructor stub
}

/*static*/ void DecisionCommon::PropagareBelief( MONITOR_ACTION_TYPE action, const BeliefState& currentState, boost::shared_ptr<Model> model, BeliefState& propagatedState, unsigned int nRepeats /*= 1*/, unsigned int nHorizon /*= 1*/)
{
	vector<boost::shared_ptr<ModelInput> > horizonInputs;

	boost::shared_ptr<ModelInput> actualInput;

	bool bUsePreallocatedParticles = true;
	if (propagatedState.GetSize()!=currentState.GetSize()*nRepeats)
	{
		bUsePreallocatedParticles = false;
		propagatedState.Clear();
	}

	for (unsigned int iRepeat = 0; iRepeat<nRepeats; iRepeat++)
	{
		for (unsigned int iParticle = 0; iParticle<currentState.GetSize(); iParticle++)
		{
			boost::shared_ptr<State> particle;
			if (!bUsePreallocatedParticles)
			{
				particle = model->CreateNewState();
			}
			else
			{
				particle = propagatedState.GetParticle(iRepeat*currentState.GetSize() + iParticle);
			}

			currentState.CopyParticle(iParticle, particle);

			for (unsigned int i=0; i<nHorizon; i++)
			{
				boost::shared_ptr<ModelInput> input;
				if (horizonInputs.size()>i)
				{
					input = horizonInputs.at(i);
				}
				else
				{
					horizonInputs.push_back(model->GetInputAt(particle));
					input = horizonInputs.at(i);
				}

				model->Step(particle, action, input, actualInput);
			}

			if (!bUsePreallocatedParticles)
			{
				propagatedState.AddParticle(particle);
			}
		}
	}
}

/*static*/ void DecisionCommon::ParticleFilter(MONITOR_ACTION_TYPE action,
		boost::shared_ptr<ModelObservation> observation,
		BeliefState& currentState, boost::shared_ptr<Model> model, boost::shared_ptr<ModelInput> input, BeliefState& tmpState, bool bUseTmpStateAsPropagated /*= false*/)
{
	double totalWeight = 0.0;

	boost::shared_array<double> particleWeights(new double[currentState.GetSize()]);
	boost::shared_ptr<ModelInput> actualInput;

	if (tmpState.GetSize()!=currentState.GetSize())
	{
		tmpState.Clear();
	}

	for (unsigned int iParticle = 0; iParticle<currentState.GetSize(); iParticle++)
	{
		boost::shared_ptr<State> particle;
		if (tmpState.GetSize()!=currentState.GetSize())
		{
			particle = model->CreateNewState();
		}
		else
		{
			particle = tmpState.GetParticle(iParticle);
		}

		if (!bUseTmpStateAsPropagated)
		{
			currentState.CopyParticle(iParticle, particle);
			//model->EvaluateAction(particle, action, immediateReward, evaluation);
			model->Step(particle, action, input, actualInput);
		}

		double weight = model->Weight(particle, observation);

		particleWeights[iParticle] = weight;
		totalWeight += weight;

		if (tmpState.GetSize()!=currentState.GetSize())
		{
			tmpState.AddParticle(particle);
		}
	}

	// normalize weights
	for (unsigned int iParticle = 0; iParticle<currentState.GetSize(); iParticle++) particleWeights[iParticle] /= totalWeight;

	// resampling step

	// Experimental:
	// First look for the particles that are very low probable considering the observation
	// We set some constant probability threshold, and look for weights that are smaller than a threshold
	// Then assume uniform distribution among them and sample 50% (less or more?) particles
	// Next, fill the rest of spaces in the belief vector with particles sampled according to the weights
	double lowThreshold = 0.01;
	double keepLowChanceParticlesRatio = 0.5;

	boost::shared_array<unsigned int> lowChanceParticles(new unsigned int[currentState.GetSize()]);
	unsigned int lowChanceParticlesCount = 0;

	for (unsigned int iParticle = 0; iParticle<currentState.GetSize(); iParticle++)
	{
		if (particleWeights[iParticle]<=lowThreshold)
		{
			lowChanceParticles[lowChanceParticlesCount++] = iParticle;
		}
	}

	unsigned int keepLowChanceParticlesCount = lowChanceParticlesCount*keepLowChanceParticlesRatio;
	//unsigned int keepLowChanceParticlesCount = 0;

	unsigned int iParticle = 0;
	for (; iParticle<keepLowChanceParticlesCount; iParticle++)
	{
		int selectedParticle = lowChanceParticles[ (unsigned int) (UniformRand(0, 1) * lowChanceParticlesCount) ];

		boost::shared_ptr<State> particle = currentState.GetParticle(iParticle);
		tmpState.CopyParticle(selectedParticle, particle);
	}

	boost::mt19937 gen;
	boost::random::discrete_distribution<> dist(particleWeights.get(), particleWeights.get() + currentState.GetSize());
	for (; iParticle<currentState.GetSize(); iParticle++)
	{
		int selectedParticle = dist(gen);

		boost::shared_ptr<State> particle = currentState.GetParticle(iParticle);
		tmpState.CopyParticle(selectedParticle, particle);
	}

	currentState.ResetHorizonRejectionProbability();
}

/*static*/ double DecisionCommon::GetHorizonRejectionProbability( BeliefState* currentBelief, boost::shared_ptr<Model> model, unsigned int horizon, unsigned int mcSimulations )
{
	double horizonRejectionProb;
	if (currentBelief->GetHorizonRejectionProbability(horizon, horizonRejectionProb))
	{
		return horizonRejectionProb;
	}

	if (horizon==1)
	{
		boost::shared_ptr<State> generatorState = currentBelief->GetParticle(0);
		horizonRejectionProb = generatorState->FailureProbability(*currentBelief);
		currentBelief->GetHorizonRejectionProbability(horizon, horizonRejectionProb);

		return horizonRejectionProb;
	}

	vector<boost::shared_ptr<ModelInput> > horizonInputs;
	boost::shared_ptr<ModelInput> actualInput;

	boost::shared_ptr<State> particle = model->CreateNewState();

	map<unsigned int, unsigned int> horizonGoodStateCounter;

	for (unsigned int i = 0; i<mcSimulations; i++)
	{
		// sample a state from the current belief distribution
		currentBelief->CopyRandomSample(particle);
		if (particle->IsFailureState())
		{
			continue;
		}

		unsigned int h = 1;
		if (!particle->IsFailureState())
		{
			if (horizonGoodStateCounter.find(h)==horizonGoodStateCounter.end())
			{
				horizonGoodStateCounter[h] = 1;
			}
			else
			{
				horizonGoodStateCounter[h]++;
			}
		}

		// propagate it forward until desired horizon is not reached
		// when calculating for the deeper horizon calculate for lower horizons on the way
		for (h=2; h<=horizon; h++)
		{
			// query for the input or use buffered
			boost::shared_ptr<ModelInput> input;
			if (horizonInputs.size()>h-2)
			{
				input = horizonInputs.at(h-2);
			}
			else
			{
				horizonInputs.push_back(model->GetInputAt(particle));
				input = horizonInputs.at(h-2);
			}

			// propagate the state according to the model and the input function
			model->Step(particle, ACTION_CONTINUE, input, actualInput);

			// check the horizon depth and make notes whether reached state is good
			if (!particle->IsFailureState())
			{
				if (horizonGoodStateCounter.find(h)==horizonGoodStateCounter.end())
				{
					horizonGoodStateCounter[h] = 1;
				}
				else
				{
					horizonGoodStateCounter[h]++;
				}
			}
		}
	}

	for (unsigned int h=1; h<=horizon; h++)
	{
		double accProb = 1.0*horizonGoodStateCounter[h]/mcSimulations;
		currentBelief->SetHorizonRejectionProbability(h, 1-accProb);
	}

	currentBelief->GetHorizonRejectionProbability(horizon, horizonRejectionProb);

	return horizonRejectionProb;
}

/*
 * ModelRewardSystem.h
 *
 *  Created on: May 14, 2013
 *      Author: ayavol2
 */

#ifndef MODELREWARDSYSTEM_H_
#define MODELREWARDSYSTEM_H_

class ModelRewardSystem {
public:
	ModelRewardSystem();
	virtual ~ModelRewardSystem();

	double GoodAlarm;
	double GoodContinue;
	double BadAlarm;
	double BadContinue;
};

#endif /* MODELREWARDSYSTEM_H_ */

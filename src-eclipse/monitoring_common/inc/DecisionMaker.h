/*
 * DecisionMaker.h
 *
 *  Created on: May 14, 2013
 *      Author: ayavol2
 */

#ifndef DECISIONMAKER_H_
#define DECISIONMAKER_H_

#include "MonitoringDefs.h"

class BeliefState;
class DecisionSystem;

class DecisionMaker {
public:
	DecisionMaker();
	virtual ~DecisionMaker();

	// loads options for the decision maker, i.e. makes it ready
	virtual bool LoadOptions(const std::string& optionsFilePath) = 0;

	// find the action to be executed according to the current knowledge
	virtual MONITOR_ACTION_TYPE GetAction(BeliefState* currentBelief) = 0;

	void SetDecisionSystem(DecisionSystem* system);
	DecisionSystem* GetDecisionSystem();;

	void SetInputFunctionKnown(bool bknown);
	bool IsInputFunctionKnown();
private:
	DecisionSystem* parentDecisionSystem;

	bool bKnownInputFunction;
};

#endif /* DECISIONMAKER_H_ */

/*
 * MonitoringHandle.h
 *
 *  Created on: Apr 17, 2014
 *      Author: andrey
 */

#ifndef MONITORINGHANDLE_H_
#define MONITORINGHANDLE_H_

#include <boost/shared_ptr.hpp>
#include "Model.h"
#include "DecisionMaker.h"
#include "ModelObservation.h"

class MonitoringHandle {
public:
	MonitoringHandle();
	virtual ~MonitoringHandle();

	boost::shared_ptr<Model> SimulationModel;
	boost::shared_ptr<Model> ControlModel;
	boost::shared_ptr<State> ControlState;
	boost::shared_ptr<DecisionSystem> DecisionSystemObj;
	boost::shared_ptr<ModelObservation> MostRecentObservation;

	bool bHistoryLoggingEnabled;
};

#endif /* MONITORINGHANDLE_H_ */

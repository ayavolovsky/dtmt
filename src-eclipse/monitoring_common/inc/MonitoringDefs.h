/*
 * MonitoringDefs.h
 *
 *  Created on: May 14, 2013
 *      Author: ayavol2
 */

#ifndef MONITORINGDEFS_H_
#define MONITORINGDEFS_H_

#include <string>

enum MONITOR_ACTION_TYPE {	ACTION_CONTINUE,
							ACTION_ALARM};

enum MONITOR_ACTION_EVALUATION {ACTION_EVAL_CORRECT_CONTINUE,
								ACTION_EVAL_CORRECT_ALARM,
								ACTION_EVAL_EARLY_ALARM,
								ACTION_EVAL_MISSED_ALARM,
								ACTION_EVAL_FALSE_ALARM};

#endif /* MONITORINGDEFS_H_ */

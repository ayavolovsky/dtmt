/*
 * ModelInput.h
 *
 *  Created on: May 19, 2014
 *      Author: Andrey
 */

#ifndef MODELINPUTFUNCTION_H_
#define MODELINPUTFUNCTION_H_

#include <boost/shared_ptr.hpp>

class ModelInput;

class ModelInputFunction {
public:
	ModelInputFunction();
	virtual ~ModelInputFunction();

	virtual boost::shared_ptr<ModelInput> getInput(double time) = 0;
};

#endif /* MODELINPUT_H_ */

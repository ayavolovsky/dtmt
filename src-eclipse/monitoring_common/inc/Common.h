/*
 * Common.h
 *
 *  Created on: May 14, 2013
 *      Author: ayavol2
 */

#ifndef COMMON_H_
#define COMMON_H_

double NormalRand(double mu/*mean*/, double delta/*standard deviation*/);

// calculates probability of x given the distribution
double NormalProbability(double mu/*mean*/, double delta/*standard deviation*/, double x);
double NormalProbability(double mu/*mean*/, double delta/*standard deviation*/, double variance, double x);

double UniformRand(double min, double max);

#endif /* COMMON_H_ */

/*
 * SolverOptions.h
 *
 *  Created on: May 14, 2013
 *      Author: ayavol2
 */

#ifndef SOLVEROPTIONS_H_
#define SOLVEROPTIONS_H_

#include <string>
#include "ModelRewardSystem.h"

class SolverOptions {
public:
	enum MODE {
		CALCULATION,
		CALCULATION_BELIEF,
		TRIALS,
		EXPERIMENT,
		GENERATE_BELIEF_TRIALS,
		REPORT
	};

	enum EXPERIMENT_SET {
		EXPERIMENT_VARYING_PENALTIES,
		EXPERIMENT_VARYING_CONSISTEMT_ALARM,
		EXPERIMENT_VARYING_FAILURE_CONTINUE,
		EXPERIMENT_VARYING_THRESHOLD
	};

	enum OS {
		OS_WINDOWS,
		OS_LINUX
	};
public:
	friend class CommandLineHandler;
	friend class ReportGenerator;
	friend class ExperimentGenerator;

	SolverOptions();
	virtual ~SolverOptions();

	bool SaveCSV(const std::string& filePath);
	bool SaveINI(const std::string& filePath);

	bool SetMode(const std::string& mode);
	bool SetExperimentSet(const std::string& set);
	bool SetOperatingSystem(const std::string& os);

	std::string GetTrialsDirPath() const;

	std::string GetCalculationBeliefFilePath() const;

	void ConvertToThresholdMode(double thresholdValue);
	void ConvertToCompareDecision();

	unsigned long GetThreads() const;
	unsigned long GetTrials() const;
	unsigned long GetMaxTrialSteps() const;
	unsigned long GetTrialsRepeats() const;
	unsigned long GetOutputVariantsPerTrial() const;
	unsigned long GetMaxDelayAfterFalseAlarm() const;
	unsigned long GetMaxDelayBeforeMissedAlarm()const;
	unsigned long GetErrornousTimeRange() const;
	unsigned long GetBeliefStateDimension() const;
	std::string GetExperimentDir() const;
	ModelRewardSystem GetRewardsSystem() const;
	bool GetSaveTrialBeliefs() const;
	OS GetOperatingSystem() const;

	double GetDiscountFactor() const;
	double GetExplorationConstant() const;
	unsigned long GetPOMCPMaxTreeDepth() const;
	unsigned long GetPOMCPSimulations() const;

	double GetThresholdValue() const;
	double GetThresholdAccuracy() const;

	std::string GetReportFilePath() const;

	MODE GetMode() const;

	EXPERIMENT_SET GetExperimentSet() const;

	std::string GetInputFunctionDataFile() const;

	void AutoSetupPOMCPExploration();
private:
	std::string Mode2String();
	std::string OperatingSystem2String(SolverOptions::OS op_sys);
public:
	MODE Mode;

	// TRIALS mode - directory where trials will be generated
	// CALCULATION mode - directory where trials will be searched for
	std::string TrialsDirPath;

	// REPORT mode
	std::string ReportFilePath;

	// TRIALS mode		- settings for trials generation
	// CALCULATION mode - if trials not defined from the directory
	unsigned long Trials;
	unsigned long MaxTrialSteps;
	unsigned long TrialRepeats;
	unsigned long OutputVariantsPerTrial;
	unsigned long MaxDelayAfterFalseAlarm;
	unsigned long MaxDelayBeforeMissedAlarm;
	unsigned long ErrorneousTimeRange;

	// CALCULATION mode - directory to store experiment calculation data
	// EXPERIMENT mode - directory to generate experiments
	std::string ExperimentDirPath;

	// CALCULATION_BELIEF mode
	std::string CalculationBeliefFilePath;

	// CALCULATION mode
	unsigned long BeliefStateDimension;
	unsigned long ExperimentThreads;
	std::string ExperimentModel;
	std::string DecisionMaker;
	std::string CompareDecisionMaker;
	bool SaveTrialBeliefs;
	ModelRewardSystem Rewards;

	double POMDPDiscountFactor;
	double POMCPExplorationConstant;
	unsigned long POMCPMaxTreeDepth;
	unsigned long POMCPSimulations;

	double ThresholdValue;
	double ThresholdAccuracy;

	std::string InputFunctionDataFilePath;

	// EXPERIMENT mode
	EXPERIMENT_SET ExperimentSet;

	// Experiment generation
	OS OperatingSystem;

	// Monitoring
	std::string MonitorHistoryFilePath;
};

#endif /* SOLVEROPTIONS_H_ */

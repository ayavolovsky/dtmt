/*
 * Model.h
 *
 *  Created on: May 14, 2013
 *      Author: ayavol2
 */

#ifndef MODEL_H_
#define MODEL_H_

#include "MonitoringDefs.h"
#include "ModelRewardSystem.h"
#include "ModelSimulationOptions.h"
#include "BeliefState.h"
#include <ModelObservation.h>
#include <ModelInputFunction.h>
#include <boost/shared_ptr.hpp>
#include <fstream>

class State {
public:
	State();
	virtual ~State();

	virtual void Continue(bool bFailureEnabled, boost::shared_ptr<ModelInput> input, boost::shared_ptr<ModelInput>& actualInput) = 0;

	virtual boost::shared_ptr<ModelInput> GetInput(boost::shared_ptr<ModelInputFunction> InputFunction) = 0;

	virtual void ResetStart() = 0;

	virtual bool IsFailureState() = 0;

	virtual boost::shared_ptr<ModelObservation> GetObservation() = 0;

	virtual double WeightObservation(boost::shared_ptr<ModelObservation> observation) = 0;

	virtual std::vector< boost::shared_ptr<ModelObservation> >& GetPossibleObservations() = 0;

	virtual unsigned int GetMaxObservations() = 0;

	virtual double GetTime() = 0;

	virtual std::string GetCSV() = 0;

	virtual bool FromCSVTokens(std::vector<std::string>& tokens, boost::shared_ptr<ModelObservation> observation, boost::shared_ptr<ModelInput> input, bool bUpdateProperty, const boost::shared_ptr<State>& currentRealState) = 0;

	virtual std::string GetCSVHeader() = 0;

	virtual std::string ToString() = 0;

	virtual std::string ObservationToString(boost::shared_ptr<ModelObservation> observation);

	virtual std::string BeliefToString(const BeliefState& belief) = 0;

	virtual double FailureProbability(const BeliefState& belief) = 0;

	virtual void Copy(boost::shared_ptr<State> other) = 0;

	virtual unsigned long GetObservationID(boost::shared_ptr<ModelObservation> observation) = 0;

	virtual unsigned long GetInputID(boost::shared_ptr<ModelInput> input) = 0;

	virtual void FullyObserve(boost::shared_ptr<ModelObservation>& observation) = 0;

	virtual void EnableImmediateFailure(bool bEnable);

	virtual void EnableProbabilisticFailure(bool bEnable);

	virtual void GetPossibleInputs(std::vector<boost::shared_ptr<ModelInput> >& inputs) = 0;

	virtual void AddObservationDisturbance(boost::shared_ptr<ModelObservation> observation);

protected:
	bool isImmediateFailureEnabled();

	bool isProbabilisticFailureEnabled();
private:
	bool bImmediateFailureEnabled;

	bool bProbabilisticFailureEnabled;
};

class Model {
public:
	Model();
	virtual ~Model();

	void SetRewardSystem(const ModelRewardSystem& r);

	void SetSimulationOptions(const ModelSimulationOptions& o);

	void SetInputFunction(boost::shared_ptr<ModelInputFunction> inputFunction);

	boost::shared_ptr<ModelInputFunction> GetInputFunction();

	boost::shared_ptr<ModelInput> GetInputAt(boost::shared_ptr<State> state);

	void InitializeHistoryFile(const std::string& historyDirPath, unsigned int GroupID, unsigned int ID, bool bNoGroup = false);

	void AddHistoryState(boost::shared_ptr<State> state, boost::shared_ptr<ModelObservation> observation, boost::shared_ptr<ModelInput> input, bool bTerminal);

	void FinalizeHistoryFile();

	virtual boost::shared_ptr<State> CreateStartState();

	virtual void GetStateObservation(boost::shared_ptr<State> state, boost::shared_ptr<ModelObservation>& observation);

	// returns true if the new state is not terminal, and false otherwise
	virtual bool Step(boost::shared_ptr<State> state, MONITOR_ACTION_TYPE action, boost::shared_ptr<ModelInput> input, boost::shared_ptr<ModelInput>& actualInput);

	//
	virtual void EvaluateAction(boost::shared_ptr<State> state, MONITOR_ACTION_TYPE action, MONITOR_ACTION_EVALUATION& evaluation);

	virtual double RewardAction(boost::shared_ptr<State> state, MONITOR_ACTION_TYPE action, ModelRewardSystem& rewards);

	//
	virtual void GetInitialBelief(BeliefState& belief, boost::shared_ptr<State> realStartState, unsigned int particles);

	//
	virtual double Weight(boost::shared_ptr<State> state, boost::shared_ptr<ModelObservation> observation);

	virtual void GetObservationDitributionAsParticles(BeliefState& belief, std::vector< boost::shared_ptr<ModelObservation> >& observations);

	virtual boost::shared_ptr<State> CreateNewState() = 0;

	virtual boost::shared_ptr<ModelObservation> CreateNewObservation() = 0;

	virtual boost::shared_ptr<ModelInput> CreateNewInput() = 0;

	bool NextStepFromFile(std::fstream& file, boost::shared_ptr<State>& state, boost::shared_ptr<ModelObservation>& observation, boost::shared_ptr<ModelInput>& input, bool& bTerminal, bool bUpdateProperty, const boost::shared_ptr<State>& currentRealState);

	unsigned long GetObservationID(boost::shared_ptr<ModelObservation> observation);

protected:

	ModelRewardSystem Rewards;
	ModelSimulationOptions Options;
	std::ofstream HistoryFile;
	bool bEmptyFile;

	boost::shared_ptr<ModelInputFunction> InputFunction;

	boost::shared_ptr<State> tmpState;
};

#endif /* MODEL_H_ */

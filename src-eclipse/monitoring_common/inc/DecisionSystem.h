/*
 * DecisionSystem.h
 *
 *  Created on: Feb 23, 2015
 *      Author: ayavol2
 */

#ifndef DECISIONSYSTEM_H_
#define DECISIONSYSTEM_H_

#include "Model.h"

#include <boost/shared_ptr.hpp>
#include "BeliefState.h"
#include <ModelObservation.h>
#include <ModelInputFunction.h>
#include <vector>
#include <SolverOptions.h>

class DecisionMaker;

class DecisionSystem {
public:
	struct DecisionMakerPair
	{
		std::string decisitionMakerType;
		boost::shared_ptr<DecisionMaker> decisionMaker;
		std::string name;
		ModelRewardSystem rewards;
	};
public:
	DecisionSystem(boost::shared_ptr<Model> simModel, std::streambuf* outBuf);
	virtual ~DecisionSystem();

	// append decision makers
	void AddDecisionMaker(const std::string& name, const std::string& type, boost::shared_ptr<DecisionMaker> decisionMaker, const ModelRewardSystem& rewards);

	int GetDecisionMakerCount();

	std::string GetDecisionMakerName(int iDecisionMaker);

	bool EarlyAlarmSupport(int iIndex);

	// find the action to be executed according to the current knowledge
	void GetAction(std::vector<MONITOR_ACTION_TYPE>& actions, std::vector<double>& actionsDuration);

	// perform the forward update according to the new observation and action just executed
	virtual void ForwardUpdate(MONITOR_ACTION_TYPE action, boost::shared_ptr<ModelObservation> observation, boost::shared_ptr<ModelInput> input, BeliefState& tmpState);

	// sets the initial belief
	virtual void SetInitialBelief(BeliefState& belief);

	// gets the simulation model
	boost::shared_ptr<Model> GetSimulationModel();

	// converts belief state into string representation
	std::string BeliefToString(boost::shared_ptr<State> generatorState);

	// saves belief state into the file
	bool BeliefToFile(const std::string& filePath);

	// restores belief state from file
	bool RestoreBeliefFromFile(const std::string& filePath);

	// calculate the probability of being in the failure state
	double FailureProbability();

	void SetOutput(std::streambuf* outputBuf);

	MONITOR_ACTION_TYPE RandomAction();

	std::ostream& Output();
protected:
	BeliefState CurrentState;

	boost::shared_ptr<std::ostream> Out;

	boost::shared_ptr<Model> SimulationModel;

	std::vector<DecisionMakerPair> DecisionMakers;
};

#endif /* DECISIONSYSTEM_H_ */

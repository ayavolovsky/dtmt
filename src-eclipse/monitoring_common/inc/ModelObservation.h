/*
 * ModelObservation.h
 *
 *  Created on: Apr 16, 2014
 *      Author: andrey
 */

#ifndef MODELOBSERVATION_H_
#define MODELOBSERVATION_H_

#include <boost/shared_ptr.hpp>

class ModelObservation {
public:
	ModelObservation();
	virtual ~ModelObservation();

	virtual ModelObservation* Clone() = 0;
	virtual void Copy(boost::shared_ptr<ModelObservation> other) = 0;

	virtual std::string GetCSVHeader() = 0;
	virtual std::string GetCSV() = 0;
};

#endif /* MODELOBSERVATION_H_ */

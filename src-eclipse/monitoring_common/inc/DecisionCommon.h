/*
 * DecisionCommon.h
 *
 *  Created on: Jun 21, 2013
 *      Author: ayavol2
 */

#ifndef DECISIONCOMMON_H_
#define DECISIONCOMMON_H_

#include <MonitoringDefs.h>
#include <ModelObservation.h>
#include <ModelInputFunction.h>
#include "boost/shared_ptr.hpp"

class BeliefState;
class Model;

class DecisionCommon {
public:
	DecisionCommon();
	virtual ~DecisionCommon();

	static void ParticleFilter(	MONITOR_ACTION_TYPE action, boost::shared_ptr<ModelObservation> observation,
								BeliefState& currentState, boost::shared_ptr<Model> simulationModel, boost::shared_ptr<ModelInput> input, BeliefState& tmpState, bool bUseTmpStateAsPropagated = false);
	static void PropagareBelief( MONITOR_ACTION_TYPE action, const BeliefState& currentState, boost::shared_ptr<Model> simulationModel, BeliefState& propagatedState, unsigned int nRepeats = 1, unsigned int nHorizon = 1);

	static double GetHorizonRejectionProbability( BeliefState* currentBelief, boost::shared_ptr<Model> model, unsigned int horizon, unsigned int mcSimulations );

};

#endif /* DECISIONCOMMON_H_ */

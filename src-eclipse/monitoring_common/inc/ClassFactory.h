/*
 * ClassFactory.h
 *
 *  Created on: May 24, 2013
 *      Author: ayavol2
 */

#ifndef CLASSFACTORY_H_
#define CLASSFACTORY_H_

#include <boost/pool/object_pool.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/bind.hpp>

template <class ClassType>
class ClassFactory {
	boost::object_pool<ClassType> allocator;
public:
	ClassFactory() :
		allocator(1000)
	{
		allocator.set_next_size(1000);
	}

	boost::shared_ptr<ClassType> Create()
	{
		return boost::shared_ptr<ClassType>(
				allocator.construct(),
				boost::bind(&ClassFactory::Destroy, this, _1));
	}

	void Destroy(ClassType* p)
	{
		allocator.destroy(p);
	}
};

#endif /* CLASSFACTORY_H_ */

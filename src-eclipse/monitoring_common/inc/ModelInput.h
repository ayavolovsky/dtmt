/*
 * ModelInput.h
 *
 *  Created on: Oct 10, 2014
 *      Author: ayavol2
 */

#ifndef MODELINPUT_H_
#define MODELINPUT_H_

#include <boost/shared_ptr.hpp>

class ModelInput {
public:
	ModelInput();
	virtual ~ModelInput();

	virtual boost::shared_ptr<ModelInput> Clone() = 0;

	virtual std::string ToString() = 0;

	virtual std::string GetCSVHeader() = 0;
	virtual std::string GetCSV() = 0;
};

#endif /* MODELINPUT_H_ */

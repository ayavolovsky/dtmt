/*
 * ModelSimulationOptions.h
 *
 *  Created on: May 15, 2013
 *      Author: ayavol2
 */

#ifndef MODELSIMULATIONOPTIONS_H_
#define MODELSIMULATIONOPTIONS_H_

class ModelSimulationOptions {
public:
	ModelSimulationOptions();
	virtual ~ModelSimulationOptions();

	void SetErrorneousTimeRange(unsigned long maxFailTime);

	unsigned int GetPossibleFailureTimeRange();

private:
	unsigned int PossibleFailureTimeRange;
};

#endif /* MODELSIMULATIONOPTIONS_H_ */

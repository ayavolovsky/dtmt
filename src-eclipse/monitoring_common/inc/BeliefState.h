/*
 * BeliefState.h
 *
 *  Created on: May 16, 2013
 *      Author: ayavol2
 */

#ifndef BELIEFSTATE_H_
#define BELIEFSTATE_H_

#include <boost/shared_ptr.hpp>
#include <vector>
#include <map>

class State;

class BeliefState {
public:
	BeliefState();
	virtual ~BeliefState();

	void AddParticle(boost::shared_ptr<State> particle);

	void Clear();

	boost::shared_ptr<State> GetParticle(unsigned int nParticle) const;

	bool CopyRandomSample(boost::shared_ptr<State> copy);

	bool CopyParticle(unsigned int nParticle, boost::shared_ptr<State> copy) const;

	unsigned int GetSize() const;

	bool GetHorizonRejectionProbability(unsigned int horizon, double& rejectionProbability);

	void SetHorizonRejectionProbability(unsigned int horizon, double rejectionProbability);

	void ResetHorizonRejectionProbability();

private:
	std::vector<boost::shared_ptr<State> > Particles;

	std::map< unsigned int, double > HorizonRejectionProbability;
};

#endif /* BELIEFSTATE_H_ */

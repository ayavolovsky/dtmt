################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/BeliefState.cpp \
../src/ClassFactory.cpp \
../src/Common.cpp \
../src/DecisionCommon.cpp \
../src/DecisionMaker.cpp \
../src/DecisionSystem.cpp \
../src/Model.cpp \
../src/ModelInput.cpp \
../src/ModelObservation.cpp \
../src/ModelSimulationOptions.cpp \
../src/MonitoringHandle.cpp 

OBJS += \
./src/BeliefState.o \
./src/ClassFactory.o \
./src/Common.o \
./src/DecisionCommon.o \
./src/DecisionMaker.o \
./src/DecisionSystem.o \
./src/Model.o \
./src/ModelInput.o \
./src/ModelObservation.o \
./src/ModelSimulationOptions.o \
./src/MonitoringHandle.o 

CPP_DEPS += \
./src/BeliefState.d \
./src/ClassFactory.d \
./src/Common.d \
./src/DecisionCommon.d \
./src/DecisionMaker.d \
./src/DecisionSystem.d \
./src/Model.d \
./src/ModelInput.d \
./src/ModelObservation.d \
./src/ModelSimulationOptions.d \
./src/MonitoringHandle.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I"/home/andrey/Documents/src-eclipse/monitoring_common/inc" -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '



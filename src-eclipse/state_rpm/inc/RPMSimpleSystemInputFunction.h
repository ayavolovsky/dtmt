/*
 * RPMSimpleSystemInputFunction.h
 *
 *  Created on: Oct 11, 2014
 *      Author: ayavol2
 */

#ifndef RPMSIMPLESYSTEMINPUTFUNCTION_H_
#define RPMSIMPLESYSTEMINPUTFUNCTION_H_

#include <ModelInputFunction.h>
#include <string>
#include <vector>

class RPMSimpleSystemInputFunction : public ModelInputFunction {
public:
	RPMSimpleSystemInputFunction(const std::string& inputFile);
	virtual ~RPMSimpleSystemInputFunction();

	virtual boost::shared_ptr<ModelInput> getInput(double time);
private:
	bool ParseInputFile();

	std::string InputFilePath;
	std::vector<double> TimeSequence;
	std::vector<double> GasPedalLevelSequence;
};

#endif /* RPMSIMPLESYSTEMINPUTFUNCTION_H_ */

/*
 * RPMSimpleSystemState.h
 *
 *  Created on: May 15, 2014
 *      Author: Andrey
 */

#ifndef RPMSIMPLESYSTEMSTATE_H_
#define RPMSIMPLESYSTEMSTATE_H_

#include <Model.h>
#include <string>

class RPMSimpleSystemState: public State {
public:
	struct System
	{
		unsigned int d;		// mode id
		double c;			// counter timer for transitional states
		double rpm;			// value of RPM

		double wheelrpm;	// value of the wheel rpm (helper variable)
		int gear;			// gear # (helper variable)

		double t;		// time
	};

	struct Property
	{
		bool operator==(const Property& other) const
		{
			if (q==other.q && counter==other.counter)
			{
				return true;
			}
			return false;
		}

		unsigned int q;			// state id
		unsigned int counter;	// counter
	};
public:
	RPMSimpleSystemState();
	virtual ~RPMSimpleSystemState();

	virtual void Continue(bool bFailureEnabled, boost::shared_ptr<ModelInput> input, boost::shared_ptr<ModelInput>& actualInput);

	virtual boost::shared_ptr<ModelInput> GetInput(boost::shared_ptr<ModelInputFunction> InputFunction);

	virtual void ResetStart();

	virtual bool IsFailureState();

	virtual boost::shared_ptr<ModelObservation> GetObservation();

	virtual double WeightObservation(boost::shared_ptr<ModelObservation> observation);

	virtual std::vector< boost::shared_ptr<ModelObservation> >& GetPossibleObservations();

	virtual unsigned int GetMaxObservations();

	virtual double GetTime();

	virtual std::string GetCSV();

	virtual std::string GetCSVHeader();

	virtual bool FromCSVTokens(std::vector<std::string>& tokens, boost::shared_ptr<ModelObservation> observation, boost::shared_ptr<ModelInput> input, bool bUpdateProperty, const boost::shared_ptr<State>& currentRealState);

	virtual std::string ToString();

	virtual std::string ObservationToString(boost::shared_ptr<ModelObservation> observation);

	virtual std::string BeliefToString(const BeliefState& belief);

	virtual double FailureProbability(const BeliefState& belief);

	virtual void Copy(boost::shared_ptr<State> other);

	virtual unsigned long GetObservationID(boost::shared_ptr<ModelObservation> observation);

	virtual unsigned long GetInputID(boost::shared_ptr<ModelInput> input);

	virtual void FullyObserve(boost::shared_ptr<ModelObservation>& observation);

	virtual void GetPossibleInputs(std::vector<boost::shared_ptr<ModelInput> >& inputs);

	virtual void AddObservationDisturbance(boost::shared_ptr<ModelObservation> observation);
private:
	void Continue_System(bool bFailureEnabled, boost::shared_ptr<ModelInput> abstractInput, boost::shared_ptr<ModelInput>& actualInput);
	void Continue_System_Transition(bool bFailureEnabled, boost::shared_ptr<ModelInput> abstractInput);
	void Continue_Property();
	boost::shared_ptr<ModelInput> GenerateSampleInput();
private:
	System sys;
	Property prop;

	double GetVelocity();

	void ExtractObservation(unsigned int observationID, double& rpm, int& gear);
	unsigned int CompressObservation(double rpm, int gear);

	static double N1_standard_deviation;
	static double N1_fail_standard_deviation;
	static double N2_standard_deviation;
	static double N2_fail_standard_deviation;
	static double N3_standard_deviation;
	static double N4_standard_deviation;
	static double N5_standard_deviation;
	static std::vector<boost::shared_ptr<ModelInput> > possibleInputs;
	static std::vector< boost::shared_ptr<ModelObservation> > possibleObservations;
};

#endif /* RPMSIMPLESYSTEMSTATE_H_ */

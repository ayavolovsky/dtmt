/*
 * RPMObservation.h
 *
 *  Created on: Oct 13, 2014
 *      Author: ayavol2
 */

#ifndef RPMOBSERVATION_H_
#define RPMOBSERVATION_H_

#include <ModelObservation.h>

class RPMObservation : public ModelObservation {
public:
	RPMObservation();
	virtual ~RPMObservation();

	virtual ModelObservation* Clone();
	virtual void Copy(boost::shared_ptr<ModelObservation> other);

	virtual std::string GetCSVHeader();
	virtual std::string GetCSV();

	double wheelRPM;

	int gear;
	double engineRPM;

	int system_d;
	int property_q;
};

#endif /* RPMOBSERVATION_H_ */

/*
 * RPMSimpleSystemInput.h
 *
 *  Created on: Oct 12, 2014
 *      Author: ayavol2
 */

#ifndef RPMSIMPLESYSTEMINPUT_H_
#define RPMSIMPLESYSTEMINPUT_H_

#include "ModelInput.h"

class RPMSimpleSystemInput : public ModelInput{
public:
	RPMSimpleSystemInput();
	RPMSimpleSystemInput(double t, double v);
	virtual ~RPMSimpleSystemInput();

	virtual boost::shared_ptr<ModelInput> Clone();
	virtual std::string ToString();

	virtual std::string GetCSVHeader();
	virtual std::string GetCSV();

	double timeValue;
	double gasPedalLevel;
};

#endif /* RPMSIMPLESYSTEMINPUT_H_ */

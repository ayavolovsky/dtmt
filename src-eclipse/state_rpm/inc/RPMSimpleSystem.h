/*
 * RPMSimpleSystem.h
 *
 *  Created on: May 15, 2014
 *      Author: Andrey
 */

#ifndef RPMSIMPLESYSTEM_H_
#define RPMSIMPLESYSTEM_H_

#include <MonitoringDefs.h>
#include <Model.h>
#include <ClassFactory.h>
#include "RPMSimpleSystemState.h"
#include "RPMSimpleSystemInputFunction.h"
#include "RPMObservation.h"
#include "RPMSimpleSystemInput.h"

class RPMSimpleSystem  : public Model {
public:
	RPMSimpleSystem();
	virtual ~RPMSimpleSystem();

	virtual boost::shared_ptr<State> CreateNewState();

	virtual boost::shared_ptr<ModelObservation> CreateNewObservation();

	virtual boost::shared_ptr<ModelInput> CreateNewInput();
private:
	ClassFactory<RPMSimpleSystemState> StateFactory;

	ClassFactory<RPMObservation> ObservationFactory;

	ClassFactory<RPMSimpleSystemInput> InputFactory;
};

#endif /* RPMSIMPLESYSTEM_H_ */

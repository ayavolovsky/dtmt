/*
 * RPMSimpleSystemInput.cpp
 *
 *  Created on: Oct 12, 2014
 *      Author: ayavol2
 */

#include <RPMSimpleSystemInput.h>
#include <boost/format.hpp>

RPMSimpleSystemInput::RPMSimpleSystemInput()
{
	timeValue = 0;
	gasPedalLevel = 0.0;
}

RPMSimpleSystemInput::RPMSimpleSystemInput(double t, double v) {
	timeValue = t;
	gasPedalLevel = v;
}

RPMSimpleSystemInput::~RPMSimpleSystemInput() {
	// TODO Auto-generated destructor stub
}

boost::shared_ptr<ModelInput> RPMSimpleSystemInput::Clone()
{
	boost::shared_ptr<RPMSimpleSystemInput> input = boost::shared_ptr<RPMSimpleSystemInput>(new RPMSimpleSystemInput(timeValue, gasPedalLevel));

	return input;
}

/*virtual*/ std::string RPMSimpleSystemInput::ToString()
{
	return boost::str (boost::format("Time = %1%; GasPedal = %2%") % timeValue % gasPedalLevel);
}

/*virtual*/ std::string RPMSimpleSystemInput::GetCSVHeader()
{
	return boost::str (boost::format("BreakingAcceleration"));
}

/*virtual*/ std::string RPMSimpleSystemInput::GetCSV()
{
	return boost::str (boost::format("%1%") % gasPedalLevel);
}

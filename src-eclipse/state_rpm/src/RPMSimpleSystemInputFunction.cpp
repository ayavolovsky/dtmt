/*
 * RPMSimpleSystemInputFunction.cpp
 *
 *  Created on: Oct 11, 2014
 *      Author: ayavol2
 */

#include "RPMSimpleSystemInputFunction.h"
#include <ModelInput.h>
#include <RPMSimpleSystemInput.h>
#include <stdio.h>
#include <math.h>

RPMSimpleSystemInputFunction::RPMSimpleSystemInputFunction(const std::string& inputFile) {
	InputFilePath = inputFile;

	ParseInputFile();
}

RPMSimpleSystemInputFunction::~RPMSimpleSystemInputFunction() {

}

bool RPMSimpleSystemInputFunction::ParseInputFile()
{
	TimeSequence.clear();
	GasPedalLevelSequence.clear();

	if (InputFilePath.empty())
	{
		return false;
	}

	FILE* f = fopen(InputFilePath.c_str(), "r");
	if (f==0)
	{
		return false;
	}

	float timeStamp, value, nextTimeStamp, nextValue;

	if (2!=fscanf(f, "%f,%f\n", &timeStamp, &value))
	{
		return false;
	}

	while (true)
	{
		if (EOF==fscanf(f, "%f,%f\n", &nextTimeStamp, &nextValue))
		{
			break;
		}

		TimeSequence.push_back(nextTimeStamp);
		GasPedalLevelSequence.push_back(nextValue);
	}

	return true;
}

/*virtual*/ boost::shared_ptr<ModelInput> RPMSimpleSystemInputFunction::getInput(double time)
{
	if (time>=TimeSequence.size())
	{
		boost::shared_ptr<RPMSimpleSystemInput> input = boost::shared_ptr<RPMSimpleSystemInput>( new RPMSimpleSystemInput(0.0, 0.0));
		return input;
	}

	double timeValue = TimeSequence[floor(time)];	// temporary solution. Need to change the way time is handled.
	double gasPedalValue = GasPedalLevelSequence[floor(time)];

	boost::shared_ptr<RPMSimpleSystemInput> input = boost::shared_ptr<RPMSimpleSystemInput>( new RPMSimpleSystemInput(timeValue, gasPedalValue));
	return input;
}

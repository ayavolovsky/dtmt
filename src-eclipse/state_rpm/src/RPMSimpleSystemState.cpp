/*
 * RPMSimpleSystemState.cpp
 *
 *  Created on: May 15, 2014
 *      Author: Andrey
 */

#include "RPMSimpleSystemState.h"
#include <RPMObservation.h>
#include <RPMSimpleSystemInput.h>
#include <BeliefState.h>
#include <Common.h>
#include <boost/format.hpp>
#include <math.h>
#include <algorithm>
#include <iostream>
#include <map>

using namespace std;

/*#define RPM_MAX_HIGH 120
#define PROP_T 10
#define RPM_MAX_LOW 115*/

/*#define RPM_MAX_HIGH 103.5
#define PROP_T 16
#define RPM_MAX_LOW 100.5*/

/*#define RPM_MAX_HIGH 110
#define PROP_T 6
#define RPM_MAX_LOW 109*/

// very good :)
#define RPM_MAX_HIGH 94
#define PROP_T 5
#define RPM_MAX_LOW 93

// alternative good with different RA
/*#define RPM_MAX_HIGH 93
#define PROP_T 3
#define RPM_MAX_LOW 92*/

#define R 0.5
#define AXLE_RATIO 1.7
#define GEAR_RATIO1 1.2
#define GEAR_RATIO2 0.8

/*#define A1 10
#define A2 8*/
#define A1 5
#define A2 4

#define RPM_1_2 50
#define RPM_2_1 40
#define RPM_2_MAX 90

#define RPM_N_MAX 150
#define RPM_N_IDLE 25

#define pi 3.1415926535897932384626433832795
#define L (2*pi*R)
#define RATIO1 (GEAR_RATIO1*AXLE_RATIO)
#define RATIO2 (GEAR_RATIO2*AXLE_RATIO)

#define WHEEL_RPM_MAX 150
#define d_WHEEL_RPM 0.25
#define TRANSMISION_GEARS_MAX 2
#define d_TRANSMISION_GEAR 1

#define P_d2_FAILURE 0.1
//#define P_d3_FAILURE 0.1	// TMP - 0.1
//#define P_d3_FAILURE 0.1	// TMP - 0.1
#define P_d3_FAILURE 0.1	// TMP - 0.1
//#define P_d8_RECOVER_TO_LIMITING_RPM 0.05
#define P_d8_RECOVER_TO_LIMITING_RPM 0.1
#define P_GEAR_OBSERVED_INCORRECTLY 0.1

#define N1_mu 0
#define N1_variance 1
#define N1_fail_mu 0
#define N1_fail_variance 1.5
#define N2_mu 0
#define N2_variance 0.8
#define N2_fail_mu 0
#define N2_fail_variance 1.2
#define N3_mu		0
#define N3_variance	3.2
#define N4_mu 0
//#define N4_variance 5
#define N4_variance 1.5

#define TIME_STEP 1

#define INPUT_QUANTIZATION 0.25

using namespace std;

/*static*/ double RPMSimpleSystemState::N1_standard_deviation = sqrt((double)N1_variance);
/*static*/ double RPMSimpleSystemState::N1_fail_standard_deviation = sqrt(N1_fail_variance);
/*static*/ double RPMSimpleSystemState::N2_standard_deviation = sqrt(N2_variance);
/*static*/ double RPMSimpleSystemState::N2_fail_standard_deviation = sqrt(N2_fail_variance);
/*static*/ double RPMSimpleSystemState::N3_standard_deviation = sqrt(N3_variance);
/*static*/ double RPMSimpleSystemState::N4_standard_deviation = sqrt(N4_variance);
/*static*/ std::vector<boost::shared_ptr<ModelInput> > RPMSimpleSystemState::possibleInputs = std::vector<boost::shared_ptr<ModelInput> >();
/*static*/ std::vector< boost::shared_ptr<ModelObservation> > RPMSimpleSystemState::possibleObservations = std::vector<boost::shared_ptr<ModelObservation> >();

RPMSimpleSystemState::RPMSimpleSystemState() {
	if (possibleInputs.empty())
	{
		GetPossibleInputs(possibleInputs);
	}

	//static unsigned int numObservations = 2*ceil(WHEEL_RPM_MAX/d_WHEEL_RPM)*ceil(TRANSMISION_GEARS_MAX/d_TRANSMISION_GEAR);
	if (possibleObservations.size()==0)
	{
		for (double rpm = 0; rpm <= 2*WHEEL_RPM_MAX; rpm += d_WHEEL_RPM)
		{
			for (int gear = 1; gear <= TRANSMISION_GEARS_MAX; gear += d_TRANSMISION_GEAR)
			{
				boost::shared_ptr<RPMObservation> rpmObservation = boost::shared_ptr<RPMObservation>(new RPMObservation());
				rpmObservation->wheelRPM = rpm;
				rpmObservation->gear = gear;

				possibleObservations.push_back( boost::static_pointer_cast<ModelObservation> (rpmObservation) );
			}
		}
	}
}

RPMSimpleSystemState::~RPMSimpleSystemState() {

}

/*virtual*/ void RPMSimpleSystemState::ResetStart()
{
	sys.t = 0;
	sys.d = 1;
	sys.c = 0;
	sys.rpm = 6*RATIO1;
	prop.q = 1;					// mode
	prop.counter = 0;			// counter
}

/*virtual*/ bool RPMSimpleSystemState::IsFailureState()
{
	return (prop.q==3);
}

boost::shared_ptr<ModelInput> RPMSimpleSystemState::GetInput(boost::shared_ptr<ModelInputFunction> InputFunction)
{
	if (!InputFunction)
	{
		return boost::shared_ptr<ModelInput>();
	}

	boost::shared_ptr<ModelInput> input = InputFunction->getInput(sys.t);

	return input;
}

/*virtual*/ void RPMSimpleSystemState::Continue(bool bFailureEnabled, boost::shared_ptr<ModelInput> input, boost::shared_ptr<ModelInput>& actualInput)
{
	Continue_System(bFailureEnabled, input, actualInput);
	Continue_Property();
}

void RPMSimpleSystemState::Continue_System_Transition(bool bFailureEnabled, boost::shared_ptr<ModelInput> abstractInput)
{
	double accelerationRatio = 1/TIME_STEP;

	if (!abstractInput)
	{
		return;
	}

	boost::shared_ptr<RPMSimpleSystemInput> pGasPedalInput = boost::dynamic_pointer_cast<RPMSimpleSystemInput>(abstractInput);

	double u = pGasPedalInput->gasPedalLevel;

	switch (sys.d)
	{
	case 1:
		if (sys.rpm >= RPM_1_2)
		{
			sys.d = 2;
			sys.c = -1.0 * fabs(sys.c);
		}
		break;
	case 2:
		if (sys.rpm < RPM_1_2 && sys.c<0)
		{
			sys.d = 1;
		}
		else
		if (sys.c >= 0)
		{
			/*bool bFail = false;
			if ((isProbabilisticFailureEnabled() && UniformRand(0, 1)<P_d2_FAILURE) ||
					isImmediateFailureEnabled())
			{
				bFail = true;
			}

			if (bFail)
			{
				sys.d = 7;
			}
			else
			{*/
				sys.d 			= 3;
				sys.gear 		= 2;
				sys.rpm 		= RATIO2 * sys.rpm / RATIO1;
				sys.wheelrpm 	= sys.rpm / RATIO2;
			//}
		}
		break;
	case 7:
		{
			/*if (sys.rpm<RPM_1_2)
			{
				sys.d		= 1;
				sys.c		= -1.0 * fabs(sys.c);
				sys.gear	= 1;
			}*/
			break;
		}
		break;
	case 3:
		if (sys.rpm<RPM_2_1)
		{
			sys.d = 6;
			sys.c = -1.0 * fabs(sys.c);
		}
		else if (sys.rpm>=RPM_2_MAX)
		{
			bool bFail = false;
			/*if ((isProbabilisticFailureEnabled() && UniformRand(0, 1)<P_d3_FAILURE) ||
					isImmediateFailureEnabled())*/

			if (UniformRand(0, 1)<P_d3_FAILURE)
			{
				bFail = true;
			}

			if (bFail)
			{
				sys.d			= 8;
				sys.gear		= 2;
				sys.rpm			= sys.rpm + ( (RATIO2/L)*A2*u + NormalRand(N2_mu, N2_standard_deviation) )/accelerationRatio;
				sys.wheelrpm	= sys.rpm / RATIO2;
			}
			else
			{
				sys.d = 4;
				sys.gear = 2;
			}
		}
		break;
	case 4:
		if (u<0)
		{
			sys.d = 5;
		}
		break;
	case 5:
		if (u<0)
		{
			if (sys.rpm < RPM_2_MAX)
			{
				sys.d = 3;
			}
		}
		else
		{
			if (sys.rpm >= RPM_2_MAX)
			{
				sys.d = 4;
			}
		}
		break;
	case 6:
		if (sys.rpm >= RPM_2_1 && sys.c<0)
		{
			sys.d = 3;
		}
		else
		if (sys.c>=0)
		{
			sys.d			= 1;
			sys.rpm			= RATIO1 * sys.rpm / RATIO2;
			sys.wheelrpm 	= sys.rpm / RATIO1;
			sys.gear 		= 1;
		}
		break;
	case 8:
		if (sys.rpm<RPM_2_1)
		{
			sys.d = 6;
			sys.c = -1.0 * fabs(sys.c);
			sys.gear = 2;
		}
		/*else
		{
			if (UniformRand(0, 1)<P_d8_RECOVER_TO_LIMITING_RPM)
			{
				sys.d = 4;
				sys.gear = 2;
			}
		}*/
		break;
	}
}

void RPMSimpleSystemState::Continue_System(bool bFailureEnabled, boost::shared_ptr<ModelInput> pAbstractInput, boost::shared_ptr<ModelInput>& actualInput)
{
	double accelerationRatio = 1/TIME_STEP;

	if (!pAbstractInput)
	{
		// Generate some input for the simulations
		pAbstractInput = GenerateSampleInput();
	}

	actualInput = pAbstractInput;

	boost::shared_ptr<RPMSimpleSystemInput> pGasPedalInput = boost::dynamic_pointer_cast<RPMSimpleSystemInput>(pAbstractInput);

	double u = pGasPedalInput->gasPedalLevel;

	switch (sys.d)
	{
	case 1:
		sys.rpm = sys.rpm + ( (RATIO1/L)*A1*u + NormalRand(N1_mu, N1_standard_deviation) )/accelerationRatio;
		if (sys.rpm<0.0) sys.rpm = 0.0;

		sys.gear 		= 1;
		sys.wheelrpm 	= sys.rpm / RATIO1;
		sys.c 			= 2 + NormalRand(N4_mu, N4_standard_deviation);
		break;
	case 2:
		sys.rpm = sys.rpm + ( (RATIO1/L)*A1*u + NormalRand(N1_mu, N1_standard_deviation) )/accelerationRatio;
		if (sys.rpm<0.0) sys.rpm = 0.0;

		sys.gear		= 1;
		sys.wheelrpm	= sys.rpm / RATIO1;
		sys.c			= sys.c + TIME_STEP;
		break;
	case 3:
		sys.rpm = sys.rpm + ( (RATIO2/L)*A2*u + NormalRand(N2_mu, N2_standard_deviation) ) /accelerationRatio;
		if (sys.rpm<0.0) sys.rpm = 0.0;

		sys.gear		= 2;
		sys.wheelrpm	= sys.rpm / RATIO2;
		sys.c 			= 2 + NormalRand(N4_mu, N4_standard_deviation);
		break;
	case 4:
		sys.c = NormalRand(N4_mu, N4_standard_deviation);
		break;
	case 5:
		sys.rpm = sys.rpm + ( (RATIO2/L)*A2*u + NormalRand(N2_mu, N2_standard_deviation) )/accelerationRatio;
		if (sys.rpm<0.0) sys.rpm = 0.0;

		sys.gear		= 2;
		sys.wheelrpm	= sys.rpm / RATIO2;
		sys.c 			= 2 + NormalRand(N4_mu, N4_standard_deviation);
		break;
	case 6:
		sys.rpm = sys.rpm + ( (RATIO2/L)*A2*u + NormalRand(N2_mu, N2_standard_deviation) )/accelerationRatio;
		if (sys.rpm<0.0) sys.rpm = 0.0;

		sys.gear 		= 2;
		sys.wheelrpm 	= sys.rpm / RATIO2;
		sys.c 			= sys.c + TIME_STEP;
		break;
	/*case 7:
		if (u>=0)
		{
			sys.rpm = RPM_N_IDLE + RPM_N_MAX*u + NormalRand(N1_fail_mu, N1_fail_standard_deviation);
		}

		if (sys.rpm<0.0) sys.rpm = 0.0;

		sys.gear = 1;
		sys.wheelrpm = sys.rpm / RATIO1;
		sys.c = 2 + NormalRand(N4_mu, N4_standard_deviation);
		break;*/
	case 8:
		sys.rpm = sys.rpm + ( (RATIO2/L)*A2*u + NormalRand(N2_mu, N2_standard_deviation) )/accelerationRatio;
		if (sys.rpm<0.0) sys.rpm = 0.0;

		sys.gear 		= 2;
		sys.wheelrpm 	= sys.rpm / RATIO2;
		sys.c 			= NormalRand(N4_mu, N4_standard_deviation);
		break;
	}

	Continue_System_Transition(bFailureEnabled, pAbstractInput);

	sys.t = sys.t + TIME_STEP;
}

void RPMSimpleSystemState::Continue_Property()
{
	switch (prop.q)
	{
		case 1:
			if (sys.rpm>RPM_MAX_HIGH)
			{
				prop.q = 2;
				prop.counter = PROP_T;
			}
			break;
		case 2:
			prop.counter -= TIME_STEP;

			if (prop.counter<=0)
			{
				prop.q = 3;
			}
			else
			if (sys.rpm<RPM_MAX_LOW && prop.counter>0)
			{
				prop.q = 1;
			}
			break;
		case 3:
			// nothing changes
			break;
	}
}

/*virtual*/ void RPMSimpleSystemState::GetPossibleInputs(std::vector<boost::shared_ptr<ModelInput> >& inputs)
{
	if (!possibleInputs.empty())
	{
		inputs = possibleInputs;
	}
	else
	{
		for (double v=-1; v<1; v+=INPUT_QUANTIZATION)
		{
			boost::shared_ptr<RPMSimpleSystemInput> pGasPedalInput = boost::shared_ptr<RPMSimpleSystemInput>(new RPMSimpleSystemInput(0, v));
			inputs.push_back(pGasPedalInput);
		}
		boost::shared_ptr<RPMSimpleSystemInput> pGasPedalInput = boost::shared_ptr<RPMSimpleSystemInput>(new RPMSimpleSystemInput(0, 1));
		inputs.push_back(pGasPedalInput);
	}
}

/*virtual*/ unsigned long RPMSimpleSystemState::GetInputID(boost::shared_ptr<ModelInput> input)
{
	boost::shared_ptr<RPMSimpleSystemInput> pGasPedalInput = boost::dynamic_pointer_cast<RPMSimpleSystemInput>(input);

	unsigned long nID = floor( (pGasPedalInput->gasPedalLevel + 1)/INPUT_QUANTIZATION );
	return nID;
}

boost::shared_ptr<ModelInput> RPMSimpleSystemState::GenerateSampleInput()
{
	double t = 0;	// not used
	double v = 0;

	double p = UniformRand(-1, 1);
	double p_step = INPUT_QUANTIZATION;

	v = floor(p/p_step) * p_step;
	//v = 0.0;

	boost::shared_ptr<RPMSimpleSystemInput> pGasPedalInput = boost::shared_ptr<RPMSimpleSystemInput>(new RPMSimpleSystemInput(t, v));

	return pGasPedalInput;
}

double RPMSimpleSystemState::GetVelocity()
{
	double velocity = 0;

	if (sys.d==1 || sys.d==2)
	{
		velocity = sys.rpm*L/RATIO1;
	}
	else
	{
		velocity = sys.rpm*L/RATIO2;
	}

	return velocity;
}

/*virtual*/ void RPMSimpleSystemState::FullyObserve(boost::shared_ptr<ModelObservation>& observation)
{
	boost::shared_ptr<RPMObservation> rpmObservation = boost::shared_ptr<RPMObservation>(new RPMObservation());

	rpmObservation->wheelRPM = sys.wheelrpm;
	rpmObservation->engineRPM = sys.rpm;
	rpmObservation->gear = sys.gear;
	rpmObservation->system_d = sys.d;
	rpmObservation->property_q = prop.q;

	observation = boost::static_pointer_cast<ModelObservation> (rpmObservation);
}

/*virtual*/ unsigned long RPMSimpleSystemState::GetObservationID(boost::shared_ptr<ModelObservation> observation)
{
	boost::shared_ptr<RPMObservation> rpmObservation = boost::static_pointer_cast<RPMObservation> (observation);

	unsigned long obs = CompressObservation(rpmObservation->wheelRPM, rpmObservation->gear);

	return obs;
}

/*virtual*/ boost::shared_ptr<ModelObservation> RPMSimpleSystemState::GetObservation()
{
	boost::shared_ptr<RPMObservation> rpmObservation = boost::shared_ptr<RPMObservation>(new RPMObservation());

	double observedWheelRPM = 0;
	switch (sys.gear)
	{
	case 1:
		observedWheelRPM = sys.rpm / RATIO1;
		break;
	case 2:
		observedWheelRPM = sys.rpm / RATIO2;
		break;
	}
	observedWheelRPM = observedWheelRPM + NormalRand(N3_mu, N3_standard_deviation);

	if (observedWheelRPM<0)
	{
		observedWheelRPM = 0;
	}

	int observedGear = 1;
	double p = UniformRand(0, 1);

	if (p<P_GEAR_OBSERVED_INCORRECTLY)
	{
		if (sys.gear==1)
		{
			observedGear = 2;
		}
		else
		{
			observedGear = 1;
		}
	}
	else
	{
		observedGear = sys.gear;
	}

	rpmObservation->wheelRPM = observedWheelRPM;
	rpmObservation->gear = observedGear;

	return boost::static_pointer_cast<ModelObservation>(rpmObservation);
}

void RPMSimpleSystemState::ExtractObservation(unsigned int observationID, double& rpm, int& gear)
{
	int rpmWheelObservationID = floor((double) observationID/TRANSMISION_GEARS_MAX);
	int gearObservationID = observationID % TRANSMISION_GEARS_MAX;

	rpm = rpmWheelObservationID * d_WHEEL_RPM;
	gear = gearObservationID + 1;
}

unsigned int RPMSimpleSystemState::CompressObservation(double wheelRpm, int gear)
{
	unsigned int wheelObsID = (int) floor(wheelRpm/d_WHEEL_RPM);
	unsigned int obs = wheelObsID*TRANSMISION_GEARS_MAX + gear - 1;

	if (obs>GetMaxObservations())
	{
		obs = GetMaxObservations();
		if (gear==1)
		{
			obs--;
		}
	}

	return obs;
}

/*virtual*/ double RPMSimpleSystemState::WeightObservation(boost::shared_ptr<ModelObservation> observation)
{
	boost::shared_ptr<RPMObservation> rpmObservation = boost::static_pointer_cast<RPMObservation> (observation);

	double observationWheelRPM = rpmObservation->wheelRPM;
	int observedGear = rpmObservation->gear;

	double currentWheelRPM = 0;
	switch (sys.gear)
	{
	case 1:
		currentWheelRPM = sys.rpm / RATIO1;
		break;
	case 2:
		currentWheelRPM = sys.rpm / RATIO2;
		break;
	}

	double p_wheel_rpm = NormalProbability(N3_mu, N3_standard_deviation, N3_variance, observationWheelRPM - currentWheelRPM);

	double p_gear;
	if (sys.gear==observedGear)
	{
		p_gear = 1 - P_GEAR_OBSERVED_INCORRECTLY;
	}
	else
	{
		p_gear = P_GEAR_OBSERVED_INCORRECTLY;
	}

	double p = 0;
	p = p_wheel_rpm*p_gear;

	return p;
}


/*virtual*/ /*double RPMSimpleSystemState::WeightObservation(boost::shared_ptr<ModelObservation> observation)
{
	boost::shared_ptr<RPMObservation> rpmObservation = boost::static_pointer_cast<RPMObservation> (observation);

	double observationWheelRPM = rpmObservation->wheelRPM;
	int observedGear = rpmObservation->gear;

	double currentWheelRPM = 0;
	double currentWheelRPM_other = 0;

	double p_gear = 0;
	switch (observedGear)
	{
	case 1:
		currentWheelRPM = sys.rpm / RATIO1;
		currentWheelRPM_other = sys.rpm / RATIO2;
		break;
	case 2:
		currentWheelRPM = sys.rpm / RATIO2;
		currentWheelRPM_other = sys.rpm / RATIO1;
		break;
	}
	double p_rpm_gear = NormalProbability(N3_mu, N3_standard_deviation, N3_variance, observationWheelRPM - currentWheelRPM);
	double p_rpm_gear_other = NormalProbability(N3_mu, N3_standard_deviation, N3_variance, observationWheelRPM - currentWheelRPM_other);

	if (sys.gear==observedGear)
	{
		p_gear = 1 - P_GEAR_OBSERVED_INCORRECTLY;
	}
	else
	{
		p_gear = P_GEAR_OBSERVED_INCORRECTLY;
	}

	double p = 0;

	p = (p_gear * p_rpm_gear + (1-p_gear) * p_rpm_gear_other)*p_gear;

	return p;
}*/

/*virtual*/ unsigned int RPMSimpleSystemState::GetMaxObservations()
{
	static unsigned int numObservations = 2*ceil((double)WHEEL_RPM_MAX/d_WHEEL_RPM)*ceil((double) TRANSMISION_GEARS_MAX/d_TRANSMISION_GEAR);
	return numObservations;
}

/*virtual*/ std::vector< boost::shared_ptr<ModelObservation> >& RPMSimpleSystemState::GetPossibleObservations()
{
	return possibleObservations;
}

/*virtual*/ double RPMSimpleSystemState::GetTime()
{
	return sys.t;
}

/*virtual*/ string RPMSimpleSystemState::ToString()
{
	int gear;
	double wheelrpm;
	double incorrectWheelrpm;

	switch (sys.d)
	{
	case 1:
	case 2:
	case 7:
		gear 		= 1;
		wheelrpm 	= sys.rpm / RATIO1;
		incorrectWheelrpm = sys.rpm / RATIO2;
		break;
	case 3:
	case 4:
	case 5:
	case 6:
	case 8:
		gear 		= 2;
		wheelrpm 	= sys.rpm / RATIO2;
		incorrectWheelrpm = sys.rpm / RATIO1;
		break;
	}

	string str = boost::str (boost::format("t = %1%, d = %2%, c = %3%, rpm = %4%, [gear = %5%, wheelrpm = %6%, incorrectwheelrpm = %7%] %8%") % sys.t % sys.d % sys.c % sys.rpm % gear % wheelrpm % incorrectWheelrpm % PROP_T);
	string str_prop = boost::str (boost::format("pq = %1%, pc = %2%") % prop.q % prop.counter);

	str = str + ", " + str_prop;

	return str;
}

/*virtual*/ std::string RPMSimpleSystemState::ObservationToString(boost::shared_ptr<ModelObservation> observation)
{
	boost::shared_ptr<RPMObservation> rpmObservation = boost::static_pointer_cast<RPMObservation> (observation);
	string str = boost::str (boost::format("WheelRPM = %1%, Gear = %2%") % rpmObservation->wheelRPM % rpmObservation->gear);

	return str;
}

/*virtual*/ bool RPMSimpleSystemState::FromCSVTokens(std::vector<std::string>& tokens, boost::shared_ptr<ModelObservation> observation, boost::shared_ptr<ModelInput> input, bool bUpdateProperty, const boost::shared_ptr<State>& currentRealState)
{
	if (tokens.size()<4)
	{
		return false;
	}

	sys.t	= strtoul(tokens.at(0).c_str(), NULL, 10);
	sys.d	= strtoul(tokens.at(1).c_str(), NULL, 10);
	sys.c	= strtod(tokens.at(2).c_str(), NULL);
	sys.rpm	= strtod(tokens.at(3).c_str(), NULL);

	if (tokens.size()<(4+2))
	{
		return false;
	}

	int index = 3;

	if (bUpdateProperty)
	{
		boost::shared_ptr<RPMSimpleSystemState> previousState = boost::static_pointer_cast<RPMSimpleSystemState> (currentRealState);

		prop.q			= previousState->prop.q;
		prop.counter	= previousState->prop.counter;
		Continue_Property();
	}
	else
	{
		prop.q			= strtoul(tokens.at(index+1).c_str(), NULL, 10);
		prop.counter	= strtoul(tokens.at(index+2).c_str(), NULL, 10);
	}

	if (prop.q==0)
	{
		prop.q = 1;
	}

	if (observation!=boost::shared_ptr<ModelObservation>() && input!=boost::shared_ptr<ModelInput>())
	{
		boost::shared_ptr<RPMObservation> rpmObs = boost::static_pointer_cast<RPMObservation> (observation);
		rpmObs->wheelRPM = strtod(tokens.at(index+3).c_str(), NULL);
		rpmObs->gear = strtoul(tokens.at(index+4).c_str(), NULL, 10);

		boost::shared_ptr<RPMSimpleSystemInput> rpmInput = boost::static_pointer_cast<RPMSimpleSystemInput> (input);
		rpmInput->gasPedalLevel = strtod(tokens.at(index+5).c_str(), NULL);
	}

	return true;
}

/*virtual*/ string RPMSimpleSystemState::GetCSV()
{
	string str = boost::str (boost::format("%1%, %2%, %3%, %4%") % sys.t % sys.d % sys.c % sys.rpm);
	string str_prop = boost::str (boost::format("%1%, %2%") % prop.q % prop.counter);
	str = str + ", " + str_prop;

	return str;
}

/*virtual*/ string RPMSimpleSystemState::GetCSVHeader()
{
	string str = boost::str (boost::format("%1%, %2%, %3%, %4%") 	% "sys.t"
																	% "sys.d"
																	% "sys.c"
																	% "sys.rpm");

	string str_prop = boost::str (boost::format("%1%, %2%") % "prop.q" % "prop.counter");
	str = str + ", " + str_prop;

	return str;
}

/*virtual*/ string RPMSimpleSystemState::BeliefToString(const BeliefState& belief)
{
	unsigned int q[3] = {0,0,0};
	unsigned int d[8] = {0,0,0,0,0,0,0,0};

	map<unsigned int, double> minRPM;
	map<unsigned int, double> maxRPM;
	map<unsigned int, double> avgRPM;

	for (unsigned int iParticle=0; iParticle<belief.GetSize(); iParticle++)
	{
		boost::shared_ptr<State> beliefState = belief.GetParticle(iParticle);
		boost::shared_ptr<RPMSimpleSystemState> thisState = boost::static_pointer_cast<RPMSimpleSystemState>(beliefState);

		q[thisState->prop.q - 1]++;
		d[thisState->sys.d - 1]++;

		if (minRPM.find(thisState->sys.d - 1)==minRPM.end() || thisState->sys.rpm < minRPM.at(thisState->sys.d - 1))
		{
			minRPM[thisState->sys.d - 1] = thisState->sys.rpm;
		}

		if (maxRPM.find(thisState->sys.d - 1)==maxRPM.end() || thisState->sys.rpm > maxRPM.at(thisState->sys.d - 1))
		{
			maxRPM[thisState->sys.d - 1] =  thisState->sys.rpm;
		}

		if (avgRPM.find(thisState->sys.d - 1)==avgRPM.end())
		{
			avgRPM[thisState->sys.d - 1] = thisState->sys.rpm;
		}
		else
		{
			avgRPM.at(thisState->sys.d - 1) += thisState->sys.rpm;
		}
	}

	for (map<unsigned int, double>::iterator iAvg = avgRPM.begin(); iAvg!=avgRPM.end(); iAvg++)
	{
		if (d[iAvg->first]==0)
		{
			continue;
		}

		iAvg->second /= d[iAvg->first];
	}

	double q_values[3] = {0.0, 0.0, 0.0};
	double d_values[8] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};

	if (belief.GetSize()!=0)
	{
		q_values[0] = (1.0*q[0])/belief.GetSize();
		q_values[1] = (1.0*q[1])/belief.GetSize();
		q_values[2] = (1.0*q[2])/belief.GetSize();

		d_values[0] = (1.0*d[0])/belief.GetSize();
		d_values[1] = (1.0*d[1])/belief.GetSize();
		d_values[2] = (1.0*d[2])/belief.GetSize();
		d_values[3] = (1.0*d[3])/belief.GetSize();
		d_values[4] = (1.0*d[4])/belief.GetSize();
		d_values[5] = (1.0*d[5])/belief.GetSize();
		d_values[6] = (1.0*d[6])/belief.GetSize();
		d_values[7] = (1.0*d[7])/belief.GetSize();
	}

	string str_system = "";
	for (int i=0; i<8; i++)
	{
		string strPart = "";

		if (d_values[i]==0)
		{
			strPart = boost::str (boost::format("d=%1% (%2%)") % (i+1) % d_values[i]);
		}
		else
		{
			strPart = boost::str (boost::format("d=%1% (%2%) {avg = %3%, [%4%, %5%] }")
												% (i+1)
												% d_values[i]
											    % avgRPM.at(i)
											    % minRPM.at(i)
											    % maxRPM.at(i));
		}

		if (!str_system.empty())
		{
			str_system += ", ";
		}
		str_system += strPart;
	}

	string str = boost::str (boost::format("\n\tproperty[q=1 (%1%), q=2 (%2%), q=3 (%3%)]\n\tsystem[%4%]")
								% q_values[0]
								% q_values[1]
								% q_values[2]
								% str_system);

	return str;
}

/*virtual*/ double RPMSimpleSystemState::FailureProbability(const BeliefState& belief)
{
	unsigned int q3 = 0;

	for (unsigned int iParticle=0; iParticle<belief.GetSize(); iParticle++)
	{
		boost::shared_ptr<State> beliefState = belief.GetParticle(iParticle);
		boost::shared_ptr<RPMSimpleSystemState> thisState = boost::static_pointer_cast<RPMSimpleSystemState>(beliefState);

		if (thisState->prop.q==3)
			q3++;
	}

	return (1.0*q3)/belief.GetSize();
}

/*virtual*/ void RPMSimpleSystemState::Copy(boost::shared_ptr<State> other)
{
	boost::shared_ptr<RPMSimpleSystemState> state = boost::static_pointer_cast<RPMSimpleSystemState>(other);
	sys  = state->sys;
	prop = state->prop;
}

/*virtual*/ void RPMSimpleSystemState::AddObservationDisturbance(boost::shared_ptr<ModelObservation> observation)
{
	boost::shared_ptr<RPMObservation> rpmObservation = boost::static_pointer_cast<RPMObservation> (observation);
	//rpmObservation->engineRPM += NormalRand(N3_mu, N3_standard_deviation);

	if (UniformRand(0, 1)<P_GEAR_OBSERVED_INCORRECTLY)
	{
		if (rpmObservation->gear==1)
		{
			rpmObservation->gear = 2;
		}
		else if (rpmObservation->gear==2)
		{
			rpmObservation->gear = 1;
		}
	}
}

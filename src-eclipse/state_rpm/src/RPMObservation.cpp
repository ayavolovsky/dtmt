/*
 * RPMObservation.cpp
 *
 *  Created on: Oct 13, 2014
 *      Author: ayavol2
 */

#include "RPMObservation.h"
#include <boost/format.hpp>

RPMObservation::RPMObservation() {
	// TODO Auto-generated constructor stub
	engineRPM = 0.0;
	wheelRPM = 0.0;

	system_d = 0;
	property_q = 0;

	gear = 0;
}

RPMObservation::~RPMObservation() {
	// TODO Auto-generated destructor stub
}

/*virtual*/ ModelObservation* RPMObservation::Clone()
{
	RPMObservation* copy = new RPMObservation();
	copy->wheelRPM = wheelRPM;
	copy->gear = gear;
	copy->engineRPM = engineRPM;
	copy->system_d = property_q;
	copy->property_q = property_q;

	return copy;
}

/*virtual*/ void RPMObservation::Copy(boost::shared_ptr<ModelObservation> other)
{
	boost::shared_ptr<RPMObservation> rpmObsOther = boost::static_pointer_cast<RPMObservation> (other);

	wheelRPM = rpmObsOther->wheelRPM;
	gear = rpmObsOther->gear;
	engineRPM = rpmObsOther->engineRPM;
	system_d = rpmObsOther->property_q;
	property_q = rpmObsOther->property_q;
}

/*virtual*/ std::string RPMObservation::GetCSVHeader()
{
	std::string str = boost::str (boost::format("Observed Wheel RPM, Observed Gear"));
	return str;
}

/*virtual*/ std::string RPMObservation::GetCSV()
{
	std::string str = boost::str (boost::format("%1%, %2%") % wheelRPM % gear);
	return str;
}

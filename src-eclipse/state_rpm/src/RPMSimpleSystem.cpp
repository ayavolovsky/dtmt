/*
 * RPMSimpleSystem.cpp
 *
 *  Created on: May 15, 2014
 *      Author: Andrey
 */

#include "RPMSimpleSystem.h"
#include "RPMSimpleSystemState.h"

RPMSimpleSystem::RPMSimpleSystem() {
	// TODO Auto-generated constructor stub

}

RPMSimpleSystem::~RPMSimpleSystem() {
	// TODO Auto-generated destructor stub
}

/*virtual*/ boost::shared_ptr<State> RPMSimpleSystem::CreateNewState()
{
	return StateFactory.Create();
}

/*virtual*/ boost::shared_ptr<ModelObservation> RPMSimpleSystem::CreateNewObservation()
{
	return ObservationFactory.Create();
}

/*virtual*/ boost::shared_ptr<ModelInput> RPMSimpleSystem::CreateNewInput()
{
	return InputFactory.Create();
}

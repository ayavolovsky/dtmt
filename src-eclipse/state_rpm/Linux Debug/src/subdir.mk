################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/RPMObservation.cpp \
../src/RPMSimpleSystem.cpp \
../src/RPMSimpleSystemInput.cpp \
../src/RPMSimpleSystemInputFunction.cpp \
../src/RPMSimpleSystemState.cpp 

OBJS += \
./src/RPMObservation.o \
./src/RPMSimpleSystem.o \
./src/RPMSimpleSystemInput.o \
./src/RPMSimpleSystemInputFunction.o \
./src/RPMSimpleSystemState.o 

CPP_DEPS += \
./src/RPMObservation.d \
./src/RPMSimpleSystem.d \
./src/RPMSimpleSystemInput.d \
./src/RPMSimpleSystemInputFunction.d \
./src/RPMSimpleSystemState.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I"/home/andrey/Documents/src-eclipse/state_rpm/inc" -I"/home/andrey/Documents/src-eclipse/monitoring_common/inc" -O0 -g3 -Wall -c -fmessage-length=0 -fPIC -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '



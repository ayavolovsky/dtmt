/*
 * SingleCarBreakingSystem.cpp
 *
 *  Created on: May 14, 2013
 *      Author: ayavol2
 */

#include "MultipleCarBreakingSystem.h"
#include "MultipleCarBreakingSystemState.h"

MultipleCarBreakingSystem::MultipleCarBreakingSystem() {
	// TODO Auto-generated constructor stub

}

MultipleCarBreakingSystem::~MultipleCarBreakingSystem() {
	// TODO Auto-generated destructor stub
}

/*virtual*/ boost::shared_ptr<State> MultipleCarBreakingSystem::CreateNewState()
{
	return StateFactory.Create();
}

/*virtual*/ boost::shared_ptr<ModelObservation> MultipleCarBreakingSystem::CreateNewObservation()
{
	return ObservationFactory.Create();
}

/*virtual*/ boost::shared_ptr<ModelInput> MultipleCarBreakingSystem::CreateNewInput()
{
	return boost::shared_ptr<ModelInput>();
}

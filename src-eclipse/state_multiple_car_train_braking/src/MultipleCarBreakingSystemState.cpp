/*
 * SingleCarBreakingSystemState.cpp
 *
 *  Created on: May 14, 2013
 *      Author: ayavol2
 */

#include "MultipleCarBreakingSystemState.h"
#include "VelocityObservation.h"
#include <BeliefState.h>
#include <Common.h>
#include <math.h>
#include <algorithm>
#include <boost/format.hpp>

#define INITIAL_VELOCITY 26

#define V_U 		28.5	// Upper level of velocity
#define V_L 		20		// Lower level of velocity
#define d_V 		0.1		// Discretization step of velocity
#define T_prime		20		// Static time out to convert to safety automaton
#define P_q2_q3		0.1		// probability of transition from state d2 to d3

#define N1_mu		0
#define N1_variance	1
#define N2_mu		0
#define N2_variance	0.1
#define N3_mu		0
#define N3_variance	3
#define N4_mu		0
#define N4_variance	3
#define N5_mu		0
#define N5_variance	3

using namespace std;

/*static*/ double MultipleCarBreakingSystemState::N1_standard_deviation = sqrt((double)N1_variance);
/*static*/ double MultipleCarBreakingSystemState::N2_standard_deviation = sqrt((double)N2_variance);
/*static*/ double MultipleCarBreakingSystemState::N3_standard_deviation = sqrt((double)N3_variance);
/*static*/ double MultipleCarBreakingSystemState::N4_standard_deviation = sqrt((double)N4_variance);
/*static*/ double MultipleCarBreakingSystemState::N5_standard_deviation = sqrt((double)N5_variance);

MultipleCarBreakingSystemState::MultipleCarBreakingSystemState() {

}

MultipleCarBreakingSystemState::~MultipleCarBreakingSystemState() {

}

/*virtual*/ void MultipleCarBreakingSystemState::ResetStart()
{
	sys.N = 10;
	sys.i = 0;							// # of engaged brakes

	sys.velocity.q = 1;					// mode
	sys.velocity.t = 1;					// time
	sys.velocity.v = INITIAL_VELOCITY;	// velocity

	for (int i=0; i<sys.N; i++)
	{
		BreakingSubsystem brake;
		brake.q = 1;	// mode
		brake.c1 = 0;
		brake.c2 = 0;

		sys.brakes.push_back(brake);
	}

	prop.q = 1;					// mode
	prop.counter = 0;			// counter
}

/*virtual*/ bool MultipleCarBreakingSystemState::IsFailureState()
{
	return (prop.q==3);
}

boost::shared_ptr<ModelInput> MultipleCarBreakingSystemState::GetInput(boost::shared_ptr<ModelInputFunction> InputFunction)
{
	if (!InputFunction)
	{
		return boost::shared_ptr<ModelInput>();
	}

	return InputFunction->getInput(sys.velocity.t);
}

/*virtual*/ void MultipleCarBreakingSystemState::Continue(bool bFailureEnabled, boost::shared_ptr<ModelInput> input, boost::shared_ptr<ModelInput>& actualInput)
{
	Continue_System(bFailureEnabled);
	Continue_Property();
}

void MultipleCarBreakingSystemState::Continue_System(bool bFailureEnabled)
{
	Continue_VelocitySubsystem(bFailureEnabled);

	for (vector<BreakingSubsystem>::iterator iBrake = sys.brakes.begin(); iBrake!=sys.brakes.end(); iBrake++)
	{
		Continue_BrakingSubsystem(*iBrake, bFailureEnabled);
	}
}

void MultipleCarBreakingSystemState::Continue_VelocitySubsystem(bool bFailureEnabled)
{
	switch (sys.velocity.q)
	{
	case 1:
		sys.velocity.v = 0.1353*sys.velocity.v + 0.8647*(25 + 2.5*sin((double) sys.velocity.t)) + NormalRand(N1_mu, N1_standard_deviation);

		if (sys.velocity.v>V_U)	// transition condition
		{
			sys.velocity.q = 2;
		}
		break;
	case 2:
		sys.velocity.v = sys.velocity.v - 0.5* max(0, sys.i - (int) floor((double)sys.N/3.0)) + NormalRand(N2_mu, N2_standard_deviation);

		if (sys.i>0)	// transition condition
		{
			sys.velocity.q = 3;
		}
		break;
	case 3:
		sys.velocity.v = sys.velocity.v - 0.5* max(0, sys.i - (int) floor((double)sys.N/3.0)) + NormalRand(N3_mu, N3_standard_deviation);

		if (sys.i==0)	// transition condition
		{
			sys.velocity.q = 1;
		}
		break;
	}

	// increment time counter
	sys.velocity.t += 1;
}

void MultipleCarBreakingSystemState::Continue_BrakingSubsystem(BreakingSubsystem& brake, bool bFailureEnabled)
{
	switch (brake.q)
	{
	case 1:
		brake.c1 = NormalRand(N4_mu, N4_standard_deviation);

		if (sys.velocity.v>V_U)
		{
			brake.c1 = -1.0 * fabs(brake.c1);
			brake.q = 2;
		}
		break;
	case 2:
		brake.c1++;

		if (brake.c1>1)
		{
			double p = UniformRand(0, 1);
			if (!bFailureEnabled)
			{
				p = 1;	// make it high so that it does not go to failure state
			}

			if (p<P_q2_q3)
			{
				brake.q = 3;
			}
			else
			{
				if (sys.velocity.v<V_L && sys.velocity.q!=2)
				{
					brake.q = 1;
				}
				else
				{
					brake.q = 4;
					sys.i++;
				}
			}
		}
		break;
	case 3:
		// nothing updates
		break;
	case 4:
		brake.c2 = NormalRand(N5_mu, N5_standard_deviation);

		if (sys.velocity.v<V_L)
		{
			brake.c2 = -1.0 * fabs(brake.c2);

			brake.q = 5;
		}
		break;
	case 5:
		brake.c2++;

		if (brake.c2>1)
		{
			brake.q = 1;
			sys.i--;
		}
		break;
	}
}

void MultipleCarBreakingSystemState::Continue_Property()
{
	switch (prop.q)
	{
		case 1:
			if (sys.velocity.v>V_U)
			{
				prop.q = 2;
				prop.counter = T_prime;
			}
			break;
		case 2:
			prop.counter -= 1;

			if (prop.counter<=0)
			{
				prop.q = 3;
			}
			else
			if (sys.velocity.v<V_L && prop.counter>0)
			{
				prop.q = 1;
			}
			break;
		case 3:
			// nothing changes
			break;
	}
}

/*virtual*/ boost::shared_ptr<ModelObservation> MultipleCarBreakingSystemState::GetObservation()
{
	double observedVelocity = sys.velocity.v + NormalRand(N3_mu, N3_standard_deviation);
	if (observedVelocity<0)
	{
		observedVelocity = 0;
	}

	boost::shared_ptr<VelocityObservation> observationObj = boost::shared_ptr<VelocityObservation>(new VelocityObservation());
	observationObj->velocity = observedVelocity;

/*	unsigned int obs = (int) floor(observedVelocity/d_V);
	if (obs>GetMaxObservations())
	{
		obs = GetMaxObservations();
	}*/

	return boost::static_pointer_cast<ModelObservation>(observationObj);
}

/*virtual*/ double MultipleCarBreakingSystemState::WeightObservation(boost::shared_ptr<ModelObservation> observation)
{
	boost::shared_ptr<VelocityObservation> velocityObs = boost::static_pointer_cast<VelocityObservation>(observation);

	return NormalProbability(N5_mu, N3_standard_deviation, N3_variance, velocityObs->velocity - sys.velocity.v);
}

/*virtual*/ unsigned int MultipleCarBreakingSystemState::GetMaxObservations()
{
	static unsigned int numObservations = 2*ceil(V_U/d_V);
	return numObservations;
}

/*virtual*/ double MultipleCarBreakingSystemState::GetTime()
{
	return sys.velocity.t;
}

/*virtual*/ string MultipleCarBreakingSystemState::ToString()
{
	string str = boost::str (boost::format("t = %1%, q = %2%, v = %3%, i = %4%, N = %5%") % sys.velocity.t % sys.velocity.q % sys.velocity.v % sys.i % sys.N);

	for (vector<BreakingSubsystem>::iterator iBrake=sys.brakes.begin(); iBrake!=sys.brakes.end(); iBrake++)
	{
		string str_brake = boost::str (boost::format("q = %1%, c1 = %2%, c2 = %3%") % iBrake->q % iBrake->c1 % iBrake->c2);
		str = str + ", " + str_brake;
	}

	string str_prop = boost::str (boost::format("pq = %1%, pc = %2%") % prop.q % prop.counter);
	str = str + ", " + str_prop;

	return str;
}

/*virtual*/ bool MultipleCarBreakingSystemState::FromCSVTokens(std::vector<std::string>& tokens, boost::shared_ptr<ModelObservation> observation, boost::shared_ptr<ModelInput> input, bool bUpdateProperty, const boost::shared_ptr<State>& currentRealState)
{
	if (tokens.size()<5)
	{
		return false;
	}

	sys.velocity.t	= strtoul(tokens.at(0).c_str(), NULL, 10);
	sys.velocity.q	= strtoul(tokens.at(1).c_str(), NULL, 10);
	sys.velocity.v	= strtod(tokens.at(2).c_str(), NULL);
	sys.i			= strtoul(tokens.at(3).c_str(), NULL, 10);
	sys.N			= strtoul(tokens.at(4).c_str(), NULL, 10);

	if (tokens.size()<(5+sys.N*3+2))
	{
		return false;
	}

	sys.brakes.clear();

	int index = 4;

	for (int i=0; i<sys.N; i++)
	{
		BreakingSubsystem brake;
		brake.q = strtoul(tokens.at(index+1).c_str(), NULL, 10);
		brake.c1 = strtod(tokens.at(index+2).c_str(), NULL);
		brake.c2 = strtod(tokens.at(index+3).c_str(), NULL);
		index += 3;

		sys.brakes.push_back(brake);
	}

	prop.q			= strtoul(tokens.at(index+1).c_str(), NULL, 10);
	prop.counter	= strtoul(tokens.at(index+2).c_str(), NULL, 10);

	boost::shared_ptr<VelocityObservation> velObs = boost::static_pointer_cast<VelocityObservation> (observation);
	velObs->velocity = strtod(tokens.at(index+3).c_str(), NULL);

	return true;
}

/*virtual*/ string MultipleCarBreakingSystemState::GetCSV()
{
	string str = boost::str (boost::format("%1%, %2%, %3%, %4%, %5%") % sys.velocity.t % sys.velocity.q % sys.velocity.v % sys.i % sys.N);

	for (vector<BreakingSubsystem>::iterator iBrake=sys.brakes.begin(); iBrake!=sys.brakes.end(); iBrake++)
	{
		string str_brake = boost::str (boost::format("%1%, %2%, %3%") % iBrake->q % iBrake->c1 % iBrake->c2);
		str = str + ", " + str_brake;
	}

	string str_prop = boost::str (boost::format("%1%, %2%") % prop.q % prop.counter);
	str = str + ", " + str_prop;

	return str;
}

/*virtual*/ string MultipleCarBreakingSystemState::GetCSVHeader()
{
	string str = boost::str (boost::format("%1%, %2%, %3%, %4%, %5%") 	% "sys.velocity.t"
															% "sys.velocity.q"
															% "sys.velocity.v"
															% "sys.i"
															% "sys.N");

	for (vector<BreakingSubsystem>::iterator iBrake=sys.brakes.begin(); iBrake!=sys.brakes.end(); iBrake++)
	{
		string str_brake = boost::str (boost::format("%1%, %2%, %3%") % "brake.q" % "brake.c1" % "brake.c2");
		str = str + ", " + str_brake;
	}

	string str_prop = boost::str (boost::format("%1%, %2%") % "prop.q" % "prop.counter");
	str = str + ", " + str_prop;

	return str;
}

/*virtual*/ string MultipleCarBreakingSystemState::BeliefToString(const BeliefState& belief)
{
	unsigned int q[3] = {0,0,0};
	unsigned int d[4] = {0,0,0};

	for (unsigned int iParticle=0; iParticle<belief.GetSize(); iParticle++)
	{
		boost::shared_ptr<State> beliefState = belief.GetParticle(iParticle);
		boost::shared_ptr<MultipleCarBreakingSystemState> thisState = boost::static_pointer_cast<MultipleCarBreakingSystemState>(beliefState);

		q[thisState->prop.q - 1]++;
		d[thisState->sys.velocity.q - 1]++;
	}

	double q_values[3] = {0.0, 0.0, 0.0};
	double d_values[4] = {0.0, 0.0, 0.0};

	if (belief.GetSize()!=0)
	{
		q_values[0] = (1.0*q[0])/belief.GetSize();
		q_values[1] = (1.0*q[1])/belief.GetSize();
		q_values[2] = (1.0*q[2])/belief.GetSize();

		d_values[0] = (1.0*d[0])/belief.GetSize();
		d_values[1] = (1.0*d[1])/belief.GetSize();
		d_values[2] = (1.0*d[2])/belief.GetSize();
	}

	string str = boost::str (boost::format("\n\tproperty[q=1 (%1%), q=2 (%2%), q=3 (%3%)]\n\tsystem[q=1 (%4%), q=2 (%5%), q=3 (%6%)]")
								% q_values[0]
								% q_values[1]
								% q_values[2]
								% d_values[0]
								% d_values[1]
								% d_values[2]);

	return str;
}

/*virtual*/ double MultipleCarBreakingSystemState::FailureProbability(const BeliefState& belief)
{
	unsigned int q3 = 0;

	for (unsigned int iParticle=0; iParticle<belief.GetSize(); iParticle++)
	{
		boost::shared_ptr<State> beliefState = belief.GetParticle(iParticle);
		boost::shared_ptr<MultipleCarBreakingSystemState> thisState = boost::static_pointer_cast<MultipleCarBreakingSystemState>(beliefState);

		if (thisState->prop.q==3)
			q3++;
	}

	return (1.0*q3)/belief.GetSize();
}

/*virtual*/ void MultipleCarBreakingSystemState::Copy(boost::shared_ptr<State> other)
{
	boost::shared_ptr<MultipleCarBreakingSystemState> state = boost::static_pointer_cast<MultipleCarBreakingSystemState>(other);
	sys  = state->sys;
	prop = state->prop;
}

/*virtual*/ unsigned long MultipleCarBreakingSystemState::GetInputID(boost::shared_ptr<ModelInput> input)
{
	return 0;
}

/*virtual*/ void MultipleCarBreakingSystemState::GetPossibleInputs(std::vector<boost::shared_ptr<ModelInput> >& inputs)
{

}

/*virtual*/ unsigned long MultipleCarBreakingSystemState::GetObservationID(boost::shared_ptr<ModelObservation> observation)
{
	boost::shared_ptr<VelocityObservation> observationObj = boost::static_pointer_cast<VelocityObservation>(observation);

	double observedVelocity = observationObj->velocity;
	if (observedVelocity<0)
	{
		observedVelocity = 0;
	}

	unsigned int obs = (int) floor(observedVelocity/d_V);
	if (obs>GetMaxObservations())
	{
		obs = GetMaxObservations();
	}

	return obs;
}

/*virtual*/ void MultipleCarBreakingSystemState::FullyObserve(boost::shared_ptr<ModelObservation>& observation)
{

}

/*virtual*/ std::vector< boost::shared_ptr<ModelObservation> >& MultipleCarBreakingSystemState::GetPossibleObservations()
{

}

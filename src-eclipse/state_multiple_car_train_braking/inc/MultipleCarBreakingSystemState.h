/*
 * SingleCarBreakingSystemState.h
 *
 *  Created on: May 14, 2013
 *      Author: ayavol2
 */

#ifndef MULTIPLECARBREAKINGSYSTEMSTATE_H_
#define MULTIPLECARBREAKINGSYSTEMSTATE_H_

#include <Model.h>
#include <string>

class BeliefState;

class MultipleCarBreakingSystemState: public State {
public:
	struct VelocitySubsystem
	{
		bool operator==(const VelocitySubsystem& other) const
		{
			if (q==other.q && t==other.t && v==other.v)
			{
				return true;
			}
			return false;
		}

		unsigned int q;		// mode
		unsigned int t;		// time
		double v;			// noisy velocity
	};

	struct BreakingSubsystem
	{
		bool operator==(const BreakingSubsystem& other) const
		{
			if (q==other.q && c1==other.c1 && c2==other.c2)
			{
				return true;
			}
			return false;
		}

		unsigned int q;		// mode
		double c1;			// noisy counter
		double c2; 			// noisy counter
	};

	struct System
	{
		VelocitySubsystem velocity;
		std::vector<BreakingSubsystem> brakes;

		int i;	// number of engaged brakes;
		int N; 	// number of brakes;
	};

	struct Property
	{
		bool operator==(const Property& other) const
		{
			if (q==other.q && counter==other.counter)
			{
				return true;
			}
			return false;
		}

		unsigned int q;			// state id
		unsigned int counter;	// counter
	};
public:
	MultipleCarBreakingSystemState();
	virtual ~MultipleCarBreakingSystemState();

	virtual void Continue(bool bFailureEnabled, boost::shared_ptr<ModelInput> input, boost::shared_ptr<ModelInput>& actualInput);

	virtual boost::shared_ptr<ModelInput> GetInput(boost::shared_ptr<ModelInputFunction> InputFunction);

	virtual void ResetStart();

	virtual bool IsFailureState();

	virtual boost::shared_ptr<ModelObservation> GetObservation();

	virtual double WeightObservation(boost::shared_ptr<ModelObservation> observation);

	virtual std::vector< boost::shared_ptr<ModelObservation> >& GetPossibleObservations();

	virtual unsigned int GetMaxObservations();

	virtual double GetTime();

	virtual std::string GetCSV();

	virtual std::string GetCSVHeader();

	virtual bool FromCSVTokens(std::vector<std::string>& tokens, boost::shared_ptr<ModelObservation> observation, boost::shared_ptr<ModelInput> input, bool bUpdateProperty, const boost::shared_ptr<State>& currentRealState);

	virtual std::string ToString();

	virtual std::string BeliefToString(const BeliefState& belief);

	virtual double FailureProbability(const BeliefState& belief);

	virtual void Copy(boost::shared_ptr<State> other);

	virtual unsigned long GetObservationID(boost::shared_ptr<ModelObservation> observation);

	virtual unsigned long GetInputID(boost::shared_ptr<ModelInput> input);

	virtual void GetPossibleInputs(std::vector<boost::shared_ptr<ModelInput> >& inputs);

	virtual void FullyObserve(boost::shared_ptr<ModelObservation>& observation);
private:
	void Continue_System(bool bFailureEnabled);
	void Continue_Property();

	void Continue_VelocitySubsystem(bool bFailureEnabled);
	void Continue_BrakingSubsystem(BreakingSubsystem& brake, bool bFailureEnabled);
private:
	System sys;
	Property prop;

	static double N1_standard_deviation;
	static double N2_standard_deviation;
	static double N3_standard_deviation;
	static double N4_standard_deviation;
	static double N5_standard_deviation;
};

#endif /* SINGLECARBREAKINGSYSTEMSTATE_H_ */

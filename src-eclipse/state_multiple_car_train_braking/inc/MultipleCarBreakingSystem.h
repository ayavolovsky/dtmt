/*
 * SingleCarBreakingSystem.h
 *
 *  Created on: May 14, 2013
 *      Author: ayavol2
 */

#ifndef MULTIPLECARBREAKINGSYSTEM_H_
#define MULTIPLECARBREAKINGSYSTEM_H_

#include "MultipleCarBreakingSystemState.h"
#include <Model.h>
#include <MonitoringDefs.h>
#include <ClassFactory.h>
#include "VelocityObservation.h"

class MultipleCarBreakingSystem: public Model {
public:
	MultipleCarBreakingSystem();
	virtual ~MultipleCarBreakingSystem();

	virtual boost::shared_ptr<State> CreateNewState();

	virtual boost::shared_ptr<ModelObservation> CreateNewObservation();

	virtual boost::shared_ptr<ModelInput> CreateNewInput();
private:
	ClassFactory<MultipleCarBreakingSystemState> StateFactory;

	ClassFactory<VelocityObservation> ObservationFactory;
};

#endif /* SINGLECARBREAKINGSYSTEM_H_ */

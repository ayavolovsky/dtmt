################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/MultipleCarBreakingSystem.cpp \
../src/MultipleCarBreakingSystemState.cpp \
../src/VelocityObservation.cpp 

OBJS += \
./src/MultipleCarBreakingSystem.o \
./src/MultipleCarBreakingSystemState.o \
./src/VelocityObservation.o 

CPP_DEPS += \
./src/MultipleCarBreakingSystem.d \
./src/MultipleCarBreakingSystemState.d \
./src/VelocityObservation.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I"/home/andrey/Documents/src-eclipse/state_multiple_car_train_braking/inc" -I"/home/andrey/Documents/src-eclipse/monitoring_common/inc" -O0 -g3 -Wall -c -fmessage-length=0 -fPIC -fPIC -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '



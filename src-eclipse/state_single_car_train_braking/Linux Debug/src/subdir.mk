################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/SingleCarBreakingSystem.cpp \
../src/SingleCarBreakingSystemState.cpp \
../src/SingleCarBreakingSystemV2.cpp \
../src/SingleCarBreakingSystemV2State.cpp \
../src/VelocityObservation.cpp 

OBJS += \
./src/SingleCarBreakingSystem.o \
./src/SingleCarBreakingSystemState.o \
./src/SingleCarBreakingSystemV2.o \
./src/SingleCarBreakingSystemV2State.o \
./src/VelocityObservation.o 

CPP_DEPS += \
./src/SingleCarBreakingSystem.d \
./src/SingleCarBreakingSystemState.d \
./src/SingleCarBreakingSystemV2.d \
./src/SingleCarBreakingSystemV2State.d \
./src/VelocityObservation.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I"/home/andrey/Documents/src-eclipse/monitoring_common/inc" -I"/home/andrey/Documents/src-eclipse/state_single_car_train_braking/inc" -O0 -g3 -Wall -c -fmessage-length=0 -fPIC -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '



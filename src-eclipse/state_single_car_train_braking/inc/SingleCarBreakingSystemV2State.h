/*
 * SingleCarBreakingSystemState.h
 *
 *  Created on: May 14, 2013
 *      Author: ayavol2
 */

#ifndef SINGLECARBREAKINGSYSTEMV2STATE_H_
#define SINGLECARBREAKINGSYSTEMV2STATE_H_

#include <Model.h>
#include <string>

class BeliefState;

class SingleCarBreakingSystemV2State: public State {
public:
	struct System
	{
		bool operator==(const System& other) const
		{
			if (d==other.d && t==other.t && c==other.c && v==other.v)
			{
				return true;
			}
			return false;
		}

		unsigned int d;		// dynamics
		unsigned int t;		// time
		double c;			// noisy counter
		double v;			// noisy velocity
	};

	struct Property
	{
		bool operator==(const Property& other) const
		{
			if (q==other.q)
			{
				return true;
			}
			return false;
		}

		unsigned int q;			// state id
	};
public:
	SingleCarBreakingSystemV2State();
	virtual ~SingleCarBreakingSystemV2State();

	virtual void Continue(bool bFailureEnabled, boost::shared_ptr<ModelInput> input, boost::shared_ptr<ModelInput>& actualInput);

	virtual boost::shared_ptr<ModelInput> GetInput(boost::shared_ptr<ModelInputFunction> InputFunction);

	virtual void ResetStart();

	virtual bool IsFailureState();

	virtual boost::shared_ptr<ModelObservation> GetObservation();

	virtual double WeightObservation(boost::shared_ptr<ModelObservation> observation);

	virtual std::vector< boost::shared_ptr<ModelObservation> >& GetPossibleObservations();

	virtual unsigned int GetMaxObservations();

	virtual double GetTime();

	virtual std::string GetCSV();

	virtual std::string GetCSVHeader();

	virtual bool FromCSVTokens(std::vector<std::string>& tokens, boost::shared_ptr<ModelObservation> observation, boost::shared_ptr<ModelInput> input, bool bUpdateProperty, const boost::shared_ptr<State>& currentRealState);

	virtual std::string ToString();

	virtual std::string BeliefToString(const BeliefState& belief);

	virtual double FailureProbability(const BeliefState& belief);

	virtual void Copy(boost::shared_ptr<State> other);

	virtual unsigned long GetObservationID(boost::shared_ptr<ModelObservation> observation);

	virtual unsigned long GetInputID(boost::shared_ptr<ModelInput> input);

	virtual void FullyObserve(boost::shared_ptr<ModelObservation>& observation);

	virtual void GetPossibleInputs(std::vector<boost::shared_ptr<ModelInput> >& inputs);
private:
	void Continue_System(bool bFailureEnabled);
	void Continue_Property();

private:
	System sys;
	Property prop;

	static double N1_standard_deviation;
	static double N2_standard_deviation;
	static double N3_standard_deviation;
	static double N4_standard_deviation;
	static double N5_standard_deviation;
};

#endif /* SINGLECARBREAKINGSYSTEMSTATE_H_ */

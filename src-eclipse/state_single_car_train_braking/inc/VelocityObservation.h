/*
 * VelocityObservation.h
 *
 *  Created on: Apr 17, 2014
 *      Author: andrey
 */

#ifndef VELOCITYOBSERVATION_H_
#define VELOCITYOBSERVATION_H_

#include "ModelObservation.h"

class VelocityObservation : public ModelObservation {
public:
	VelocityObservation();
	virtual ~VelocityObservation();

	virtual ModelObservation* Clone();
	virtual void Copy(boost::shared_ptr<ModelObservation> other);

	virtual std::string GetCSVHeader();
	virtual std::string GetCSV();

	double velocity;
};

#endif /* VELOCITYOBSERVATION_H_ */

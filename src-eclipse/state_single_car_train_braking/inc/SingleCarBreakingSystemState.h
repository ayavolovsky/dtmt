/*
 * SingleCarBreakingSystemState.h
 *
 *  Created on: May 14, 2013
 *      Author: ayavol2
 */

#ifndef SINGLECARBREAKINGSYSTEMSTATE_H_
#define SINGLECARBREAKINGSYSTEMSTATE_H_

#include <Model.h>
#include <string>

class BeliefState;

class SingleCarBreakingSystemState: public State {
public:
	struct System
	{
		bool operator==(const System& other) const
		{
			if (d==other.d && t==other.t && c==other.c && v==other.v)
			{
				return true;
			}
			return false;
		}

		unsigned int d;		// dynamics
		unsigned int t;		// time
		double c;			// noisy counter
		double v;			// noisy velocity
	};

	struct Property
	{
		bool operator==(const Property& other) const
		{
			if (q==other.q && counter==other.counter)
			{
				return true;
			}
			return false;
		}

		unsigned int q;			// state id
		unsigned int counter;	// counter
	};
public:
	SingleCarBreakingSystemState();
	virtual ~SingleCarBreakingSystemState();

	virtual void Continue(bool bFailureEnabled, boost::shared_ptr<ModelInput> input, boost::shared_ptr<ModelInput>& actualInput);

	virtual boost::shared_ptr<ModelInput> GetInput(boost::shared_ptr<ModelInputFunction> InputFunction);

	virtual void ResetStart();

	virtual bool IsFailureState();

	virtual boost::shared_ptr<ModelObservation> GetObservation();

	virtual double WeightObservation(boost::shared_ptr<ModelObservation> observation);

	virtual std::vector< boost::shared_ptr<ModelObservation> >& GetPossibleObservations();

	virtual unsigned int GetMaxObservations();

	virtual double GetTime();

	virtual std::string GetCSV();

	virtual std::string GetCSVHeader();

	virtual bool FromCSVTokens(std::vector<std::string>& tokens, boost::shared_ptr<ModelObservation> observation, boost::shared_ptr<ModelInput> input, bool bUpdateProperty, const boost::shared_ptr<State>& currentRealState);

	virtual std::string ToString();

	virtual std::string BeliefToString(const BeliefState& belief);

	virtual double FailureProbability(const BeliefState& belief);

	virtual void Copy(boost::shared_ptr<State> other);

	virtual unsigned long GetObservationID(boost::shared_ptr<ModelObservation> observation);

	virtual unsigned long GetInputID(boost::shared_ptr<ModelInput> input);

	virtual void FullyObserve(boost::shared_ptr<ModelObservation>& observation);

	virtual void GetPossibleInputs(std::vector<boost::shared_ptr<ModelInput> >& inputs);

	virtual std::string ObservationToString(boost::shared_ptr<ModelObservation> observation);
private:
	void Continue_System(bool bFailureEnabled);
	void Continue_Property();

private:
	System sys;
	Property prop;

	static double Normal_standard_deviation;
	static double Int_standard_deviation;
	static double Fail_standard_deviation;
	static double Decel_standard_deviation;
	static double SysTimer_standard_deviation;
	static double Obs_standard_deviation;
};

#endif /* SINGLECARBREAKINGSYSTEMSTATE_H_ */

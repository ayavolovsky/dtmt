/*
 * SingleCarBreakingSystem.h
 *
 *  Created on: May 14, 2013
 *      Author: ayavol2
 */

#ifndef SINGLECARBREAKINGSYSTEMV2_H_
#define SINGLECARBREAKINGSYSTEMV2_H_

#include "SingleCarBreakingSystemV2State.h"
#include "VelocityObservation.h"

#include <Model.h>
#include <MonitoringDefs.h>
#include <ClassFactory.h>

class SingleCarBreakingSystemV2: public Model {
public:
	SingleCarBreakingSystemV2();
	virtual ~SingleCarBreakingSystemV2();

	virtual boost::shared_ptr<State> CreateNewState();

	virtual boost::shared_ptr<ModelObservation> CreateNewObservation();

	virtual boost::shared_ptr<ModelInput> CreateNewInput();
private:
	ClassFactory<SingleCarBreakingSystemV2State> StateFactory;

	ClassFactory<VelocityObservation> ObservationFactory;
};

#endif /* SINGLECARBREAKINGSYSTEM_H_ */

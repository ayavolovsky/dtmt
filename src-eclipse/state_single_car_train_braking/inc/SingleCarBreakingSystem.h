/*
 * SingleCarBreakingSystem.h
 *
 *  Created on: May 14, 2013
 *      Author: ayavol2
 */

#ifndef SINGLECARBREAKINGSYSTEM_H_
#define SINGLECARBREAKINGSYSTEM_H_

#include "SingleCarBreakingSystemState.h"
#include "VelocityObservation.h"
#include <Model.h>
#include <MonitoringDefs.h>
#include <ClassFactory.h>

class SingleCarBreakingSystem: public Model {
public:
	SingleCarBreakingSystem();
	virtual ~SingleCarBreakingSystem();

	virtual boost::shared_ptr<State> CreateNewState();

	virtual boost::shared_ptr<ModelObservation> CreateNewObservation();

	virtual boost::shared_ptr<ModelInput> CreateNewInput();
private:
	ClassFactory<SingleCarBreakingSystemState> StateFactory;
	ClassFactory<VelocityObservation> ObservationFactory;
};

#endif /* SINGLECARBREAKINGSYSTEM_H_ */

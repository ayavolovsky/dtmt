/*
 * SingleCarBreakingSystem.cpp
 *
 *  Created on: May 14, 2013
 *      Author: ayavol2
 */

#include "SingleCarBreakingSystem.h"
#include "SingleCarBreakingSystemState.h"

SingleCarBreakingSystem::SingleCarBreakingSystem() {
	// TODO Auto-generated constructor stub

}

SingleCarBreakingSystem::~SingleCarBreakingSystem() {
	// TODO Auto-generated destructor stub
}

/*virtual*/ boost::shared_ptr<State> SingleCarBreakingSystem::CreateNewState()
{
	return StateFactory.Create();
}

/*virtual*/ boost::shared_ptr<ModelObservation> SingleCarBreakingSystem::CreateNewObservation()
{
	return ObservationFactory.Create();
}

/*virtual*/ boost::shared_ptr<ModelInput> SingleCarBreakingSystem::CreateNewInput()
{
	return boost::shared_ptr<ModelInput>();
}

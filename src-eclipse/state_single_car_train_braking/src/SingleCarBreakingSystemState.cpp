/*
 * SingleCarBreakingSystemState.cpp
 *
 *  Created on: May 14, 2013
 *      Author: ayavol2
 */

#include "SingleCarBreakingSystemState.h"
#include "VelocityObservation.h"
#include <BeliefState.h>
#include <Common.h>
#include <boost/format.hpp>
#include <math.h>

#define INITIAL_VELOCITY 26

#define V_U 		28.5	// Upper level of velocity
#define V_L 		20		// Lower level of velocity
#define V_failure	26.5	// Velocity measurement in the failure state
#define d_V 		0.1		// Discretization step of velocity
#define T_prime		20		// Static time out to convert to safety automaton

//#define P_d2_d3		0.01	// probability of transition from state d2 to d3
#define P_d2_d3		0.1	// probability of transition from state d2 to d3

#define Normal_mu			0
#define Normal_variance		1
#define Int_mu				0
#define Int_variance		0.5
#define Fail_mu				0
#define Fail_variance		0.7
#define Decel_mu			0
#define Decel_variance		0.7
#define SysTimer_mu			0
#define SysTimer_variance	5
#define Obs_mu				0
#define Obs_variance		0.5

using namespace std;

/*static*/ double SingleCarBreakingSystemState::Normal_standard_deviation = sqrt((double)Normal_variance);	// normal
/*static*/ double SingleCarBreakingSystemState::Int_standard_deviation = sqrt((double)Int_variance);	// intermediate
/*static*/ double SingleCarBreakingSystemState::Fail_standard_deviation = sqrt((double)Fail_variance);	// failure
/*static*/ double SingleCarBreakingSystemState::Decel_standard_deviation = sqrt((double)Decel_variance);	// deceleration mode
/*static*/ double SingleCarBreakingSystemState::SysTimer_standard_deviation = sqrt((double)SysTimer_variance);	// system timer
/*static*/ double SingleCarBreakingSystemState::Obs_standard_deviation = sqrt((double)Obs_variance);	// observation

SingleCarBreakingSystemState::SingleCarBreakingSystemState() {

}

SingleCarBreakingSystemState::~SingleCarBreakingSystemState() {

}

/*virtual*/ void SingleCarBreakingSystemState::ResetStart()
{
	//sys.d = 10;					// dynamics
	sys.d = 1;					// dynamics
	sys.t = 1;					// time
	sys.c = 0;					// counter
	sys.v = INITIAL_VELOCITY;	// velocity

	prop.q = 1;					// state
	prop.counter = 0;			// counter
}

/*virtual*/ bool SingleCarBreakingSystemState::IsFailureState()
{
	return (prop.q==3);
}

boost::shared_ptr<ModelInput> SingleCarBreakingSystemState::GetInput(boost::shared_ptr<ModelInputFunction> InputFunction)
{
	if (!InputFunction)
	{
		return boost::shared_ptr<ModelInput>();
	}

	return InputFunction->getInput(sys.t);
}

/*virtual*/ void SingleCarBreakingSystemState::Continue(bool bFailureEnabled, boost::shared_ptr<ModelInput> input, boost::shared_ptr<ModelInput>& actualInput)
{
	Continue_System(bFailureEnabled);
	Continue_Property();
}

/*void SingleCarBreakingSystemState::Continue_System(bool bFailureEnabled)
{
	double p;

	switch (sys.d)
	{
	case 10:
		sys.v = 0.1353*sys.v + 0.8647*(25+2.5*sin(sys.t)) + NormalRand(Normal_mu, Normal_standard_deviation);
		sys.c = NormalRand(SysTimer_mu, SysTimer_standard_deviation);

		p = UniformRand(0, 1);
		if (bFailureEnabled && p<=P_d2_d3)
		{
			sys.d = 3;
		}
		else
		if (sys.v>V_U)	// transition condition
		{
			sys.d = 20;
			sys.c = -1*fabs(sys.c);
		}
		else
		{
			sys.d = 11;
		}
		break;
	case 11:
		sys.v = 0.1353*sys.v + 0.8647*(25+2.5*sin(sys.t)) + NormalRand(Normal_mu, Normal_standard_deviation);
		sys.c = NormalRand(SysTimer_mu, SysTimer_standard_deviation);

		p = UniformRand(0, 1);
		if (bFailureEnabled && p<=P_d2_d3)
		{
			sys.d = 3;
		}
		else
		if (sys.v>V_U)	// transition condition
		{
			sys.d = 20;
			sys.c = -1*fabs(sys.c);
		}
		else
		{
			sys.d = 10;
		}
		break;
	case 20:
		sys.c += 1;
		sys.v = sys.v + NormalRand(Int_mu, Int_standard_deviation);

		p = UniformRand(0, 1);
		if (bFailureEnabled && p<=P_d2_d3)
		{
			sys.d = 3;
		}
		else
		if (sys.c>1)	// transition condition
		{
			sys.d = 40;
		}
		else
		{
			sys.d = 21;
		}
		break;
	case 21:
		sys.c += 1;
		sys.v = sys.v + NormalRand(Int_mu, Int_standard_deviation);

		p = UniformRand(0, 1);
		if (bFailureEnabled && p<=P_d2_d3)
		{
			sys.d = 3;
		}
		else
		if (sys.c>1)	// transition condition
		{
			sys.d = 40;
		}
		else
		{
			sys.d = 20;
		}
		break;
	case 3:
		sys.v = sys.v + NormalRand(Fail_mu, Fail_standard_deviation);

		break;
	case 40:
		sys.v = sys.v - 1 + NormalRand(Decel_mu, Decel_standard_deviation);

		p = UniformRand(0, 1);
		if (bFailureEnabled && p<=P_d2_d3)
		{
			sys.d = 3;
		}
		else
		if (sys.v<V_L)
		{
			sys.d = 10;
		}
		else
		{
			sys.d = 41;
		}
		break;
	case 41:
		sys.v = sys.v - 1 + NormalRand(Decel_mu, Decel_standard_deviation);

		p = UniformRand(0, 1);
		if (bFailureEnabled && p<=P_d2_d3)
		{
			sys.d = 3;
		}
		else
		if (sys.v<V_L)
		{
			sys.d = 10;
		}
		else
		{
			sys.d = 40;
		}
		break;
	}

	// increment time counter
	sys.t += 1;
}*/

void SingleCarBreakingSystemState::Continue_System(bool bFailureEnabled)
{
	switch (sys.d)
	{
	case 1:
		sys.v = 0.1353*sys.v + 0.8647*(25+2.5*sin((double) sys.t)) + NormalRand(Normal_mu, Normal_standard_deviation);
		sys.c = NormalRand(SysTimer_mu, SysTimer_standard_deviation);

		if (sys.v>V_U)	// transition condition
		{
			sys.d = 2;
			sys.c = -1*fabs(sys.c);
		}
		break;
	case 2:
		sys.c += 1;
		sys.v = sys.v + NormalRand(Int_mu, Int_standard_deviation);

		if (sys.c>0)	// transition condition
		{
			double p = UniformRand(0, 1);
			if (!bFailureEnabled)
			{
				sys.d = 4;
			}
			else
			{
				if (p<=P_d2_d3)
				{
					sys.d = 3;
				}
				else
				{
					sys.d = 4;
				}
			}
		}
		break;
	case 3:
		sys.v = sys.v + fabs(NormalRand(Fail_mu, Fail_standard_deviation));

		break;
	case 4:
		sys.v = sys.v - 1 + NormalRand(Decel_mu, Decel_standard_deviation);

		if (sys.v<V_L)
		{
			sys.d = 1;
		}
		break;
	}

	// increment time counter
	sys.t += 1;
}

void SingleCarBreakingSystemState::Continue_Property()
{
	switch (prop.q)
	{
		case 1:
			if (sys.v>V_U)
			{
				prop.q = 2;
				prop.counter = T_prime;
			}
			break;
		case 2:
			prop.counter -= 1;

			if (prop.counter<=0)
			{
				prop.q = 3;
			}
			else
			if (sys.v<V_L && prop.counter>0)
			{
				prop.q = 1;
			}
			break;
		case 3:
			// nothing changes
			break;
	}

	/*switch (prop.q)
		{
			case 1:
				if (sys.d==3)
				{
					prop.q = 3;
				}
				break;
			case 2:
				break;
			case 3:
				// nothing changes
				break;
		}*/
}

/*virtual*/ boost::shared_ptr<ModelObservation> SingleCarBreakingSystemState::GetObservation()
{
	boost::shared_ptr<VelocityObservation> velocityObs = boost::shared_ptr<VelocityObservation>(new VelocityObservation());

	double observedVelocity = sys.v + NormalRand(Obs_mu, Obs_standard_deviation);
	if (observedVelocity<0)
	{
		observedVelocity = 0;
	}

	velocityObs->velocity = observedVelocity;
	return velocityObs;
}

std::string SingleCarBreakingSystemState::ObservationToString(boost::shared_ptr<ModelObservation> observation)
{
	boost::shared_ptr<VelocityObservation> velocityObs = boost::static_pointer_cast<VelocityObservation>(observation);

	string str = boost::str (boost::format("Observed Velocity = %1%") % velocityObs->velocity);

	return str;
}

/*virtual*/ std::vector< boost::shared_ptr<ModelObservation> >& SingleCarBreakingSystemState::GetPossibleObservations()
{

}

/*virtual*/ double SingleCarBreakingSystemState::WeightObservation(boost::shared_ptr<ModelObservation> observation)
{
	boost::shared_ptr<VelocityObservation> velocityObs = boost::static_pointer_cast<VelocityObservation>(observation);

	double observationVelocity = velocityObs->velocity;

	//double x = observationVelocity - sys.v;
	//double p = (1.0/(N5_delta*sqrt(2*M_PI))) * exp((-1.0*(x-N5_mu)*(x-N5_mu))/(2*N5_delta*N5_delta));

	return NormalProbability(Obs_mu, Obs_standard_deviation, Obs_variance, observationVelocity - sys.v);
}

/*virtual*/ unsigned int SingleCarBreakingSystemState::GetMaxObservations()
{
	static unsigned int numObservations = 2*ceil(V_U/d_V);
	return numObservations;
}

/*virtual*/ double SingleCarBreakingSystemState::GetTime()
{
	return sys.t;
}

/*virtual*/ string SingleCarBreakingSystemState::ToString()
{
	string str = boost::str (boost::format("t = %1%, d = %2%, c = %3%, v = %4%, q = %5%, counter = %6%") % sys.t % sys.d % sys.c % sys.v % prop.q % prop.counter);
	return str;
}

/*virtual*/ bool SingleCarBreakingSystemState::FromCSVTokens(std::vector<std::string>& tokens, boost::shared_ptr<ModelObservation> observation, boost::shared_ptr<ModelInput> input, bool bUpdateProperty, const boost::shared_ptr<State>& currentRealState)
{
	if (tokens.size()!=7)
	{
		return false;
	}

	sys.t			= strtoul(tokens.at(0).c_str(), NULL, 10);
	sys.d			= strtoul(tokens.at(1).c_str(), NULL, 10);
	sys.c			= strtod(tokens.at(2).c_str(), NULL);
	sys.v			= strtod(tokens.at(3).c_str(), NULL);
	prop.q			= strtoul(tokens.at(4).c_str(), NULL, 10);
	prop.counter	= strtoul(tokens.at(5).c_str(), NULL, 10);

	boost::shared_ptr<VelocityObservation> velObs = boost::static_pointer_cast<VelocityObservation> (observation);
	velObs->velocity = strtod(tokens.at(6).c_str(), NULL);

	return true;
}

/*virtual*/ string SingleCarBreakingSystemState::GetCSV()
{
	string str = boost::str (boost::format("%1%, %2%, %3%, %4%, %5%, %6%") % sys.t % sys.d % sys.c % sys.v % prop.q % prop.counter);
	return str;
}

/*virtual*/ string SingleCarBreakingSystemState::GetCSVHeader()
{
	string str = boost::str (boost::format("%1%, %2%, %3%, %4%, %5%, %6%") 	% "system.t"
															% "system.d"
															% "system.c"
															% "system.v"
															% "property.q"
															% "property.counter");
	return str;
}

/*virtual*/ string SingleCarBreakingSystemState::BeliefToString(const BeliefState& belief)
{
	unsigned int q[3] = {0,0,0};
	unsigned int d[4] = {0,0,0,0};

	for (unsigned int iParticle=0; iParticle<belief.GetSize(); iParticle++)
	{
		boost::shared_ptr<State> beliefState = belief.GetParticle(iParticle);
		boost::shared_ptr<SingleCarBreakingSystemState> thisState = boost::static_pointer_cast<SingleCarBreakingSystemState>(beliefState);

		q[thisState->prop.q - 1]++;
		d[thisState->sys.d - 1]++;
	}

	double q_values[3] = {0.0, 0.0, 0.0};
	double d_values[4] = {0.0, 0.0, 0.0, 0.0};

	if (belief.GetSize()!=0)
	{
		q_values[0] = (1.0*q[0])/belief.GetSize();
		q_values[1] = (1.0*q[1])/belief.GetSize();
		q_values[2] = (1.0*q[2])/belief.GetSize();

		d_values[0] = (1.0*d[0])/belief.GetSize();
		d_values[1] = (1.0*d[1])/belief.GetSize();
		d_values[2] = (1.0*d[2])/belief.GetSize();
		d_values[3] = (1.0*d[3])/belief.GetSize();
	}

	string str = boost::str (boost::format("\n\t[q=1 (%1%), q=2 (%2%), q=3 (%3%)]\n\t[d=1 (%4%), d=2 (%5%), d=3 (%6%), d=4 (%7%)]")
								% q_values[0]
								% q_values[1]
								% q_values[2]
								% d_values[0]
								% d_values[1]
								% d_values[2]
								% d_values[3]);

	return str;
}

/*virtual*/ double SingleCarBreakingSystemState::FailureProbability(const BeliefState& belief)
{
	unsigned int q3 = 0;

	for (unsigned int iParticle=0; iParticle<belief.GetSize(); iParticle++)
	{
		boost::shared_ptr<State> beliefState = belief.GetParticle(iParticle);
		boost::shared_ptr<SingleCarBreakingSystemState> thisState = boost::static_pointer_cast<SingleCarBreakingSystemState>(beliefState);

		if (thisState->prop.q==3)
			q3++;
	}

	return (1.0*q3)/belief.GetSize();
}

/*virtual*/ void SingleCarBreakingSystemState::Copy(boost::shared_ptr<State> other)
{
	boost::shared_ptr<SingleCarBreakingSystemState> state = boost::static_pointer_cast<SingleCarBreakingSystemState>(other);
	sys  = state->sys;
	prop = state->prop;
}

/*virtual*/ unsigned long SingleCarBreakingSystemState::GetInputID(boost::shared_ptr<ModelInput> input)
{
	return 0;
}

/*virtual*/ unsigned long SingleCarBreakingSystemState::GetObservationID(boost::shared_ptr<ModelObservation> observation)
{
	boost::shared_ptr<VelocityObservation> velocityObservation = boost::static_pointer_cast<VelocityObservation> (observation);

	double observedVelocity = velocityObservation->velocity;
	if (observedVelocity<0)
	{
		observedVelocity = 0;
	}

	unsigned int obs = (int) floor(observedVelocity/d_V);
	if (obs>GetMaxObservations())
	{
		obs = GetMaxObservations();
	}

	return obs;
}

/*virtual*/ void SingleCarBreakingSystemState::FullyObserve(boost::shared_ptr<ModelObservation>& observation)
{

}

/*virtual*/ void SingleCarBreakingSystemState::GetPossibleInputs(std::vector<boost::shared_ptr<ModelInput> >& inputs)
{

}

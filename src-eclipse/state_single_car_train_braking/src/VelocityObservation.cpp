/*
 * VelocityObservation.cpp
 *
 *  Created on: Apr 17, 2014
 *      Author: andrey
 */

#include "VelocityObservation.h"
#include <boost/format.hpp>

VelocityObservation::VelocityObservation() {
	// TODO Auto-generated constructor stub
	velocity = 0.0;
}

VelocityObservation::~VelocityObservation() {
	// TODO Auto-generated destructor stub
}

/*virtual*/ ModelObservation* VelocityObservation::Clone()
{
	VelocityObservation* copy = new VelocityObservation();
	copy->velocity = velocity;

	return copy;
}

/*virtual*/ void VelocityObservation::Copy(boost::shared_ptr<ModelObservation> other)
{
	boost::shared_ptr<VelocityObservation> velObsOther = boost::static_pointer_cast<VelocityObservation> (other);
	velocity = velObsOther->velocity;
}

/*virtual*/ std::string VelocityObservation::GetCSVHeader()
{
	std::string str = boost::str (boost::format("Observed Velocity"));
	return str;
}

/*virtual*/ std::string VelocityObservation::GetCSV()
{
	std::string str = boost::str (boost::format("%1%") % velocity);
	return str;
}

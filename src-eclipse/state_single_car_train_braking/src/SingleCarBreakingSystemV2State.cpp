/*
 * SingleCarBreakingSystemState.cpp
 *
 *  Created on: May 14, 2013
 *      Author: ayavol2
 */

#include "SingleCarBreakingSystemV2State.h"
#include "VelocityObservation.h"
#include <BeliefState.h>
#include <Common.h>
#include <boost/format.hpp>
#include <math.h>

#define INITIAL_VELOCITY 26

#define V_U 		26.5	// Upper level of velocity
#define V_L 		20		// Lower level of velocity
#define V_failure	26.5	// Velocity measurement in the failure state
#define d_V 		0.1		// Discretization step of velocity
#define T_0			1		// Delay in actuation and computation
#define T_prime		25		// Static time out to convert to safety automaton
#define DECELERATION_RATE 0.25

#define P_d2_d3		0.08		// probability of transition from state d2 to d3

#define N1_mu		0
#define N1_variance	1
#define N2_mu		0
#define N2_variance	0.1
#define N3_mu		0
#define N3_variance	0.5
#define N4_mu		0
#define N4_variance	3
#define N5_mu		0
#define N5_variance	1.25

using namespace std;

/*static*/ double SingleCarBreakingSystemV2State::N1_standard_deviation = sqrt(N1_variance);
/*static*/ double SingleCarBreakingSystemV2State::N2_standard_deviation = sqrt(N2_variance);
/*static*/ double SingleCarBreakingSystemV2State::N3_standard_deviation = sqrt(N3_variance);
/*static*/ double SingleCarBreakingSystemV2State::N4_standard_deviation = sqrt(N4_variance);
/*static*/ double SingleCarBreakingSystemV2State::N5_standard_deviation = sqrt(N5_variance);

SingleCarBreakingSystemV2State::SingleCarBreakingSystemV2State() {

}

SingleCarBreakingSystemV2State::~SingleCarBreakingSystemV2State() {

}

/*virtual*/ void SingleCarBreakingSystemV2State::ResetStart()
{
	sys.d = 1;					// dynamics
	sys.t = 1;					// time
	sys.c = 0;					// counter
	sys.v = INITIAL_VELOCITY;	// velocity

	prop.q = 1;					// state
}

/*virtual*/ bool SingleCarBreakingSystemV2State::IsFailureState()
{
	return (prop.q==2);
}

boost::shared_ptr<ModelInput> SingleCarBreakingSystemV2State::GetInput(boost::shared_ptr<ModelInputFunction> InputFunction)
{
	if (!InputFunction)
	{
		return boost::shared_ptr<ModelInput>();
	}

	return InputFunction->getInput(sys.t);
}

/*virtual*/ void SingleCarBreakingSystemV2State::Continue(bool bFailureEnabled, boost::shared_ptr<ModelInput> input, boost::shared_ptr<ModelInput>& actualInput)
{
	Continue_System(bFailureEnabled);
	Continue_Property();
}

void SingleCarBreakingSystemV2State::Continue_System(bool bFailureEnabled)
{
	switch (sys.d)
	{
	case 1:
		sys.v = 0.1353*sys.v + 0.8647*(25+sin(sys.t)) + NormalRand(N1_mu, N1_standard_deviation);
		sys.c = NormalRand(N4_mu, N4_standard_deviation);

		if (sys.v>V_U)	// transition condition
		{
			sys.d = 2;
			sys.c = -1*fabs(sys.c);
		}
		break;
	case 2:
		sys.c += 1;
		//sys.v = sys.v + NormalRand(N2_mu, N2_standard_deviation);
		sys.v = V_failure + NormalRand(N2_mu, N2_standard_deviation);

		//sys.v = V_failure + /*sys.v +*/ NormalRand(N2_mu, N2_standard_deviation);	// temporary change

		if (sys.c>T_0)	// transition condition
		{
			double p = UniformRand(0, 1);
			if (!bFailureEnabled)
			{
				sys.d = 4;
			}
			else
			{
				if (p<P_d2_d3)
				{
					sys.d = 3;
				}
				else
				{
					sys.d = 4;
				}
			}
		}
		break;
	case 3:
		//sys.v = sys.v + NormalRand(N2_mu, N2_standard_deviation);
		sys.v = V_failure + NormalRand(N2_mu, N2_standard_deviation);

		//sys.v = V_failure + /*sys.v +*/ NormalRand(N2_mu, N2_standard_deviation);
		break;
	case 4:
		sys.v = sys.v - DECELERATION_RATE + NormalRand(N3_mu, N3_standard_deviation);

		if (sys.v<V_L)
		{
			sys.d = 1;
		}
		break;
	}

	// increment time counter
	sys.t += 1;
}

void SingleCarBreakingSystemV2State::Continue_Property()
{
	switch (prop.q)
	{
		case 1:
			if (sys.d==3)
			{
				prop.q = 2;
			}
			break;
		case 2:
			// nothing changes
			break;
	}
}

/*virtual*/ boost::shared_ptr<ModelObservation> SingleCarBreakingSystemV2State::GetObservation()
{
	boost::shared_ptr<VelocityObservation> velObs = boost::shared_ptr<VelocityObservation> (new VelocityObservation());

	double observedVelocity = sys.v + NormalRand(N5_mu, N5_standard_deviation);
	if (observedVelocity<0)
	{
		observedVelocity = 0;
	}

	velObs->velocity = observedVelocity;

	return boost::static_pointer_cast<ModelObservation> (velObs);
}

/*virtual*/ double SingleCarBreakingSystemV2State::WeightObservation(boost::shared_ptr<ModelObservation> observation)
{
	boost::shared_ptr<VelocityObservation> velocityObs = boost::static_pointer_cast<VelocityObservation>(observation);

	double observationVelocity = velocityObs->velocity;
	return NormalProbability(N5_mu, N5_standard_deviation, N5_variance, observationVelocity - sys.v);
}

/*virtual*/ std::vector< boost::shared_ptr<ModelObservation> >& SingleCarBreakingSystemV2State::GetPossibleObservations()
{

}

/*virtual*/ unsigned int SingleCarBreakingSystemV2State::GetMaxObservations()
{
	static unsigned int numObservations = 2*ceil(V_U/d_V);
	return numObservations;
}

/*virtual*/ double SingleCarBreakingSystemV2State::GetTime()
{
	return sys.t;
}

/*virtual*/ string SingleCarBreakingSystemV2State::ToString()
{
	string str = boost::str (boost::format("t = %1%, d = %2%, c = %3%, v = %4%, q = %5%") % sys.t % sys.d % sys.c % sys.v % prop.q);
	return str;
}

/*virtual*/ bool SingleCarBreakingSystemV2State::FromCSVTokens(std::vector<std::string>& tokens, boost::shared_ptr<ModelObservation> observation, boost::shared_ptr<ModelInput> input, bool bUpdateProperty, const boost::shared_ptr<State>& currentRealState)
{
	if (tokens.size()!=5)
	{
		return false;
	}

	sys.t			= strtoul(tokens.at(0).c_str(), NULL, 10);
	sys.d			= strtoul(tokens.at(1).c_str(), NULL, 10);
	sys.c			= strtod(tokens.at(2).c_str(), NULL);
	sys.v			= strtod(tokens.at(3).c_str(), NULL);
	prop.q			= strtoul(tokens.at(4).c_str(), NULL, 10);

	boost::shared_ptr<VelocityObservation> velObs = boost::static_pointer_cast<VelocityObservation> (observation);
	velObs->velocity = strtod(tokens.at(5).c_str(), NULL);

	return true;
}

/*virtual*/ string SingleCarBreakingSystemV2State::GetCSV()
{
	string str = boost::str (boost::format("%1%, %2%, %3%, %4%, %5%") % sys.t % sys.d % sys.c % sys.v % prop.q);
	return str;
}

/*virtual*/ string SingleCarBreakingSystemV2State::GetCSVHeader()
{
	string str = boost::str (boost::format("%1%, %2%, %3%, %4%, %5%") 	% "system.t"
															% "system.d"
															% "system.c"
															% "system.v"
															% "property.q");
	return str;
}

/*virtual*/ string SingleCarBreakingSystemV2State::BeliefToString(const BeliefState& belief)
{
	unsigned int q[2] = {0,0};
	unsigned int d[4] = {0,0,0,0};

	for (unsigned int iParticle=0; iParticle<belief.GetSize(); iParticle++)
	{
		boost::shared_ptr<State> beliefState = belief.GetParticle(iParticle);
		boost::shared_ptr<SingleCarBreakingSystemV2State> thisState = boost::static_pointer_cast<SingleCarBreakingSystemV2State>(beliefState);

		q[thisState->prop.q - 1]++;
		d[thisState->sys.d - 1]++;
	}

	double q_values[3] = {0.0, 0.0};
	double d_values[4] = {0.0, 0.0, 0.0, 0.0};

	if (belief.GetSize()!=0)
	{
		q_values[0] = (1.0*q[0])/belief.GetSize();
		q_values[1] = (1.0*q[1])/belief.GetSize();

		d_values[0] = (1.0*d[0])/belief.GetSize();
		d_values[1] = (1.0*d[1])/belief.GetSize();
		d_values[2] = (1.0*d[2])/belief.GetSize();
		d_values[3] = (1.0*d[3])/belief.GetSize();
	}

	string str = boost::str (boost::format("\n\t[q=1 (%1%), q=2 (%2%)]\n\t[d=1 (%3%), d=2 (%4%), d=3 (%5%), d=4 (%6%)]")
								% q_values[0]
								% q_values[1]
								% d_values[0]
								% d_values[1]
								% d_values[2]
								% d_values[3]);

	return str;
}

/*virtual*/ double SingleCarBreakingSystemV2State::FailureProbability(const BeliefState& belief)
{
	unsigned int q2 = 0;

	for (unsigned int iParticle=0; iParticle<belief.GetSize(); iParticle++)
	{
		boost::shared_ptr<State> beliefState = belief.GetParticle(iParticle);
		boost::shared_ptr<SingleCarBreakingSystemV2State> thisState = boost::static_pointer_cast<SingleCarBreakingSystemV2State>(beliefState);

		if (thisState->prop.q==2)
			q2++;
	}

	return (1.0*q2)/belief.GetSize();
}

/*virtual*/ void SingleCarBreakingSystemV2State::Copy(boost::shared_ptr<State> other)
{
	boost::shared_ptr<SingleCarBreakingSystemV2State> state = boost::static_pointer_cast<SingleCarBreakingSystemV2State>(other);
	sys  = state->sys;
	prop = state->prop;
}

/*virtual*/ unsigned long SingleCarBreakingSystemV2State::GetInputID(boost::shared_ptr<ModelInput> input)
{
	return 0;
}

/*virtual*/ unsigned long SingleCarBreakingSystemV2State::GetObservationID(boost::shared_ptr<ModelObservation> observation)
{
	boost::shared_ptr<VelocityObservation> velObs = boost::static_pointer_cast<VelocityObservation>(observation);

	double observedVelocity = velObs->velocity;
	if (observedVelocity<0)
	{
		observedVelocity = 0;
	}

	unsigned int obs = (int) floor(observedVelocity/d_V);
	if (obs>GetMaxObservations())
	{
		obs = GetMaxObservations();
	}

	return obs;
}

/*virtual*/ void SingleCarBreakingSystemV2State::FullyObserve(boost::shared_ptr<ModelObservation>& observation)
{

}

/*virtual*/ void SingleCarBreakingSystemV2State::GetPossibleInputs(std::vector<boost::shared_ptr<ModelInput> >& inputs)
{

}

/*
 * SingleCarBreakingSystem.cpp
 *
 *  Created on: May 14, 2013
 *      Author: ayavol2
 */

#include "SingleCarBreakingSystemV2.h"
#include "SingleCarBreakingSystemV2State.h"

SingleCarBreakingSystemV2::SingleCarBreakingSystemV2() {
	// TODO Auto-generated constructor stub

}

SingleCarBreakingSystemV2::~SingleCarBreakingSystemV2() {
	// TODO Auto-generated destructor stub
}

/*virtual*/ boost::shared_ptr<State> SingleCarBreakingSystemV2::CreateNewState()
{
	return StateFactory.Create();
}

/*virtual*/ boost::shared_ptr<ModelObservation> SingleCarBreakingSystemV2::CreateNewObservation()
{
	return ObservationFactory.Create();
}

/*virtual*/ boost::shared_ptr<ModelInput> SingleCarBreakingSystemV2::CreateNewInput()
{
	return boost::shared_ptr<ModelInput>();
}
